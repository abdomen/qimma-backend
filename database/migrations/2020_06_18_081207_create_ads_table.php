<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image')->nullable();
            $table->string('title_ar',100)->nullable();
            $table->string('title_en',100)->nullable();
            $table->string('sub_title_ar',100)->nullable();
            $table->string('sub_title_en',100)->nullable();
            $table->string('btn_text_ar',100)->nullable();
            $table->string('btn_text_en',100)->nullable();
            $table->string('btn_link',100)->nullable();
            $table->smallInteger('place_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
