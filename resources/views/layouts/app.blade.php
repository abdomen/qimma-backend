<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    @if(get_lang()=='ar')
        <link href="/assets/css/styleAR.css" rel="stylesheet">
        <link href="/assets/css/ResponsiveAR.css" rel="stylesheet">

    @else
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/Responsive.css" rel="stylesheet">
    @endif
    <script data-ad-client="ca-pub-6502088367002684" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <link rel="stylesheet" href="/assets/toast/jquery.toast.css">

</head>

<!-- LOADER -->
<div class="preloader">
    <div class="lds-ellipsis">
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- END LOADER -->


@include('include.header')
@include('include.model')



@yield('content')



@include('include.footer')

@include('include.product_model')


<script type="text/javascript" style="display:none">
    //<![CDATA[
    window.__mirage2 = {petok: "0e9dce7ced70b73bc910ecbe04b33c55d7811336-1595135654-1800"};
    //]]>
</script>
<script type="text/javascript"
        src="https://ajax.cloudflare.com/cdn-cgi/scripts/04b3eb47/cloudflare-static/mirage2.min.js"></script>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src="/assets/js/jquery-3.3.1.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/custom.js"></script>
<script src="/assets/toast/jquery.toast.js"></script>

<script>

    function Toset(messag, btnClass, text, hideAfter) {
        $.toast({
            heading: messag,
            text: text,
            position: 'bottom-right',
            stack: 2,
            icon: btnClass,
            hideAfter: hideAfter,

        });
    }
</script>
@include('include.maniScripts')

@yield('script')

</body>
</htm>
