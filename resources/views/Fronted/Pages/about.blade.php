@extends('layouts.app')
@php
$about=\App\Models\About::first();
$settings=\App\Models\Settings::first();
$active=3;
@endphp
@section('title')
    {{trans('main.About_Us')}}
    @endsection
@section('content')
    <div class="header-top-inner">
        <div class="container">

            <h2>{{trans('main.About_Us')}}</h2>

        </div>
    </div>

    <div class="welcome_area">
        <div class="container">
            <div class="row">
                <div class="about_shipping">
                    <ul>
                        <li>
                            <figure><img data-cfsrc="/assets/images/icon_shipping.png" alt="happy cutomer"
                                         style="display:none;visibility:hidden;">
                                <noscript><img src="/assets/images/icon_shipping.png" alt="happy cutomer"></noscript>
                            </figure>
                            <h4>{{trans('main.Shipping')}}<span>
                                {{get_lang() =='ar' ? get_main_seetings()->free_deliviry_ar : get_main_seetings()->free_deliviry_en}}
                            </span></h4>
                        </li>
                        <li>
                            <figure><img data-cfsrc="/assets/images/icon_support.png" alt="happy cutomer"
                                         style="display:none;visibility:hidden;">
                                <noscript><img src="/assets/images/icon_support.png" alt="happy cutomer"></noscript>
                            </figure>
                            <h4>{{trans('main.Support')}}<span>
                            {{get_lang() =='ar' ? get_main_seetings()->Support_ar : get_main_seetings()->Support_en}}
                            </span></h4>
                        </li>
                        <li>
                            <figure><img data-cfsrc="/assets/images/icon_partner.png" alt="happy cutomer"
                                         style="display:none;visibility:hidden;">
                                <noscript><img src="/assets/images/icon_partner.png" alt="happy cutomer"></noscript>
                            </figure>
                            <h4>{{trans('main.Guarantee')}}<span>
                                {{get_lang() =='ar' ? get_main_seetings()->Guarantee_ar : get_main_seetings()->Guarantee_en}}
                            </span></h4>
                        </li>
                        <li>
                            <figure><img data-cfsrc="/assets/images/icon_heart.png" alt="happy cutomer"
                                         style="display:none;visibility:hidden;">
                                <noscript><img src="/assets/images/icon_heart.png" alt="happy cutomer"></noscript>
                            </figure>
                            <h4>{{trans('main.made')}}<span>
                                {{get_lang() =='ar' ? get_main_seetings()->made_ar : get_main_seetings()->made_en}}
                            </span></h4>
                        </li>
                    </ul>
                </div>
                <div class="welcome_screen">
                    <div class="welcome_head">
                        <div class="site-section-area">
                            <h2>
                                {{ get_lang()=='ar' ? $about->title_ar : $about->title_en}}
                            </h2>

                        </div>
                    </div>
                    <div class="welcome_para">
                        <p>
                            {{ get_lang()=='ar' ? $about->desc_ar : $about->desc_en}}
                        </p>


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
