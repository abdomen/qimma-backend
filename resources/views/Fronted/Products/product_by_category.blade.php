
@extends('layouts.app')
@section('title')
    {{trans('main.products')}}
@endsection

@section('content')
    <div class="header-top-inner">
        <div class="container">

            <h2>{{trans('main.products')}}</h2>

        </div>
    </div>


    <div class="featured-area">
        <div class="container">
            <div class="row">
                <div class="products-sec">

                    <div class="feature-tab-area">

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="container">
                                    <div class="row">
                                        @foreach($products as $row)
                                            {!! product_div($row) !!}
                                        @endforeach
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <br>
            <div class="pagination_area text-center">
                <ul class="pagination">
                    {{$products->links()}}
                </ul>
            </div>

        </div>

    </div>

    @include('include.suscribe')
    @include('include.our_brands')

@endsection
