<div class="row">
    <div class="col-12">
        <div class="heading_s1">
            <h3>{{trans('main.Releted_Products')}}</h3>
        </div>
        <div class="releted_product_slider carousel_slider owl-carousel owl-theme" data-margin="20" data-responsive='{"0":{"items": "1"}, "481":{"items": "2"}, "768":{"items": "3"}, "1199":{"items": "4"}}'>
            @foreach($products as $row)
            <div class="item">
                <div class="product">
                        {!! img_div($row) !!}
                    <div class="product_info">
                        <h6 class="product_title"><a href="{{Route('productsFront.productDetails',$row->id)}}">{{getTitleOrDesc($row,1)}}</a></h6>
                        {!! product_price($row) !!}
                        <div class="rating_wrap">
                            <div class="rating">
                                <div class="product_rate" style="width:{{$row->rate * 100 / 5}}%"></div>
                            </div>
                            <span class="rating_num">({{$row->user_rate->count()}})</span>
                        </div>
                        <div class="pr_desc">
                            <p></p>
                        </div>
                        {!! color_div($row) !!}
                    </div>
                </div>
            </div>
                @endforeach

        </div>
    </div>
</div>

