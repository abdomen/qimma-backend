<div class="container"> 
      <br>
      <h2 class="title-review"> {{trans('main.Reviews')}}</h2>
      <br>
 <div class="review-content review-content-show">
								<div class="review-showing">
								     @if($product->user_rate->count() > 0)
									<ul class="content">
									    @foreach($product->user_rate as $row)
										<li>
											<div class="post-thumb">
												<img src="{{getImageUrl('users',$row->image)}}" alt="shop">
											</div>
											<div class="post-content">
												<div class="entry-meta">
													<div class="posted-on">
														<a href="#">{{$row->frist_name}}</a>
														<p>{{date('d M Y',strtotime($row->pivot->created_at))}}</p>
													</div>
													<div class="rating">
                                    @for($i=0;$i<$row->pivot->rate;$i++)
                                    <i class="fa fa-star"></i> 
                                    @endfor
													</div>
												</div>
												<div class="entry-content">
													<p> {{$row->pivot->comment}}</p>
												</div>
											</div>
										</li>
										@endforeach
									</ul>
									@endif
									<div class="client-review">
										<div class="review-form">
											<div class="review-title">
												<h5>@if(get_lang()=='en') Add a review @else اضافة تقييمك @endif</h5>
											</div>
											<form action="action" class="row" id="rate_form">
												<div class="col-md-4 col-12">
													<div class="rating">
														<span class="rating-title">@if(get_lang() == 'en') Your Rating @else تقييمك @endif : </span>
												<div class="star_rating">
                                                    <span class="gg" data-value="1"><i class="far fa-star"></i></span>
                                                    <span class="gg" data-value="2"><i class="far fa-star"></i></span> 
                                                    <span class="gg" data-value="3"><i class="far fa-star"></i></span>
                                                    <span class="gg" data-value="4"><i class="far fa-star"></i></span>
                                                    <span class="gg" data-value="5"><i class="far fa-star"></i></span>
                                                </div>
													</div>
												</div>
												<div class="col-md-12 col-12">
													<textarea rows="5" name="comment" id="comment"  placeholder="@if(get_lang()=='ar') التقييم @else Your Review* @endif"></textarea>
												</div>
												<div class="col-12">
													<button class="defult-btn" type="submit">@if(get_lang()=='en') Add a review @else اضافة تقييمك @endif</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							
							</div>   
    </div>
