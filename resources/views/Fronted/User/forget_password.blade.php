@extends('layouts.app')

@section('title')
    {{trans('main.reset_password')}}
    @endsection

@section('content')
    <!-- START SECTION BREADCRUMB -->
    <div class="breadcrumb_section bg_gray page-title-mini">
        <div class="container"><!-- STRART CONTAINER -->
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="page-title">
                        <h1>{{trans('main.reset_password')}}</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end">
                        <li class="breadcrumb-item"><a href="/">{{trans('main.home')}}</a></li>
                        <li class="breadcrumb-item active">{{trans('main.reset_password')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- END CONTAINER-->
    </div>
    <!-- END SECTION BREADCRUMB -->

    <!-- START MAIN CONTENT -->
    <div class="main_content">

        <!-- START LOGIN SECTION -->
        <div class="login_register_wrap section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-md-10">
                        <div class="login_wrap">
                            <div class="padding_eight_all bg-white">
                                <div class="heading_s1">
                                    <h3 id="title">{{trans('main.reset_password')}}</h3>
                                </div>
                                <form method="post" id="login_form">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" required="" class="form-control" name="email" id="email" placeholder="{{trans('main.Email_address')}}">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-fill-out btn-block" name="login">{{trans('main.reset_password')}}</button>
                                    </div>
                                </form>
                                @include('Fronted.User.code_form')
                                <div class="form-note text-center">{{trans('main.Already_have_an_account')}}<a href="{{route('User.show_login')}}">{{trans('main.login')}}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END LOGIN SECTION -->
        @include('include.suscribe')

    </div>
    <!-- END MAIN CONTENT -->

@endsection

@section('script')
    <script>
        $('#login_form').submit(function(e){
            e.preventDefault();
            $('#login').attr("disabled", true);
            $.toast().reset('all');
            Toset('{{trans("main.progress")}}','info','',false);
            var data={'email': $('#email').val()};

            $.ajax({
                url : '/User/reset_password',
                headers: { 'lang': '{{get_lang()}}'},
                type : 'get',
                data : data,
                success : function(data){
                    $.toast().reset('all');
                    $('#login').attr("disabled", 5000);
                    if(data.status == 0){
                        Toset(data.message,'error','',5000);
                    }else if(data.status == 1){
                        Toset(data.message,'success','',5000);
                        $('#login_form')[0].reset();
                        $('#login_form').slideUp(500);
                        $('#code_form').slideDown(500);
                        $('#title').text('{{trans("main.newPass")}}')
                    }
                },
                error : function(data)
                {
                    Toset('{{trans("main.serverError")}}','error','',5000);
                }
            })
        })
    </script>

    <script>
        $('#code_form').submit(function(e){
            e.preventDefault();
            $('#login').attr("disabled", true);
            $.toast().reset('all');
            Toset('{{trans("main.progress")}}','info','',false);
            var data={'code': $('#code').val() , 'password' : $('#password').val()};

            $.ajax({
                url : '/api/Auth_general/reset_password',
                headers: { 'lang': '{{get_lang()}}'},
                type : 'post',
                data : data,
                success : function(data){
                    $.toast().reset('all');
                    $('#login').attr("disabled", 5000);
                    if(data.status == 0){
                        Toset(data.message,'error','',5000);
                    }else if(data.status == 1){
                        Toset(data.message,'success','',5000);
                        location.href='{{route("User.show_login")}}'

                    }
                },
                error : function(data)
                {
                    Toset('{{trans("main.serverError")}}','error','',5000);
                }
            })
        })
    </script>
@endsection

