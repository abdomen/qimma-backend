<div class="featured-area">
    <div class="container">
        <div class="row">
            <div class="products-sec">
                <h2>{{trans('main.Featured_Products')}}</h2>
                <div class="feature-tab-area">

                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            @if(getProduct(1,4)->count() > 0)
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#home"
                               role="tab" aria-controls="nav-home" aria-selected="true">{{trans('main.New_Arrival')}}</a>
                            @endif
                            @if(getProduct(2,4)->count() > 0)
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#menu1" role="tab"
                               aria-controls="nav-profile" aria-selected="false">{{trans('main.Best_Sellers')}}</a>
                            @endif
                            @if(getProduct(3,4)->count() > 0)
                            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="menu2" role="tab"
                               aria-controls="nav-contact" aria-selected="false">{{trans('main.Popular')}}</a>
                            @endif
                            @if(getProduct(4,4)->count() > 0)
                            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#menu3" role="tab"
                               aria-controls="nav-contact" aria-selected="false">{{trans('main.Spacial_Offer')}}</a>
                                @endif
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="container">
                                <div class="row">
                                    @foreach(getProduct(1,4) as $row)
                                        {!! product_div($row) !!}
                                    @endforeach
                                </div>
                            </div>
                            <div class="feature_btn_area"><a href="{{route('productsFront.product_by_category')}}">
                                    <button class="cmn-btn">{{trans('main.More_Collections')}}</button>
                                </a></div>


                        </div>
                        <div class="tab-pane fade" id="menu1" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="container">
                                <div class="row">
                                    @foreach(getProduct(2,4) as $row)
                                    {!! product_div($row) !!}
                                        @endforeach
                                </div>
                            </div>
                            <div class="feature_btn_area"><a href="{{route('productsFront.product_by_category')}}">
                                    <button class="cmn-btn">{{trans('main.More_Collections')}}</button>
                                </a></div>


                        </div>
                        <div class="tab-pane fade" id="menu2" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="container">
                                <div class="row">
                                    @foreach(getProduct(3,4) as $row)
                                    {!! product_div($row) !!}
                                        @endforeach
                                </div>
                            </div>
                            <div class="feature_btn_area"><a href="{{route('productsFront.product_by_category')}}">
                                    <button class="cmn-btn">{{trans('main.More_Collections')}}</button>
                                </a></div>


                        </div>

                        <div class="tab-pane fade" id="menu3" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="container">
                                <div class="row">
                                    @foreach(getProduct(4,4) as $row)
                                        {!! product_div($row) !!}
                                    @endforeach
                                </div>
                            </div>
                            <div class="feature_btn_area"><a href="{{route('productsFront.product_by_category')}}">
                                    <button class="cmn-btn">{{trans('main.More_Collections')}}</button>
                                </a></div>


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
