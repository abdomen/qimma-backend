<div class="footer-area">
    <div class="container">
        <div class="row">
            <div class="footer_bx footer_bx1">
                <figure style="width: 100px;height: 100px"><img data-cfsrc="/assets/images/Logo.png" alt="logo"
                                                                style="display:none;visibility:hidden;"></figure>
                <p>
                    {{ get_lang()=='ar' ? substr(get_main_seetings()->about_ar,0,300) : substr(get_main_seetings()->about_en,0,300)}}

                </p>
                <div class="social_icon">
                    <a href="{{get_main_seetings()->facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a href="{{get_main_seetings()->twitter}}"><i class="fab fa-twitter"></i></a>
                    <a href="{{get_main_seetings()->snap}}"><i class="fab fa-snapchat"></i></a>
                    <a href="{{get_main_seetings()->instagram}}"><i class="fab fa-instagram"></i></a></div>
            </div>
            <div class="footer_bx links_txt">
                <h3>{{trans('main.Useful_Links')}}</h3>
                <ul class="footer_list">
                    <li><a href="/">{{trans('main.home')}}</a></li>
                    <li><a href="{{route('pages.about')}}">{{trans('main.About_Us')}}</a></li>
                    <li><a href="{{route('pages.Contact_Us')}}">{{trans('main.Contact_Us')}}</a></li>
                    @if(Auth::check() AND Auth::user()->my_wishlist->count() > 0)
                    <li><a href="{{route('Users.my_wishlist')}}">{{trans('main.Wishlist')}}</a></li>
                    @endif

                </ul>
            </div>
            @foreach(getCats(2,2) as $row)
                <div class="footer_bx deals_txt">
                    <h3>{{getTitleOrDesc($row,1)}}</h3>
                    <ul class="footer_list">
                        @foreach($row->product->take(3) as $pro)
                            <li>
                                <a href="{{route('productsFront.productDetails',$pro->id)}}">{{getTitleOrDesc($pro,1)}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
    </div>
</div>

<!--<div class="copyright_area">-->
<!--    <div class="container">-->
<!--        <div class="row"><span><a href="http://codecaique.com/">{{trans('main.All_Rights_Reserved_by_codecaique')}}</a></span>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->