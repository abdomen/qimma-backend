<div id="myModal" class="modal fade pro_popup" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="product_details clr">
                    <div class="product_details_img ">
                        <div class="images oflow"><a href="https://www.google.com" class="woocommerce-main-image zoom">
                                <img data-cfsrc="/assets/img/product6.png" class="attachment-shop_thumbnail" alt=""
                                     style="display:none;visibility:hidden;"/>
                                <noscript><img src="/assets/img/product6.png" class="attachment-shop_thumbnail" alt=""/></noscript>
                            </a>
                            <div class="thumbnails"><a href="#" class="zoom first"> <img data-cfsrc="/assets/img/product6.png"
                                                                                         class="attachment-shop_thumbnail"
                                                                                         alt=""
                                                                                         style="display:none;visibility:hidden;"/>
                                    <noscript><img src="/assets/img/product6.png" class="attachment-shop_thumbnail" alt=""/>
                                    </noscript>
                                </a> <a href="#" class="zoom first"> <img data-cfsrc="/assets/img/product8.png"
                                                                          class="attachment-shop_thumbnail" alt=""
                                                                          style="display:none;visibility:hidden;"/>
                                    <noscript><img src="/assets/img/product8.png" class="attachment-shop_thumbnail" alt=""/>
                                    </noscript>
                                </a> <a href="#" class="zoom first"> <img data-cfsrc="/assets/img/product7.png"
                                                                          class="attachment-shop_thumbnail" alt=""
                                                                          style="display:none;visibility:hidden;"/>
                                    <noscript><img src="/assets/img/product7.png" class="attachment-shop_thumbnail" alt=""/>
                                    </noscript>
                                </a> <a href="#" class="zoom first"> <img data-cfsrc="/assets/img/product9.png"
                                                                          class="attachment-shop_thumbnail" alt=""
                                                                          style="display:none;visibility:hidden;"/>
                                    <noscript><img src="/assets/img/product9.png" class="attachment-shop_thumbnail" alt=""/>
                                    </noscript>
                                </a></div>
                        </div>
                    </div>
                    <div class="product_details_txt">
                        <h5>Home / Fashion Trend <span>. Instock</span></h5>
                        <h2>/assets/imagesfire Shorts in Castaway</h2>
                        <h3>$19.00
                            <small>$26.00</small>
                        </h3>
                        <div class="rating_bx clr">
                            <div class="rating"><i class="far fa-star"></i> <i class="far fa-star"></i><i
                                        class="far fa-star"></i><i class="far fa-star"></i> <i class="far fa-star"></i>&nbsp;
                                &nbsp; Not Rated Yet
                            </div>
                            <div class="write_review"><a href="#"><i class="fa fa-edit"></i> Write A Review </a></div>
                        </div>
                        <div class="product_code"><span>Product Code: </span> EDM-0078F4</div>
                        <div class="product_code"><span>Brand: </span> Fabirex</div>
                        <div class="size_bx clr">
                            <div class="size size_radio">
                                <ul>
                                    <li>Size:</li>
                                    <li>
                                        <input type="radio" id="f0" name="cc"/>
                                        <label for="f0"><span>M</span></label>
                                    </li>
                                    <li>
                                        <input type="radio" id="f1" name="cc"/>
                                        <label for="f1"><span>L</span></label>
                                    </li>
                                    <li>
                                        <input type="radio" id="f2" name="cc"/>
                                        <label for="f2"><span>XL</span></label>
                                    </li>
                                </ul>
                            </div>
                            <div class="size colors color_radio">
                                <ul>
                                    <li>Color:</li>
                                    <li>
                                        <input type="radio" id="c1" name="cc"/>
                                        <label for="c1"><span></span></label>
                                    </li>
                                    <li>
                                        <input type="radio" id="c2" name="cc"/>
                                        <label for="c2"><span></span></label>
                                    </li>
                                    <li>
                                        <input type="radio" id="c3" name="cc"/>
                                        <label for="c3"><span></span></label>
                                    </li>
                                    <li>
                                        <input type="radio" id="c4" name="cc"/>
                                        <label for="c4"><span></span></label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="add_btn_area">
                            <div class="qty_bx">
                                <div class="sp-quantity">
                                    <div class="sp-minus fff"><a class="ddd" href="javascript:void(0)">-</a></div>
                                    <div class="sp-input">
                                        <input type="text" class="quntity-input" value="1"/>
                                    </div>
                                    <div class="sp-plus fff"><a class="ddd" href="javascript:void(0)">+</a></div>
                                </div>
                                <div class="share_product"><a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                              id="social_popop"><i class="fa fa-share-alt"></i></a>
                                    <div class="dropdown-menu social_pop" aria-labelledby="social_popop">
                                        <h4>Share on:</h4>
                                        <a href="#"><i class="fab fa-facebook-f"></i></a> <a href="#"><i
                                                    class="fab fa-twitter"></i></a> <a href="#"><i
                                                    class="fab fa-pinterest-square"></i></a> <a href="#"><i
                                                    class="fab fa-instagram"></i></a></div>
                                </div>
                            </div>
                            <div class="btn_bx">
                                <button class="cmn-btn">Add to cart</button>
                            </div>
                            <div class="add_wishlist"><i class="fa fa-heart"></i> <a href="#">Add To Wishlist</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
