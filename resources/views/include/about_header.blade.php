<div class="welcome_area">
    <div class="container">
        <div class="row">
            <div class="about_shipping">
                <ul>
                    <li>
                        <figure><img data-cfsrc="/assets/images/icon_shipping.png" alt="happy cutomer"
                                     style="display:none;visibility:hidden;">
                            <noscript><img src="/assets/images/icon_shipping.png" alt="happy cutomer"></noscript>
                        </figure>
                        <h4>{{trans('main.Shipping')}}<span>
                                {{get_lang() =='ar' ? get_main_seetings()->free_deliviry_ar : get_main_seetings()->free_deliviry_en}}
                            </span></h4>
                    </li>
                    <li>
                        <figure><img data-cfsrc="/assets/images/icon_support.png" alt="happy cutomer"
                                     style="display:none;visibility:hidden;">
                            <noscript><img src="/assets/images/icon_support.png" alt="happy cutomer"></noscript>
                        </figure>
                        <h4>{{trans('main.Support')}}<span>
                            {{get_lang() =='ar' ? get_main_seetings()->Support_ar : get_main_seetings()->Support_en}}
                            </span></h4>
                    </li>
                    <li>
                        <figure><img data-cfsrc="/assets/images/icon_partner.png" alt="happy cutomer"
                                     style="display:none;visibility:hidden;">
                            <noscript><img src="/assets/images/icon_partner.png" alt="happy cutomer"></noscript>
                        </figure>
                        <h4>{{trans('main.Guarantee')}}<span>
                                {{get_lang() =='ar' ? get_main_seetings()->Guarantee_ar : get_main_seetings()->Guarantee_en}}
                            </span></h4>
                    </li>
                   
                </ul>
            </div>
            <div class="welcome_screen">
                <div class="welcome_head">
                    <div class="site-section-area">
                        <h2>{{trans('main.Welcome_To')}}
                            {{ get_lang()=='ar' ? get_main_seetings()->name_ar : get_main_seetings()->name_en}}
                        </h2>

                    </div>
                </div>
                <div class="welcome_para">
                    <p>
                        {{ get_lang()=='ar' ? substr(get_main_seetings()->about_ar,0,300) : substr(get_main_seetings()->about_en,0,300)}}
                    </p>

                    <p>
                        <button onclick="location.href='/pages/About_us'" class="cmn-btn2">{{trans('main.More_Information')}}</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
