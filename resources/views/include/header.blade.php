<header>
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="header-top-left">
                        <ul>
                            <li><i class="fa fa-mobile" aria-hidden="true"></i>+{{get_main_seetings()->phone}}</li>
                            <li><i class="fas fa-envelope"></i><a href="mailto:info@{{get_main_seetings()->email}}">{{get_main_seetings()->email}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="header-top-right">
                        <div class="lang">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="lang_option">
                                @if(get_lang() =='en')
                                    English
                                @else
                                    العربية
                                @endif
                                 </a>
                            <div class="dropdown-menu" aria-labelledby="lang_option">
                                <a href="{{ LaravelLocalization::getLocalizedUrl(get_lang()=='ar' ? "en" : "ar") }}">
                                    @if(get_lang() =='ar')
                                        English
                                        @else
                                    Arabic
                                        @endif
                                </a>

                            </div>
                        </div>
                        @if(!Auth::check())
                            <div class="account">
                                <a href="{{route('User.show_login')}}"><i class="fas fa-lock"></i> {{trans('main.login')}} </a>
                            </div>
                            <div class="account"><a href="{{route('Users.show_Register')}}">
                                    <i class="fa fa-user"></i> {{trans('main.Register')}}</a>

                            </div>
                        @else
                            <div class="account"><a href="{{route('Users.logout')}}">
                                    <i class="fa fa-user"></i> {{trans('main.logout')}}</a>

                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">

            <a class="navbar-brand" href="#" style="width: 100px;height: 100px"><img src="/assets/images/Logo.png"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">


                <ul class="navbar-nav ml-auto">
                    <li class="nav-item @if(isset($active) and $active==1) active @endif">
                        <a class="nav-link" href="/">{{trans('main.home')}} <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item @if(isset($active) and $active==3) active @endif">
                        <a class="nav-link" href="{{route('pages.about')}}">{{trans('main.About_Us')}}</a>
                    </li>

                    <li class="nav-item @if(isset($active) and $active==22) active @endif">
                        <a class="nav-link" href="{{route('pages.Contact_Us')}}">{{trans('main.Contact_Us')}}</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0" action="{{route('pages.search')}}">
                    <div class="search_area">
                        <input type="text" name="text" required placeholder="{{trans('main.search')}}">
                        <button></button>
                    </div>
                </form>
                @if(Auth::check())
                <div class="btn-wrap">
                    <div class="mini-cart-wrap ">
                        <div class="mini-cart">
                            <div class="mini-cart-icon">
                                <i class="fas fa-shopping-cart">
                                    @if(Auth::user()->my_cart->count() > 0)
                                        <span class="badge">
                                            {{Auth::user()->my_cart->count()}}
                                        </span>
                                    @endif

                                </i>
                                <span>{{trans('main.cart')}}</span>
                                <span style="color: #42ADC1">
                                    @if(Auth::user()->my_cart->count() > 0) $
                                {{\App\Http\Controllers\Manage\BaseController::get_total_price(Auth::user()->my_cart,Auth::user())}}
                                @endif
                                </span>
                            </div>

                            @if(Auth::user()->my_cart->count() > 0)
                            <div class="widget-shopping-cart-content">
                                <ul class="cart-list">
                                    @foreach(Auth::user()->my_cart as $key=>$row)
                                        @if($key==2) @break @endif

                                        <li>
                                        <a href="#" onclick="delete_from_cart('{{$row->id}}')" class="remove">×</a>
                                        <a href="{{route('productsFront.productDetails',$row->id)}}">
                                            <img src="{{getImageUrl('Products',$row->image)}}" alt="">
                                            {{getTitleOrDesc($row,1)}}&nbsp;
                                        </a>
                                        <span class="quantity">{{$row->pivot->quantity}} ×
                                            ${{\App\Http\Controllers\Manage\BaseController::get_price(Auth::user(),$row)}}</span>
                                    </li>
                                    @endforeach
                                </ul>
                                <p class="total">
                                    <strong>{{trans('main.Cart_Subtotal')}}:</strong>
                                    <span class="amount">${{\App\Http\Controllers\Manage\BaseController::get_total_price(Auth::user()->my_cart,Auth::user())}}</span>
                                </p>
                                <p class="buttons">
                                    <a href="{{Route('Users.my_cart')}}" class="view-cart">{{trans('main.View_Cart')}}</a>
                                    <a href="{{Route('Users.make_order')}}" class="checkout">{{trans('main.Checkout')}}</a>
                                </p>
                            </div>
                                @endif
                        </div>


                    </div>

                </div>
                    @endif

            </div>
        </div>

    </nav>


</header>
