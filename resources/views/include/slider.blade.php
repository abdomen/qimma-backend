@php
    $sliders=\App\Models\Slider::get();
@endphp
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        @foreach($sliders as $key=>$row)
        <div class="carousel-item @if($key==0) active @endif">
            <img src="{{getImageUrl('Slider',$row->image)}}" height="500" class="d-block w-100" alt="...">
        </div>
            @endforeach

    </div>
</div>