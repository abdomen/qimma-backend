<?php

use Illuminate\Http\Request;



    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    //notAllwed
    Route::get('/notAllwed/{check}','Api\Admin\BaseController@notAllwed');



        //Test Role
        Route::group(['middleware' => 'testRole' , 'roles' => 'Admin'], function () 
    {
        // lang Routes
        Route::prefix('city')->group(function()
        {
            Route::get('/view', 'Api\Admin\CityController@view');
            Route::post('/store', 'Api\Admin\CityController@store');
            Route::get('/show/{id}', 'Api\Admin\CityController@show');
            Route::post('/update/{id}', 'Api\Admin\CityController@update');
            Route::post('/delete/{id}', 'Api\Admin\CityController@delete');
        });


        // lang Routes
        Route::prefix('Section')->group(function()
        {
            Route::get('/view', 'Api\Admin\SectionController@view');
            Route::post('/store', 'Api\Admin\SectionController@store');
            Route::get('/show/{id}', 'Api\Admin\SectionController@show');
            Route::post('/update/{id}', 'Api\Admin\SectionController@update');
            Route::post('/delete/{id}', 'Api\Admin\SectionController@delete');
        });

        // lang Routes
        Route::prefix('Currency')->group(function()
        {
            Route::get('/view', 'Api\Admin\CurrencyController@view');
            Route::post('/store', 'Api\Admin\CurrencyController@store');
            Route::get('/show/{id}', 'Api\Admin\CurrencyController@show');
            Route::post('/update/{id}', 'Api\Admin\CurrencyController@update');
            Route::post('/delete/{id}', 'Api\Admin\CurrencyController@delete');
        });


        // lang Routes
        Route::prefix('Brand')->group(function()
        {
            Route::get('/view', 'Api\Admin\BrandController@view');
            Route::post('/store', 'Api\Admin\BrandController@store');
            Route::get('/show/{id}', 'Api\Admin\BrandController@show');
            Route::post('/update/{id}', 'Api\Admin\BrandController@update');
            Route::post('/delete/{id}', 'Api\Admin\BrandController@delete');
        });


        // lang Routes
        Route::prefix('Sliders')->group(function()
        {
            Route::get('/view', 'Api\Admin\SlidersController@view');
            Route::post('/store', 'Api\Admin\SlidersController@store');
            Route::get('/show/{id}', 'Api\Admin\SlidersController@show');
            Route::post('/update/{id}', 'Api\Admin\SlidersController@update');
            Route::post('/delete/{id}', 'Api\Admin\SlidersController@delete');
        });


        // Category Routes
        Route::prefix('Category')->group(function()
        {
            Route::get('/view', 'Api\Admin\CategoryController@view');
            Route::post('/store', 'Api\Admin\CategoryController@store');
            Route::get('/show/{id}', 'Api\Admin\CategoryController@show');
            Route::post('/update/{id}', 'Api\Admin\CategoryController@update');
            Route::post('/delete/{id}', 'Api\Admin\CategoryController@delete');
        });
        

        

    });

        //Auth Jwt
        Route::middleware('jwt.auth')->group(function()
    {

    });

