<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;


header('Access-Controlplicatio-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: apn/json; charset=UTF-8', true);

Route::get('/test', 'Fronted\ProductsController@test');


Auth::routes();
Route::group(['prefix' => LaravelLocalization::setLocale(),

    'middleware' => ['localeSessionRedirect','localizationRedirect','localeViewPath']] ,  function()

{
    Route::get('/' , function(){
        return view('home');
    })->name('/');
    Route::get('/testtt', 'Fronted\UserController@testtt')->name('Users.redirect');


    /** Products front routes */
    Route::prefix('Products')->group(function()
    {
        Route::get('/product_details/{product_id}', 'Fronted\ProductsController@product_details')->name('productsFront.productDetails');
        Route::get('/product_by_category', 'Fronted\ProductsController@product_by_category')->name('productsFront.product_by_category');
        Route::get('/get_model/{product_id}', 'Fronted\ProductsController@get_model')->name('productsFront.get_model');
    });

    /** Products front routes */
    Route::prefix('pages')->group(function()
    {
        Route::get('/About_us', 'Fronted\PagesController@About_us')->name('pages.about');
        Route::get('/Contact_Us', 'Fronted\PagesController@Contact_Us')->name('pages.Contact_Us');
        Route::get('/submit_message', 'Api\GeneralController@contact_us')->name('pages.submit_message');
        Route::get('/subscribe', 'Fronted\PagesController@subscribe')->name('pages.subscribe');
        Route::get('/search', 'Fronted\PagesController@search')->name('pages.search');
    });

    /** Auth front routes */
    Route::prefix('User')->group(function()
    {
        Route::get('/show_login', 'Fronted\UserController@show_login')->name('User.show_login');
        Route::get('/forget_password', 'Fronted\UserController@forget_password')->name('User.forget_password');
        Route::get('/login', 'Api\UserController@login')->name('user.login');
        Route::get('/reset_password', 'Api\UserController@forget_password')->name('User.reset_password');
        Route::get('/logout', 'Fronted\UserController@logout')->name('Users.logout');
        Route::get('/register', 'Api\UserController@register')->name('user.register');
        Route::get('/add_address', 'Api\UserController@add_address')->name('user.add_address');
        Route::get('/show_Register', 'Fronted\UserController@show_Register')->name('Users.show_Register');
        Route::get('/save_my_rate/{product_id}', 'Api\RateCommentController@save_my_rate')->name('rate_comment.save_my_rate');
        Route::get('/add_to_cart/{product_id}', 'Api\ProductController@add_to_cart')->name('Users.add_to_cart');
        Route::get('/delete_from_cart/{product_id}', 'Api\ProductController@delete_from_cart')->name('Users.delete_from_cart');
        Route::get('/my_wishlist', 'Fronted\UserController@my_wishlist')->name('Users.my_wishlist');
        Route::get('/my_cart', 'Fronted\UserController@my_cart')->name('Users.my_cart');
        Route::get('/orderSuccess', 'Fronted\UserController@orderSuccess')->name('Users.orderSuccess');
        Route::get('/Checkout', 'Fronted\UserController@make_order')->name('Users.make_order');
        Route::get('/wishlist/{product_id}', 'Api\ProductController@wishlist')->name('Users.wishlist');
        Route::get('/make_my_order', 'Api\OrderController@add_order_once')->name('Users.make_orders');
        Route::get('/check_discount_code', 'Api\OrderController@check_discount_code')->name('Users.make_orders');
        Route::get('/update_cart', 'Fronted\UserController@update_cart')->name('Users.update_cart');


    });
});
