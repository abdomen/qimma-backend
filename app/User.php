<?php

namespace App;

use App\Models\order_locations;
use App\Models\Rep_Inv;
use App\Models\Safe;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;


class User extends Authenticatable
{


    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function my_wishlist()
    {
        return $this->belongsToMany('App\Models\Product_details','whishlists','user_id','product_detail_id');
    }

    public function my_cart()
    {
        return $this->belongsToMany('App\Models\Product_details','carts','user_id','product_detail_id')
            ->withPivot('color_id','size_id','quantity')->where('carts.type',0);
    }

    // public function my_cart()
    // {
    //     return $this->belongsToMany('App\Models\Product_details','carts','user_id','product_detail_id')
    //         ->withPivot('color_id','size_id','quantity')->where('carts.type',0);
    // }

    public function user_rate()
    {
        return $this->belongsToMany('App\Models\Product_details','rates','user_id','product_detail_id')
            ->withPivot('rate');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order','user_id');
    }

    public function P_d_orders()
    {
        return $this->hasMany(P_d_orders::class,'user_id');
    }

    public function orders_representative()
    {
        return $this->hasMany('App\Models\Order','user_id');
    }

    public function representativeOfUser()
    {
        return $this->hasMany(Representative_User::class,'representative_id');
    }

    public function userOfRepresentative()
    {
        return $this->hasMany(Representative_User::class,'user_id');
    }

    public function my_address()
    {
        return $this->hasMany('App\Models\order_locations','user_id');
    }

    public function user_code()
    {
        return $this->belongsToMany('App\Models\Discount','user__discounts','user_id','code_id')
            ->withPivot('is_use');
    }

    public function user_address()
    {
        return $this->hasMany(order_locations::class,'user_id');
    }

    public function Rep_Inv()
    {
        return $this->hasMany(Rep_Inv::class,'representative_id');
    }


    public function safe()
    {
        return $this->belongsTo(Safe::class,'safe_id');
    }
}
 

