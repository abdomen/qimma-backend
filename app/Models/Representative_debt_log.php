<?php

namespace App\Models;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Representative_debt_log extends Model
{
    protected $table = 'rep_debt_log';

    public function representative()
    {
        return $this->belongsTo(User::class,'representative_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
