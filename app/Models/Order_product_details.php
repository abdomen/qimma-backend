<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_product_details extends Model
{
    protected $table = 'order_product_details';

    protected $fillable = [
        'P_d_order_id',
        'product_detail_id',
        'quantity',
        'price',
        'color_id',
        'size_id',
    ];

    public function Order()
    {
        return $this->belongsTo(P_d_orders::class , 'p_d_order_id');
    }

    public function Product_detail()
    {
        return $this->belongsTo(Product_details::class , 'product_detail_id');
    }

    public function Color()
    {
        return $this->belongsTo(color::class , 'color_id');
    }

    public function Size()
    {
        return $this->belongsTo(Sizes::class , 'size_id');
    }
}
