<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group_discount extends Model
{
    protected $table = 'group_discount';
    
    public function group()
    {
        return $this->belongsTo(Groups::class);
    }

}
