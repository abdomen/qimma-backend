<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Group_gallery;

class Groups extends Model
{
    public function images_gallery()
    {
        return $this->hasMany(Group_gallery::class,'group_id');
    }

    public function product()
    {
        return $this->hasMany(Product_group::class,'group_id'); 
    }

    public function discount()
    {
        return $this->hasMany(Group_discount::class,'group_id');
    }
    
}
