<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    //
    public function user_code()
    {
        return $this->belongsToMany('App\User','user__discounts','code_id','user_id');
    }
}
