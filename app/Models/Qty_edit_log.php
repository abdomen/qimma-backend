<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Qty_edit_log extends Model
{
    protected $table='qty_edit_log';

    public function Product_detail()
    {
        return $this->belongsTo(Product_details::class , 'product_detail_id');
    }

    public function admin()
    {
        return $this->belongsTo(Shop::class,'admin_id');
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class , 'inventory_id');
    }
}
