<?php

namespace App\Models;
use App\User;
use App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class User_debt_log extends Model
{
    protected $table = 'user_debt_log';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function admin()
    {
        return $this->belongsTo(Shop::class,'admin_id');
    }
}
