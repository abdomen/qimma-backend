<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_group extends Model
{
    
    protected $table = 'product_group';
    
    public function group()
    {
        return $this->belongsTo(Groups::class ,'group_id');
    }


    public function product()
    {
        return $this->belongsTo(Products::class,'product_id');
    }
}
