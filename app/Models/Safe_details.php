<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Safe_details extends Model
{
    protected $table = 'safe_details';

    public function safe()
    {
        return $this->belongsTo(Safe::class,'safe_id');
    }
}
