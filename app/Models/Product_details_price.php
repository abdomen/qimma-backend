<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_details_price extends Model
{
    Protected $table = 'product_details_price';

    public function Product_detail()
    {
        return $this->belongsTo(Product_details::class , 'product_detail_id');
    }

    public function Price()
    {
        return $this->belongsTo(Price::class , 'price_id');
    }
}
