<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Representative_User extends Model
{
    protected $table = 'representative_user';

    public function Representative()
    {
        return $this->belongsTo(User::class , 'representative_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class , 'user_id');
    }
}
