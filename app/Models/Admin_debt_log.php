<?php

namespace App\Models;
use App\User;
use App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class Admin_debt_log extends Model
{
    protected $table = 'admin_debt_log';

    public function representative()
    {
        return $this->belongsTo(User::class,'representative_id');
    }

    public function admin()
    {
        return $this->belongsTo(Shop::class,'admin_id');
    }
}
