<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventory';
    
    public function products()
    {
        return $this->hasMany(Products::class,'inventory_id');
    }

    public function Rep_Inv()
    {
        return $this->hasMany(Rep_Inv::class,'inventory_id');
    }

    public function product_detail()
    {
        return $this->hasMany(Product_detail_inventory::class , 'inventory_id');
    }

}
