<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Order extends Model
{
    //
    public function products(){
        return $this->belongsToMany('App\Models\Products','carts','order_id','product_id')
            ->withPivot('size_id','color_id','quantity')->where('carts.type',1);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function representative()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\order_locations','address_id');
    }

}
