<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Order_return extends Model
{
    protected $table = 'order_return';

    public function Order()
    {
        return $this->belongsTo(P_d_orders::class , 'p_d_order_id');
    }

    public function Product_detail()
    {
        return $this->belongsTo(Product_details::class , 'product_detail_id');
    }

    public function representative()
    {
        return $this->belongsTo(User::class , 'representative_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id');
    }
}
