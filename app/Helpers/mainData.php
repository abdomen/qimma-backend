<?php
/**
 * @return mixed
 */
function get_lang()
{
    return Request::segment(1);
}

/**
 * @param $type
 * @return \App\Models\Category[]|\Illuminate\Database\Eloquent\Collection
 */
function getCats($type,$count)
{
    $cats=\App\Models\Category::whereHas('product')->orderBy('id','desc')->where('id','!=',1);
    if($type == 2){
        $cats=$cats->take($count)->get();
    }
    if($type == 3){
        $cats=$cats->skip($count)->take($count)->get();
    }
    return $cats;
}

/**
 * @param $type
 * @param $take
 * @return mixed
 */
function getProduct($type,$take)
{
    $products=\App\Models\Products::where('status',1);
    if($type==1){
        $products=$products->orderBy('created_at','desc');
    }
    if($type==2){
        $products=$products->whereHas('orders')->withCount('orders')->orderBy('orders_count','desc');
    }
    if($type==3){
        $products=$products->whereHas('user_favorite')->withCount('user_favorite')->orderBy('user_favorite_count','desc');
    }
    if($type==4){
        $products=$products->where('is_offer',1);
    }
    $products=$products->whereHas('cat',function($q){
            $q->where('status',1);
        })->take($take)->get();
    return $products;
}

/**
 * @param $row
 * @param int $type
 * @return mixed
 */
function getTitleOrDesc($row,int $type)
{
    if($type==1)
        $data=get_lang() == 'ar' ? $row->name_ar : $row->name_en;
    if($type==2)
        $data=get_lang() == 'ar' ? $row->desc_ar : $row->desc_en;
    return $data;
}

/**
 * @param $type
 * @param $count
 * @return mixed
 */
function get_ads($type,$count)
{
    $ads=\App\Models\Ads::where('place_type',$type)->take($count)->get();
    return $ads;
}

/**
 * @param $row
 * @return string
 */
function product_price($row)
{
    $request=new \Illuminate\Http\Request();
    $price_v1=$row->price_product->count() > 0 ? $row->price_product[0]->pivot->price : 0;
    $price_after_amount=$row->price_product->count() > 0 ? $row->price_product[0]->pivot->price_after_offer : 0;
    if($row->is_offer == 1) {
        $price=$price_after_amount;
    }else{
        $price=$price_v1;

    }
    if(isset($_GET['price'])){
        $price=111;
    }

    $price_div= '<h2>$'.$price_v1.'</span>';
    if($row->is_offer==1) {
        $price_div .= '<span class="small"> $'.$price_after_amount.'</span>';
    }
    $price_div.='</h2>';
    return $price_div;
}

/**
 * @param $folder
 * @param $image
 * @return string
 */
function getImageUrl($folder,$image){
    if($image)
        return '/images/'.$folder .'/'.$image;
    return '/images/1.png';
}


function product_div($row)
{
 $product='<div class="col-lg-3 col-md-6 col-sm-12">'.
     '<div class="featured-item">'.
              '<figure><img data-cfsrc="'.getImageUrl("Products",$row->image).'" alt="featured image"style="display:none;visibility:hidden;">'.
                  '<noscript><img src="'.getImageUrl("Products",$row->image).'" alt="featured image"></noscript>'.
                                               '<div class="featured-overlay">'.
                                                    '<div class="featured-overlay-des">'.
                                                        '<div class="featured-overlay-rht"><ul>'.
                                                                '<li><a   href="'.route("productsFront.productDetails",$row->id).'" >'.
                                                                    '<i class="fa fa-search"></i></a><span>'.trans("main.quick_View").'</span></li>'.
                                                                '<li><a  onclick="add_to_cart('.$row->id.')"><i class="fa fa-shopping-basket"></i></a><span>'.trans("main.add_t_cat").'</span></li>'.
                                                                '</li><li><a  onclick="wishlist('.$row->id.')"><i class="fa fa-heart"></i></a><span>'.trans("main.Wishlist").'</span>'.
                                                                '</li>'.
                                                            '</ul>'.
                                                        '</div></div></div></figure>'.
                                            '<div class="featured-des">'.
                                                '<h4><a href="'.route("productsFront.productDetails",$row->id).'">'.getTitleOrDesc($row,1).'</a></h4>'
                                                .product_price($row).
                                            '</div></div></div>';
 return $product;
}


function getPrice($type)
{
    $price=\App\Http\Controllers\Manage\BaseController::get_total_price(Auth::user()->my_cart,Auth::user());
    $shipping_price=\App\Models\Settings::first()->shipping_price;
    $total_price=Auth::user()->catr_price ? Auth::user()->catr_price : $price + $shipping_price;
    if($type==1)
        return $price;
    if($type==2)
        return $shipping_price;

    return $total_price;
}

function get_main_seetings()
{
    return \App\Models\Settings::first();
}


function img_div($row){
    $style='';
    if(Auth::check())
    {
        $style=Auth::user()->my_wishlist->contains($row->id) ? 'color : red' : '';
    }
    $image_div='<div class="product_img">'.
        '<a href="'.route("productsFront.productDetails",$row->id).'">'.
        '<img src="'.getImageUrl("Products",$row->image).'" height="300" width="270" alt="furniture_img1"></a>'.
        '<div class="product_action_box">'.
        '<ul class="list_none pr_action_btn">'.
        '<li><a onclick="add_to_cart('.$row->id.')"><i class="icon-basket-loaded"></i></a></li>'.
        '<li><a href="/Products/get_model/'.$row->id.'" class="popup-ajax"><i class="icon-magnifier-add"></i></a></li>'.
        '<li><a onclick="wishlist('.$row->id.')"><i class="icon-heart" id="whish_list_style_'.$row->id.'" style="'.$style.'"></i></a></li></ul></div></div>';
    return $image_div;
}

function rate($row){
    $num=$row->rate * 100 /5;
    $rate='<div class="rating_wrap"><div class="rating">'.
        '<div class="product_rate" style="width:'.$num.'%"></div></div>'.
        '<span class="rating_num">('.$row->user_rate->count().')</span></div>';
    return $rate;
}
