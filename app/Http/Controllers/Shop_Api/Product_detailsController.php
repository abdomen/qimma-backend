<?php

namespace App\Http\Controllers\Shop_Api;

use App\Models\Product_details;
use App\Models\Products;
use App\Models\Price;
use App\Models\Inventory;
use App\Models\Product_details_price;
use App\Models\Product_detail_inventory;
use App\Models\Currency;
use App\Models\Product_Currency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\Product_detailsResource;
use App\Http\Resources\Product_details_priceResource;
use App\Http\Resources\Product_detail_inventoryResource;
use App\Http\Resources\Product_detailsCollectionResource;
use App\Http\Resources\InventoryResource;
use App\Models\Product_image;
use App\Models\Product_color;
use App\Models\Produc_view_user;
use App\Models\Product_Size;
use App\Http\Resources\Inventory_product_details_resource;

class Product_detailsController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new Product_details to database
     */
    public function add_Product_details(Request $request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        $validate_Product_details=$this->validate_Product_details_add($request);
        if(isset($validate_Product_details)){
            return $validate_Product_details;
        }
        
        $product_id = $request->product_id;
        $proex = Products::find($product_id);
        $check=$this->not_found($proex,'المنتج','Product',$lang);
        if(isset($check)){
            return $check;
        }

        $product_details=new Product_details();
        $product_details->product_id=$product_id;
        $product_details->difference_en=$request->difference_en;
        $product_details->difference_ar=$request->difference_ar;
        $product_details->status=$request->status;
        $product_details->is_offer=$request->is_offer;
        $product_details->offer_amount=$request->offer_amount;
        $product_details->desc_ar=$request->desc_ar;
        $product_details->desc_en=$request->desc_en;
        $product_details->lat=$request->lat;
        $product_details->lng=$request->lng;
        $product_details->barcode=$request->barcode;
        $product_details->price=$request->selling_price;
        if($request->image){
            $name=BaseController::saveImage('Product_image',$request->file('image'));
            $product_details->image=$name;
        }
        $product_details->save();

        $product_details_id = $product_details->id;

        $Price=new Price();
        $Price->purchasing_price=$request->purchasing_price;
        $Price->wholesale_wholesale_price=$request->wholesale_wholesale_price;
        $Price->wholesale_price=$request->wholesale_price;
        $Price->selling_price=$request->selling_price;
        $Price->save();

        $Price_id = $Price->id;

        $Product_details_price=new Product_details_price();
        $Product_details_price->product_detail_id=$product_details_id;
        $Product_details_price->price_id=$Price_id;
        $Product_details_price->save();

        $inventory_id = $request->inventory_id;
        $inventoryex = Inventory::find($inventory_id);
        $check=$this->not_found($inventoryex,'المخزن','Inventory',$lang);
        if(isset($check)){
            return $check;
        }
        
        $check=$this->not_found($inventoryex,'المخزن','Inventory',$lang);
        if(isset($check)){
            return $check;
        }
        $Product_detail_inventory=new Product_detail_inventory();
        $Product_detail_inventory->inventory_id=$inventory_id;
        $Product_detail_inventory->product_detail_id=$product_details_id;
        $Product_detail_inventory->quantity=$request->quantity;
        $Product_detail_inventory->save();

        $this->addDefualPrice($product_details_id,$request->selling_price);
        BaseController::set_offer($product_details_id);
        
        $lang = $request->header('lang');
        $msg = $lang=='ar' ? 'تم اضافة تفاصيل المنتج بنجاح'  : 'Product_details added successfully';
        return $this->apiResponseData(new Product_detailsResource($product_details),$msg);

    }

    /*
     * Add Products Defualt Price price
     */
    public function addDefualPrice($product_id,$price){
        $currency=Currency::where('defualt',1)->first();
        $currency=is_null($currency) ? Currency::first() : $currency;
        $Product_Currency=new Product_Currency;
        $Product_Currency->product_detail_id=$product_id;
        $Product_Currency->currency_id=$currency->id;
        $Product_Currency->price=$price;
        $Product_Currency->save();
    }

    /*
     * Edit product
    */
    public function edit_Product_details(Request $request,$Product_details_id)
    {
        $lang = $request->header('lang');
        $Product_details = Product_details::find($Product_details_id);
        $check=$this->not_found($Product_details,'تفاصيل المنتج','Product_details',$lang);
        if(isset($check)){
            return $check;
        }

        $Price_id = Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->value('price_id');
        $price = Price::find($Price_id);
        
        $Product_details->difference_en=$request->difference_en;
        $Product_details->difference_ar=$request->difference_ar;
        if ($request->filled('status')){
            $Product_details->status=$request->status;
        }else{
            $Product_details->status=$Product_details->status;
        }
        if ($request->filled('is_offer')){
            $Product_details->is_offer=$request->is_offer;
        }else{
            $Product_details->is_offer=$Product_details->is_offer;
        }
        $Product_details->offer_amount=$request->offer_amount;
        $Product_details->desc_ar=$request->desc_ar;
        $Product_details->desc_en=$request->desc_en;
        $Product_details->lat=$request->lat;
        $Product_details->lng=$request->lng;
        $Product_details->barcode=$request->barcode;
        $Product_details->price=$request->selling_price;
        if($request->image){
            BaseController::deleteFile('Product_image',$Product_details->image);
            $name=BaseController::saveImage('Product_image',$request->file('image'));
            $Product_details->image=$name;   
        }
        $Product_details->save();

        $price->purchasing_price=$request->purchasing_price;
        $price->wholesale_wholesale_price=$request->wholesale_wholesale_price;
        $price->wholesale_price=$request->wholesale_price;
        $price->selling_price=$request->selling_price;
        $price->save();

        // if ($request->filled('inventory_id')){
        //     $Product_details->status=$request->status;
        // }else{
        //     $Product_details->status=$Product_details->status;
        // }
        // $inventory_id = $request->inventory_id;
        // $inventoryex = Inventory::find($inventory_id);
        // $check=$this->not_found($inventoryex,'المخزن','Inventory',$lang);
        // if(isset($check)){
        //     return $check;
        // }

        // $Product_detail_inventory = Product_detail_inventory::where('product_detail_id' , '=' , $Product_details_id)->first();

        // $Product_detail_inventory->inventory_id=$inventory_id;
        // $Product_detail_inventory->quantity=$request->quantity;
        // $Product_detail_inventory->save();

        BaseController::set_offer($Product_details_id);

        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم تعديل تفاصيل المنتج بنجاح'  : 'Product_details edited successfully';
        return $this->apiResponseData(new Product_detailsResource($Product_details),$msg);
    }


    /*
     * get All product for Auth shop
     */
    public function all_Product_details(Request $request)
    {
        $user=Auth::user();
        $page=$request->page * 20;
        $Product_details=Product_details::orderBy('id','DESC')->get();
        return $this->apiResponseData(Product_detailsResource::collection($Product_details),'success');
    }


    /*
     * Show single Product_details
     */
    public function single_Product_details(Request $request,$Product_details_id){
        $lang=$request->header('lang');
        $Product_details=Product_details::find($Product_details_id);
        $check=$this->not_found($Product_details,'تفاصيل المنتج','Product_details',$lang);
        if(isset($check)){
            return $check;
        }



        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new Product_detailsCollectionResource($Product_details),$msg);
    }


    public function product_Product_details(Request $request,$product_id){
        $lang=$request->header('lang');
        $Product_details=Product_details::where('product_id',$product_id)->orderBy('id' , 'DESC')->get();
        $check=$this->not_found($Product_details,' المنتج','Product',$lang);
        if(isset($check)){
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(Product_detailsResource::collection($Product_details),$msg);
    }

    /*
     * Delete Product_details ..
     */

    public function delete_Product_details(Request $request,$Product_details_id){
        $lang=$request->header('lang');
        $Product_details=Product_details::where('id',$Product_details_id)->first();
        $check=$this->not_found($Product_details,'تفاصيل المنتج','Product_details',$lang);
        if(isset($check)){
            return $check;
        }

        if(Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->exists()){
            $Product_details_price = Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->first();

            if(Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->value('price_id') != null){
                
                $Price_id = Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->value('price_id');
                if(Price::where('id',$Price_id)->exists()){Price::where('id',$Price_id)->delete();}
                
            }

            $Product_details_price->delete();
        }
        
        if(Product_detail_inventory::where('product_detail_id' , '=' , $Product_details_id)->exists()){
            Product_detail_inventory::where('product_detail_id' , '=' , $Product_details_id)->delete();
        }

        if(Product_Currency::where('product_detail_id' , '=' , $Product_details_id)->exists()){
            Product_Currency::where('product_detail_id' , '=' , $Product_details_id)->delete();
        }

        if(Product_color::where('product_detail_id' , '=' , $Product_details_id)->exists()){
            Product_color::where('product_detail_id' , '=' , $Product_details_id)->delete();
        }

        if(Produc_view_user::where('product_detail_id' , '=' , $Product_details_id)->exists()){
            Produc_view_user::where('product_detail_id' , '=' , $Product_details_id)->delete();
        }

        if(Product_Size::where('product_detail_id' , '=' , $Product_details_id)->exists()){
            Product_Size::where('product_detail_id' , '=' , $Product_details_id)->delete();
        }

        if(Product_image::where('product_detail_id' , '=' , $Product_details_id)->exists()){
            Product_image::where('product_detail_id' , '=' , $Product_details_id)->delete();
        }

        BaseController::deleteFile('Product_details',$Product_details->iamge);
        foreach ($Product_details->images as $row)
        {
            BaseController::deleteFile('Product_image',$row->iamge);
        }
        
        $Product_details->delete();

        $msg=$lang=='ar' ? 'تم حذف تفاصيل المنتج بنجاح'  : 'Product_details Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }


    /*
     * @pram $request
     * @return Error message or check if cateogry is null
     */

    private function validate_Product_details_add($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'product_id.required' => $lang == 'ar' ?  'من فضلك ادخل كود المنتج' :"product_id is required" ,
            'difference_ar.required' => $lang == 'ar' ? 'من فضلك ادخل الاختلاف تفاصيل المنتج بالعربية' :"Difference in english is required"  ,
            'difference_en.required' => $lang == 'ar' ? 'من فضلك ادخل الاختلاف تفاصيل المنتج بالانجليزية' :"Difference in english is required"  ,
            'image.required' => $lang == 'ar' ? 'من فضلك ادخل صورة المنتج ' :"image is required"  ,
            'quantity.required' => $lang == 'ar' ?  'من فضلك ادخل الكمية' :"quantity is required" ,
            'image.image'=>$lang == 'ar' ?  'من فضلك ادخل صورة صحيحة' :"The image must be a valid image",
            'quantity.numeric' => $lang == 'ar' ?  'يجب ان تكون الكمية رقم صحيح' :"The quantity must be a number" ,
            'purchasing_price.required' => $lang == 'ar' ? 'من فضلك ادخل سعر الشراء ' :"purchasing_price is required"  ,
            'wholesale_wholesale_price.required' => $lang == 'ar' ? 'من فضلك ادخل سعر جملة الجملة ' :"wholesale_wholesale_price is required"  ,
            'wholesale_price.required' => $lang == 'ar' ? 'من فضلك ادخل سعر الجملة ' :"wholesale_price is required"  ,
            'selling_price.required' => $lang == 'ar' ? 'من فضلك ادخل سعر البيع ' :"selling_price is required"  ,
            'offer_amount.between'=>$lang == 'ar' ? 'نسبة الخصم يجب ان تكون من0 الي 100' : 'The offer amount must be between 0 and 100.',
            'inventory_id.required' => $lang == 'ar' ? 'من فضلك ادخل المخزن ' :"Inventory is required"  ,
            'barcode.required' => $lang == 'ar' ? 'من فضلك ادخل الباركود ' :"Barcode is required"  ,

        ];
        $validator = Validator::make($input, [
            'product_id' => 'required',
            'difference_ar' => 'required',
            'difference_en' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'quantity' => 'required|numeric',
            'purchasing_price'=>'required',
            'wholesale_wholesale_price'=>'required',
            'wholesale_price'=>'required',
            'selling_price'=>'required',
            'offer_amount'=>'nullable|integer|between:0,100',
            'inventory_id' => 'required',
            'barcode' => 'required',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        
    }
    

    private function validate_Product_details_edit($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'difference_ar.required' => $lang == 'ar' ? 'من فضلك ادخل الاختلاف تفاصيل المنتج بالعربية' :"Difference in english is required"  ,
            'difference_en.required' => $lang == 'ar' ? 'من فضلك ادخل الاختلاف تفاصيل المنتج بالانجليزية' :"Difference in english is required"  ,
        ];
        $validator = Validator::make($input, [
            'difference_ar' => 'required',
            'difference_en' => 'required',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        
    }

    /*
     * Show get Product_details by inventory_id
     */
    public function Product_details_by_inv(Request $request,$inventory_id){
        $lang=$request->header('lang');
        $pr = Product_detail_inventory::where('inventory_id', $inventory_id)->get();
        // return Inventory_product_details_resource::collection($pr);
        // $pro_ids = Product_detail_inventory::where('inventory_id', $inventory_id)->pluck('product_detail_id');
        // $Product_details=Product_details::whereIn('id',$pro_ids)->get();
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(Inventory_product_details_resource::collection($pr),$msg);
    }

    public function Product_detail_inventories(Request $request,$product_detail_id){
        $lang=$request->header('lang');
        $inv_ids = Product_detail_inventory::where('product_detail_id', $product_detail_id)->pluck('inventory_id');
        $invs=Inventory::whereIn('id',$inv_ids)->get();
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(InventoryResource::collection($invs),$msg);
    }


}
