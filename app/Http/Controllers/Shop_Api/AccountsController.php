<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Models\Order_product_details;
use App\Models\Product_details;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\P_d_ordersResource;
use App\Http\Resources\Order_product_detailsResource;
use App\Http\Resources\PriceResource;
use App\User;
use App\Models\P_d_orders;
use App\Models\Shop;
use App\Models\Price;
use App\Models\Product_details_price;
use App\Http\Resources\Admin_debt_logResource;
use App\Models\Admin_debt_log;
use App\Http\Controllers\Manage\BaseController;

class AccountsController extends Controller
{
    
    use \App\Http\Controllers\Api\ApiResponseTrait;
    
    /*
     * get user_orders
     */
    public function user_orders (Request $request , $user_id){
        $orders=P_d_orders::select()->where('user_id',$user_id)->orderBy('created_at','desc')->get();
        return $this->apiResponseData(P_d_ordersResource::collection($orders),'success');
    }

    public function representative_orders (Request $request , $representative_id){
        $orders=P_d_orders::select()->where('representative_id',$representative_id)->orderBy('created_at','desc')->get();
        return $this->apiResponseData(P_d_ordersResource::collection($orders),'success');
    }


    /*
     * get user_orders_sum
     */
    public function user_orders_sum (Request $request , $user_id){
        $total_price=P_d_orders::select()->where('user_id',$user_id)->sum('total_price');
        return $this->apiResponseData($total_price,'success');
    }

    public function representative_orders_sum (Request $request , $representative_id){
        $total_price=P_d_orders::select()->where('representative_id',$representative_id)->sum('total_price');
        return $this->apiResponseData($total_price,'success');
    }


    public function user_representative_orders (Request $request){
        $representative_id = $request->representative_id;
        $user_id = $request->user_id;
        $orders=P_d_orders::select()->where('representative_id',$representative_id)->where('user_id',$user_id)->orderBy('created_at','desc')->get();
        return $this->apiResponseData($orders,'success');
    }


    public function user_representative_orders_sum (Request $request){
        $representative_id = $request->representative_id;
        $user_id = $request->user_id;
        $total_price=$total_price=P_d_orders::select()->where('representative_id',$representative_id)->where('user_id',$user_id)->sum('total_price');
        return $this->apiResponseData($total_price,'success');
    }


    public function representative_orders_of_days (Request $request){
        $representative_id = $request->representative_id;
        $from = date($request->from);
        $to = date($request->to);
        $orders=P_d_orders::select()->where('representative_id',$representative_id)->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('created_at','desc')->get();
        return $this->apiResponseData(P_d_ordersResource::collection($orders),'success');
    }



    public function Price_filter (Request $request){
        $price_type_req = $request->price_type;
        $from = $request->from;
        $to = $request->to;
        if($price_type_req == '1'){
            $price_type = 'wholesale_wholesale_price';
        }elseif($price_type_req == '2'){
            $price_type = 'wholesale_price';
        }elseif($price_type_req == '3'){
            $price_type = 'selling_price';
        }
        $prices = Price::select()->whereBetween($price_type, [$from,$to])->get();
        // return $prices;
        foreach($prices as $price){
            $price_ids[] = $price->id;
        }
        $p_d_price = Product_details_price::where('price_id',$price_ids)->get();
        return $p_d_price;
        
        // return $this->apiResponseData(PriceResource::collection($prices),'success');

    }
}
