<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\Shop\ShopResource;
use App\Models\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\UserResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\CartResource;
use App\User;
use App\Models\Shop;
use App\Http\Resources\Shop\AdminResource;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Controllers\Manage\EmailsController;

class LoginController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;



    public function login(Request $request)
    {
        $lang = $request->header('lang');
        $credentials = [

            'name' => $request['name'],
            'password'=>$request['password'],
        ];
        $credentials_2 = [

            'email' => $request['name'],
            'password'=>$request['password'],
        ];
        if (Auth::guard('Shop')->attempt($credentials) || Auth::guard('Shop')->attempt($credentials_2)) {
            $user=Auth::guard('Shop')->user();
            $token=$user->createToken('Shop')->accessToken;
            $user['token']=$token;
            $msg=$lang=='ar' ? 'تم تسجيل الدخول بنجاح' : 'login success';
            return $this->apiResponseData(new AdminResource($user),$msg,200);
        }
        $msg=$lang=='ar' ? 'البيانات المدخلة غير صحيحة' : 'invalid username or password';
        return $this->apiResponseMessage(0,$msg,200);

    }


    public function get_info(Request $request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        $user['token']=null;
        $msg=$lang=='ar' ? 'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new AdminResource($user),$msg,200);

    }

    public function shop_info(Request $request)
    {
        $lang = $request->header('lang');
        $user=Settings::where('mainShop_id' , 10)->first();
        $msg=$lang=='ar' ? 'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new ShopResource($user),$msg,200);

    }

    /*
     * Edit Shop Admin Info
     */
    public function edit_info(Request $request)
    {
        $lang = $request->header('lang');
        $user = Auth::user();

        $check=$this->not_found($user,'العضو','user',$lang);
        if(isset($check))
        {
            return $check;
        }
        $id=Auth::user()->id;

        $input = $request->all();
        $validationMessages = [
            'name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم اسمك الاول' :"frist name is required" ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضلك ادخل البريد رقم الهاتف' :"phone is required"  ,
            'name.unique' => $lang == 'ar' ? 'اسم المستخدم موجود لدينا بالفعل' :"username is already teken" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  'رقم الهاتف يجب ان يكون رقما' :"The phone must be a number" ,

        ];

        $validator = Validator::make($input, [
            'phone' => 'required|min:7|numeric',
            'email' => 'required|unique:shops,email,'.$id.'|regex:/(.+)@(.+)\.(.+)/i',
            'name' => 'required|unique:shops,name,'.$id,
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }


        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->name = $request->name;
        if($request->password) {
            $user->password = Hash::make($request->password);
        }
        if($request->logo){
            BaseController::deleteFile('Shop',$user->logo);
            $name=BaseController::saveImage('Shop',$request->file('logo'));
            $user->logo=$name;
        }
        $user->save();
        $user['token']=null;
        $msg=$lang=='ar' ?  'تمت العملية بنجاح' :'success' ;
        return $this->apiResponseData(new AdminResource($user),$msg,200);
    }

    /*
     * Edit main info
     */

    public function edit_settings(Request $request)
    {
        $shop=Settings::where('mainShop_id' , 10)->first();
        $shop->address_ar=$request->address_ar;
        $shop->name_ar=$request->name_ar;
        $shop->name_en=$request->name_en;
        $shop->address_ar=$request->address_ar;
        $shop->address_en=$request->address_en;
        $shop->tax=$request->tax;
        $shop->shipping_price=$request->shipping_price;
        $shop->email=$request->email;
        $shop->phone=$request->phone;
        $shop->facebook=$request->facebook;
        $shop->snap=$request->snap;
        $shop->twitter=$request->twitter;
        $shop->instagram=$request->instagram;

        if($request->logo){
            BaseController::deleteFile('Settings',$shop->logo);
            $name=BaseController::saveImage('Settings',$request->file('logo'));
            $shop->logo=$name;
        }
        $shop->save();
        $lang=$request->header('lang');
        $msg=$lang=='ar' ?  'تمت العملية بنجاح' :'success' ;
        return $this->apiResponseData(new ShopResource($shop),$msg,200);

    }
}