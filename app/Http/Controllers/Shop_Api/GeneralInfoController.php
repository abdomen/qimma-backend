<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\ColorResource;
use App\Http\Resources\Shop\CategoryResource;
use App\Http\Resources\Shop\SizeResource;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Sizes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\UserResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\CartResource;
use App\User;
use App\Models\Shop;
use App\Http\Controllers\Manage\BaseController;
use App\Models\color;
class GeneralInfoController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * All Color to add to custom product
     * many to many with products (colors relationship)
     */
    public function get_colors(Request $request)
    {
        $shop=Auth::user();
        $color=color::get();
        return $this->apiResponseData(ColorResource::collection($color),'success');
    }

    /*
     * All Sizes to add to custom product
     * many to many with products (sizes relationship)
     */
    public function get_sizes(Request $request)
    {
        $shop = Auth::User();    
        $Sizes=Sizes::get();
        return $this->apiResponseData(SizeResource::collection($Sizes),'success');
    }

    /*
     * All currencies to add to custom product
     * many to many with products (price_product relationship)
     * many to many with shop and he can choose default currency
     */
    public function get_currency(Request $request)
    {
        $shop = Auth::user();
        $currencies=Currency::get();
        return $this->apiResponseData($currencies,'success');
    }

    /*
     * Get Category level one or level two
     * if send cat_id as a pram get level two
     */

    public function get_category(Request $request)
    {
        $cats=Category::where('id','!=',1)->get();
        return $this->apiResponseData(CategoryResource::collection($cats),'success');
    }

}