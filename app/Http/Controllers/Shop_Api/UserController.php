<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Http\Resources\ColorResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\Shop\UserResource;
use App\Http\Resources\UserAddressResource;
use App\User;
use App\Http\Controllers\Manage\BaseController;
use App\Models\Notfication;
use App\Models\Representative_User;
use App\Http\Resources\Rep_attendenceResource;
use App\Models\Rep_attendence;

class UserController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new user to Auth Shop database
     */
    public function add_user(Request $request)
    {
        $lang = $request->header('lang');
        $shop=Auth::user();
        $input = $request->all();
        $validationMessages = [
            'first_name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الاسم الاول' :"frist name is required" ,
            'password.required' => $lang == 'ar' ? 'من فضلك ادخل كلمة السر' :"password is required"  ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضلك ادخل البريد رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'last_name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم اسم العائلة' :"last name name is required" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  'رقم الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
        ];

        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users|numeric|min:7',
            'email' => 'required|unique:users|regex:/(.+)@(.+)\.(.+)/i',
            'password' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $user = new User();
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->lat  = $request->lat;
        $user->lng  = $request->lng;
        $user->status  = $request->status;
        $user->shop_id = $shop->id;
        $user->social  = 0;
        $user->lang=$lang;
        $user->password = Hash::make($request->password);
        if($request->image){
            $name=BaseController::saveImage('users',$request->file('image'));
            $user->image=$name;
        }
        $user->save();
        $user['token']=null;
        $msg=$lang == 'ar' ? 'تم اضافة العضو بنجاح' : 'user added successfully';
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new UserResource($user)]);

    }

    /*
     * Edit user information
    */
    public function edit_user(Request $request,$id)
    {
        $lang = $request->header('lang');
        $user = User::find($id);
        $check=$this->not_found($user,'العميل','Client',$lang);
        if(isset($check))
        {
            return $check;
        }
        $input = $request->all();
        $validationMessages = [
            'first_name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الاسم الاول' :"frist name is required" ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضلك ادخل البريد رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'last_name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم اسم العائلة' :"last name name is required" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  'رقم الهاتف يجب ان يكون رقما' :"The phone must be a number" ,

        ];

        $validator = Validator::make($input, [
            'phone' => 'required|min:7|numeric|unique:users,phone,'.$id,
            'email' => 'required|unique:users,email,'.$id.'|regex:/(.+)@(.+)\.(.+)/i',
            'first_name' => 'required',
            'last_name'=>'required'
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }


        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->lat  = $request->lat;
        $user->lng  = $request->lng;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->status  = $request->status;
        if($request->password){
            $user->password = Hash::make($request->password);
        }
        if($request->image){
            BaseController::deleteFile('users',$user->image);
            $name=BaseController::saveImage('users',$request->file('image'));
            $user->image=$name;
        }
        $user->save();
        $user['token']=null;
        $msg=$lang=='ar' ?  'تم تعديل بيانات العميل' :'Client Edited successfully' ;
        return $this->apiResponseData(new UserResource($user),  $msg);
    }

    /*
     * Show single  user
    */
    public function single_user(Request $request,$user_id){
        $lang=$request->header('lang');
        $user = User::where('id' , $user_id)->first();
        $check=$this->not_found($user,'العميل','Client',$lang);
        if(isset($check))
        {
            return $check;
        }
        $user['token']=null;
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new userResource($user),$msg);
    }

    /*
     * Delete user ..
     */

    public function delete_user(Request $request,$user_id){
        $lang=$request->header('lang');
        $user=User::find($user_id);
        $check=$this->not_found($user,'العميل','Client',$lang);
        if(isset($check)){
            return $check;
        }
        BaseController::deleteFile('users',$user->image);
        if(Representative_User::where('user_id',$user_id)->exists()){
            Representative_User::where('user_id',$user_id)->delete();
        }
        $user->delete();
        $msg=$lang=='ar' ? 'تم حذف العميل بنجاح'  : 'Client Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * get All user for Auth shop
     */
    public function all_users(Request $request)
    { 
        $shop=Auth::user();
        // $page=$request->page * 20;
        $users=User::where('status' , '!=' , '2')->orderBy('id','desc')->get();
        // foreach($users as $row){$row['image']=BaseController::getImageUrl('users',$row->image);}
        return $this->apiResponseData(UserAddressResource::collection($users),'success');
    }


    /*
     * Send custom notifcation
     */

    public function send_notifcation(Request $request,$user_id)
    {
        $lang=$request->header('lang');
        $user=User::find($user_id);
        $check=$this->not_found($user,'العضو','user',$lang);
        if(isset($check))
        {
            return $check;
        }
        NotificationMethods::senNotificationToSingleUser($user->fire_base_token,$request->title,$request->desc,null,1,1);
        $noti = new Notfication;
        $noti->user_id=$user->id;
        $noti->name=$request->title;
        $noti->desc=$request->desc;


        $noti->click_action=0;
        $noti->redirect_id=1;
        $noti->status=1;
        $noti->save();
        $msg=$lang=='ar' ? 'تم ارسال الاشعار بنجاح'  : 'notification send successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }



    public function get_user_location(Request $request,$user_id)
    {
       // DB::table('users');
        $user=Auth::user();
        $user_location=User::select('id','lat', 'lng')->where('id',$user_id)->get();
        return $this->apiResponseData($user_location,'success');

    }


    public function all_attendence(Request $request)
    {
        $attendence = Rep_attendence::orderBy('created_at' , 'desc')->get();
        return $this->apiResponseData(Rep_attendenceResource::collection($attendence),'success');
    }
    
    
    
    public function representative_attendence(Request $request , $representative_id)
    {
        $attendence = Rep_attendence::where('representative_id' , $representative_id)->get();
        return $this->apiResponseData(Rep_attendenceResource::collection($attendence),'success');
    }


    public function single_attendence(Request $request , $id)
    {
        $attendence = Rep_attendence::where('id' , $id)->first();
        return $this->apiResponseData(new Rep_attendenceResource($attendence),'success');
    }


    public function filter_attendence_by_date(Request $request)
    {
        $from = date($request->from);
        $to = date($request->to);
        $dated_orders = Rep_attendence::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->get();
        return $this->apiResponseData(Rep_attendenceResource::collection($dated_orders),'success');
    }


}
