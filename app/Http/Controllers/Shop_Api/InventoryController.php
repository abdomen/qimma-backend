<?php

namespace App\Http\Controllers\Shop_Api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;
use App\Models\Inventory;
use App\Models\Product_detail_inventory;
use App\Models\Inventory_stock;
use App\Models\Product_details_price;
use App\Models\Product_details;
use App\Models\Price;
use App\Models\inventory_transformation;
use App\Models\Products;
use App\Models\Qty_edit_log;
use App\Http\Resources\InventoryResource;
use App\Http\Resources\Product_detail_inventoryResource;
use App\Http\Resources\Inventory_stockResource;
use App\Http\Resources\inventorytransformationResource;
use App\Http\Resources\ProductInventoriesResource;
use App\Http\Resources\Qty_edit_logResource;


class InventoryController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new Inventory to database
     */
    public function add_Inventory(Request $request)
    {
        $lang=$request->header('lang');
        $user=Auth::user();
        $validate_Inventory=$this->validate_Inventory($request);
        if(isset($validate_Inventory)){
            return $validate_Inventory;
        }
        $Inventory=new Inventory();
        $Inventory->name_ar=$request->name_ar;
        $Inventory->name_en=$request->name_en;
        $Inventory->address_ar=$request->address_ar;
        $Inventory->address_en=$request->address_en;
        $Inventory->status=$request->status;
        $Inventory->phone=$request->phone;
        $Inventory->lng=$request->lng;
        $Inventory->lat=$request->lat;
        $Inventory->save();
        $msg=$lang=='ar' ? 'تم اضافة المخزن بنجاح'  : 'Inventory added successfully';
        return $this->apiResponseData(new InventoryResource($Inventory),$msg);

    }

    /*
     * Edit product
    */
    public function edit_Inventory(Request $request,$Inventory_id)
    {
        $lang=$request->header('lang');
        $Inventory=Inventory::find($Inventory_id);
        $check=$this->not_found($Inventory,'المخزن','Inventory',$lang);
        if(isset($check)){
            return $check;
        }
        $validate_Inventory=$this->validate_Inventory($request);
        if(isset($validate_Inventory)){
            return $validate_Inventory;
        }

        $Inventory->name_ar=$request->name_ar;
        $Inventory->name_en=$request->name_en;
        $Inventory->address_ar=$request->address_ar;
        $Inventory->address_en=$request->address_en;
        $Inventory->status=$request->status;
        $Inventory->phone=$request->phone;
        $Inventory->lng=$request->lng;
        $Inventory->lat=$request->lat;
        $Inventory->save();
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم تعديل المخزن بنجاح'  : 'Inventory edited successfully';
        return $this->apiResponseData(new InventoryResource($Inventory),$msg);
    }


    /*
     * get All product for Auth shop
     */
    public function all_Inventory(Request $request)
    {
        $user=Auth::user();
        // $page=$request->page * 20;
        $Inventory=Inventory::/*where('mainShop_id' , $request->mainShop_id)->*/orderBy('id','DESC')->get();
        return $this->apiResponseData(InventoryResource::collection($Inventory),'success');
    }


    /*
     * Show single Inventory
     */
    public function single_Inventory(Request $request,$Inventory_id){
        $lang=$request->header('lang');
        $Inventory=Inventory::find($Inventory_id);
        $check=$this->not_found($Inventory,'المخزن','Inventory',$lang);
        if(isset($check)){
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData($Inventory,$msg); 
    }


    // public function single_Inv(Request $request,$Inventory_id){
    //     $lang=$request->header('lang');
    //     $Inventory=Inventory::find($Inventory_id);
    //     $check=$this->not_found($Inventory,'المخزن','Inventory',$lang);
    //     if(isset($check)){
    //         return $check;
    //     }
    //     $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
    //     return $this->apiResponseData($Inventory,$msg);
    // }

    /*
     * Delete Inventory ..
     */

    public function delete_Inventory(Request $request,$Inventory_id){
        $lang=$request->header('lang');
        $Inventory=Inventory::where('id',$Inventory_id)->where('id','!=',1)->first();
        $check=$this->not_found($Inventory,'المخزن','Inventory',$lang);
        if(isset($check)){
            return $check;
        }
        $inv_pro = Product_detail_inventory::where('inventory_id' , $Inventory_id);
        if (Product_detail_inventory::where('inventory_id' , $Inventory_id)->exists()){
            $msg=$lang=='ar' ? 'لا يمكن حذف المخزن لاحتوائه علي منتجات'  : 'Inventory can not be Deleted because it has products';
        }else{
            $Inventory->delete();
            $msg=$lang=='ar' ? 'تم حذف المخزن بنجاح'  : 'Inventory Deleted successfully';
        }
        return $this->apiResponseMessage(1,$msg,200);
    }


    public function Inventories_count(Request $request){
        $user=Auth::user();
        $page=$request->page * 20;
        $inv_count = count(Inventory::select('id')->get());
        return $this->apiResponseData(["Inventories count = $inv_count"],'success');
    }


    public function Inventories_stock(Request $request , $inv_id){
        $user=Auth::user();
        $Inv_sts=Product_detail_inventory::select('product_detail_id','quantity')->where('inventory_id',$inv_id)->get();
        return $this->apiResponseData(Product_detail_inventoryResource::collection($Inv_sts),'success');
    }


    public function Inventory_edit_stock(Request $request){

        $lang=$request->header('lang');
        $user=Auth::user();
        $inv_id = $request->inventory_id;
        $p_d_id = $request->product_detail_id;
        $real_amount = $request->real_amount;

        $Inventory=Inventory::find($inv_id);
        $check=$this->not_found($Inventory,'المخزن','Inventory',$lang);
        if(isset($check)){
            return $check;
        }

        $price_id = Product_details_price::where('product_detail_id',$p_d_id)->value('price_id');
        $price = Price::where('id',$price_id)->value('purchasing_price');

        $Inv_sts=Product_detail_inventory::where('inventory_id',$inv_id)->where('product_detail_id',$p_d_id)->first();
        $system_amount = $Inv_sts->quantity;

        $p_d = Product_details::find($p_d_id);
        $check=$this->not_found($p_d,'المنتج','product',$lang);
        if(isset($check)){
            return $check;
        }

        if ($system_amount >= $real_amount){
            $diff_amount = $system_amount-$real_amount;
        }else{
            $diff_amount = $real_amount-$system_amount;
        }

        $total_price = $diff_amount*$price;

        $Inv_sts->quantity = $real_amount;
        $Inv_sts->save();

        $inventory_stock = new Inventory_stock;
        $inventory_stock->user_id = $user->id;
        $inventory_stock->inventory_id = $inv_id;
        $inventory_stock->product_detail_id = $p_d_id;
        $inventory_stock->system_amount = $system_amount;
        $inventory_stock->real_amount = $real_amount;
        $inventory_stock->diff_amount = $diff_amount;
        $inventory_stock->total_price = $total_price;
        $inventory_stock->save();

        return $this->apiResponseData(new Inventory_stockResource($inventory_stock),'success');

    }

    public function Inventories_stockEdit_show(Request $request){
        $user=Auth::user();
        $Inv_sts=Inventory_stock::orderBy('created_at','DESC')->get();
        return $this->apiResponseData(Inventory_stockResource::collection($Inv_sts),'success');
    }
    

    public function Inventories_stockEdit_show_byDate(Request $request){
        $from = date($request->from);
        $to = date($request->to);
        $Inv_sts=Inventory_stock::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('created_at' , 'desc')->get();
        return $this->apiResponseData(Inventory_stockResource::collection($Inv_sts),'success');
    }


    /*
     * @pram $request
     * @return Error message or check if cateogry is null
     */

    private function validate_Inventory($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'name_ar.required' => $lang == 'ar' ?  'من فضلك ادخل اسم المخزن بالعربية' :"name in arabic is required" ,
            'name_en.required' => $lang == 'ar' ? 'من فضلك ادخل اسم المخزن بالانجليزية' :"name in english is required" ,
            'lng.required' => $lang == 'ar' ? 'من فضلك ادخل الموقع' :"Location is required" ,
            'lat.required' => $lang == 'ar' ? 'من فضلك ادخل الموقع' :"Location is required" ,
        ];
        $validator = Validator::make($input, [
            'name_ar' => 'required',
            'name_en' => 'required',
            'lng' => 'required',
            'lat' => 'required',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }

    }


    public function add_ProductDetail_toInventory(Request $request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        
        $product_detail_id = $request->product_detail_id;
        
        $proex = Product_details::find($product_detail_id);
        $check=$this->not_found($proex,'المنتج','Product',$lang);
        if(isset($check)){
            return $check;
        }

        $inventory_id = $request->inventory_id;
        $inventoryex = Inventory::find($inventory_id);
        $check=$this->not_found($inventoryex,'المخزن','Inventory',$lang);
        if(isset($check)){
            return $check;
        }

        $checkPInv = Product_detail_inventory::where('product_detail_id' , $product_detail_id)->where('inventory_id' , $inventory_id)->first();
        if($checkPInv){
            $checkPInv->quantity += $request->quantity;
            $checkPInv->save();
            $msg=$lang=='ar' ? 'المنتج موجود بالمخزن بالفعل و تم تعديل الكمية'  : 'Product already exists in this inventory and quantity edited successfully';
            return $this->apiResponseMessage(1,$msg,200);
        }

        $Product_detail_inventory=new Product_detail_inventory();
        $Product_detail_inventory->inventory_id=$inventory_id;
        $Product_detail_inventory->product_detail_id=$product_detail_id;
        $Product_detail_inventory->quantity=$request->quantity;
        $Product_detail_inventory->save();

        $msg = $lang=='ar' ? 'تم اضافة تفاصيل المنتج للمخزن بنجاح'  : 'Product_detail added to inventory successfully';
        return $this->apiResponseData(new ProductInventoriesResource($Product_detail_inventory),$msg);

    }

    public function inventory_transformation(Request $request){
        $lang=$request->header('lang');
        $user=Auth::user();
        $product_detail_id =$request->product_detail_id;
        $from_inv_id = $request->from_inv_id;
        $to_inv_id = $request->to_inv_id;
        $quantity = $request->quantity;
        $from_inv = Product_detail_inventory::where('product_detail_id' , $product_detail_id)->where('inventory_id' , $from_inv_id)->first();
        $check=$this->not_found($from_inv,'المخزن','Inventory',$lang);
        if(isset($check)){
            return $check;
        }

        $qty = $from_inv->quantity;
        if($from_inv->quantity < $quantity){
            $msg=$lang=='ar' ? 'الكمية غير كافية للتحويل'  : 'Quantity not Enough';
            return $this->apiResponseMessage(0,$msg,200);
        }else{

            if (Product_detail_inventory::where('product_detail_id' , $product_detail_id)->where('inventory_id' , $to_inv_id)->exists()){
                $to_inv = Product_detail_inventory::where('product_detail_id' , $product_detail_id)->where('inventory_id' , $to_inv_id)->first();
                $new_qty = ($from_inv->quantity)-$quantity;
                $from_inv->quantity =$new_qty;
                $from_inv->save();

                $to_inv->quantity += $quantity;
                $to_inv->save();

            }else{

                $new_qty = ($from_inv->quantity)-$quantity;
                $from_inv->quantity = $new_qty;
                $from_inv->save();

                $new_p_d_inv = new Product_detail_inventory;
                $new_p_d_inv->product_detail_id = $product_detail_id;
                $new_p_d_inv->inventory_id = $to_inv_id;
                $new_p_d_inv->quantity = $quantity;
                $new_p_d_inv->save();
            }

            $Inventory_trans = new inventory_transformation();
            $Inventory_trans->from_inv_id = $from_inv_id;
            $Inventory_trans->to_inv_id=$to_inv_id;
            $Inventory_trans->product_detail_id =$product_detail_id ;
            $Inventory_trans->quantity=$request->quantity;
            $Inventory_trans->user_id= $user->id;
            $Inventory_trans->save();

            $msg=$lang=='ar' ? 'تم التحويل  بنجاح'  : 'Inventory transformation successfully';
            return $this->apiResponseData(new inventorytransformationResource($Inventory_trans),$msg);
        }
    }


    public function all_inventory_transformation(Request $request){
        $Inv_sts=inventory_transformation::orderBy('created_at' , 'desc')->get();
        return $this->apiResponseData(inventorytransformationResource::collection($Inv_sts),'success');
    }


    public function inventoryTransformation_byDate(Request $request)
    {
        $from = date($request->from);
        $to = date($request->to);
        $log = inventory_transformation::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('created_at' , 'desc')->get();
        return $this->apiResponseData(inventorytransformationResource::collection($log),'success');
    }


    public function inventory_multi_transformation(Request $request){

        $lang=$request->header('lang');
        $user=Auth::user();

        $req = $request->all();
        
        foreach($req['products'] as $product){
            $product_detail_id = $product['product_detail_id'];
            $from_inv_id = $product['from_inv_id'];
            $to_inv_id = $product['to_inv_id'];
            $quantity = $product['quantity'];

            $from_inv = Product_detail_inventory::where('product_detail_id' , $product_detail_id)->where('inventory_id' , $from_inv_id)->first();
            $check=$this->not_found($from_inv,'المخزن','Inventory',$lang);
            if(isset($check)){
                return $check;
            }

            $qty = $from_inv->quantity;
            if($from_inv->quantity < $quantity){
                $msg=$lang=='ar' ? 'الكمية غير كافية للتحويل'  : 'Quantity not Enough';
                return $this->apiResponseMessage(0,$msg,200);
            }else{

                if (Product_detail_inventory::where('product_detail_id' , $product_detail_id)->where('inventory_id' , $to_inv_id)->exists()){
                    $to_inv = Product_detail_inventory::where('product_detail_id' , $product_detail_id)->where('inventory_id' , $to_inv_id)->first();
                    $new_qty = ($from_inv->quantity)-$quantity;
                    $from_inv->quantity =$new_qty;
                    $from_inv->save();

                    $to_inv->quantity += $quantity;
                    $to_inv->save();

                }else{

                    $new_qty = ($from_inv->quantity)-$quantity;
                    $from_inv->quantity = $new_qty;
                    $from_inv->save();

                    $new_p_d_inv = new Product_detail_inventory;
                    $new_p_d_inv->product_detail_id = $product_detail_id;
                    $new_p_d_inv->inventory_id = $to_inv_id;
                    $new_p_d_inv->quantity = $quantity;
                    $new_p_d_inv->save();
                }

                $Inventory_trans = new inventory_transformation();
                $Inventory_trans->from_inv_id = $product['from_inv_id'];
                $Inventory_trans->to_inv_id=$product['to_inv_id'];
                $Inventory_trans->product_detail_id =$product['product_detail_id'] ;
                $Inventory_trans->quantity=$product['quantity'];
                $Inventory_trans->user_id= $user->id;
                $Inventory_trans->save();
            }
        }
        $msg=$lang=='ar' ? 'تم التحويل  بنجاح'  : 'Inventory transformation successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }


    public function product_qty_edit(Request $request,$inv_id,$product_detail_id)
    {
        $admin = Auth::user();
        $lang=$request->header('lang');

        $product = Product_detail_inventory::where('product_detail_id' , $product_detail_id)->where('inventory_id' , $inv_id)->first();
        $new_qty = $request->quantity;
        $old_qty = $product->quantity;

        $product->quantity = $new_qty;
        $product->save();

        $log = new Qty_edit_log;
        $log->admin_id = $admin->id;
        $log->inventory_id = $inv_id;
        $log->product_detail_id = $product_detail_id;
        $log->old_qty = $old_qty;
        $log->new_qty = $new_qty;
        $log->save();

        $msg=$lang=='ar' ? 'تم تعديل الكمية بنجاح'  : 'Quantity edited successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }


    public function qty_edit_log(Request $request){
        $Inv_sts = Qty_edit_log::orderBy('created_at' , 'desc')->get();
        return $this->apiResponseData(Qty_edit_logResource::collection($Inv_sts),'success');
    }

    public function qty_edit_log_byDate(Request $request)
    {
        $from = date($request->from);
        $to = date($request->to);
        $log = Qty_edit_log::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->orderBy('created_at' , 'desc')->get();
        return $this->apiResponseData(Qty_edit_logResource::collection($log),'success');
    }

}
