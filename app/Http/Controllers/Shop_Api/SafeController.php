<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;

use App\Models\Safe;
use App\Models\Safe_details;
use App\Models\Safe_transactions;
use App\Http\Resources\SafeResource;
use App\Http\Resources\Safe_detailsResource;
use App\Http\Resources\Safe_transactionsResource;

class SafeController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * add safe
     */
    public function add_safe (Request $request)
    {

        $lang=$request->header('lang');

        $validate_safe=$this->validate_safe($request);
        if(isset($validate_safe)){
            return $validate_safe;
        }

        $safe = new Safe;
        $safe->name_ar = $request->name_ar;
        $safe->name_en = $request->name_en;
        if(!$request->status){
            $safe->status = 1;
        }else{
            $safe->status = $request->status;
        }
        $safe->total_money = $request->total_money;
        $safe->save();

        return $this->apiResponseData(new SafeResource($safe),'success');
    }


    /*
     * edit safe
     */
    public function edit_safe (Request $request,$safe_id)
    {

        $lang=$request->header('lang');

        $safe = Safe::find($safe_id);
        $check=$this->not_found($safe,'الخزنه','safe',200);
        if(isset($check))
        {
            return $check;
        }
        $safe->name_ar = $request->name_ar;
        $safe->name_en = $request->name_en;
        $safe->status = $request->status;
        $safe->total_money = $request->total_money;
        $safe->save();

        return $this->apiResponseData(new SafeResource($safe),'success');
    }


    /*
     * delete safe
     */
    public function delete_safe (Request $request,$safe_id)
    {

        $lang=$request->header('lang');

        $safe = Safe::where('id',$safe_id)->where('id','!=',1)->first();
        $check=$this->not_found($safe,'الخزنه','safe',200);
        if(isset($check))
        {
            return $check;
        }
        $safe->delete();

        $msg = $lang=='ar' ? 'تم الحذف بنجاح'  : 'Deleted successfully';
        return $this->apiResponseMessage(0,$msg, 200);
    }


    /*
     * single safe
     */
    public function single_safe (Request $request,$safe_id)
    {

        $lang=$request->header('lang');

        $safe = Safe::find($safe_id);
        $check=$this->not_found($safe,'الخزنه','safe',200);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData($safe,'success');
    }


    /*
     * all safes
     */
    public function all_safes (Request $request)
    {

        $lang=$request->header('lang');

        $safe = Safe::orderBy('id','DESC')->get();

        return $this->apiResponseData(SafeResource::collection($safe),'success');
    }


    /*
     * add safe process
     */
    public static function add_safe_process ($details)
    {
        
        $safe = Safe::find($details->safe_id);
        
        $safe_detail = new Safe_details;
        $safe_detail->safe_id = $details->safe_id;
        $safe_detail->process_name_ar = $details->process_name_ar;
        $safe_detail->process_name_en = $details->process_name_en;
        if($details->process_type == 1){
            $safe->total_money += $details->value;
            $safe->save();
            $safe_detail->process_type = 'addition';
        }elseif($details->process_type == 2){
            $safe->total_money -= $details->value;
            $safe->save();
            $safe_detail->process_type = 'subtraction';
        }
        $safe_detail->value = $details->value;
        if(isset($details->notes)){
            $safe_detail->notes = $details->notes;
        }
        $safe_detail->save();
    }



    /*
     * add safe transaction
     */
    public function add_safe_transaction (Request $request)
    {
        $lang=$request->header('lang');

        $admin = Auth::user();

        $safe_from = Safe::find($request->safe_from);
        $check=$this->not_found($safe_from,'الخزنه المحول منها','safe transerred from',200);
        if(isset($check))
        {
            return $check;
        }

        $safe_to = Safe::find($request->safe_to);
        $check=$this->not_found($safe_to,'الخزنه المحول إليها','safe transerred to',200);
        if(isset($check))
        {
            return $check;
        }

        $old_value = Safe::where('id',$request->safe_from)->value('total_money');
        if($old_value < $request->value){
            $msg = $lang=='ar' ? "قمية المبلغ المحول اكبر من المتواجد بالخزنة"  : 'transferred cash is greater than safe cash';
            return $this->apiResponseMessage(0,$msg, 200);
        }

        $safe_transactions = new Safe_transactions;
        $safe_transactions->safe_from = $request->safe_from;
        $safe_transactions->safe_to = $request->safe_to;
        $safe_transactions->admin_id = $admin->id;
        $safe_transactions->value = $request->value;
        $safe_transactions->notes = $request->notes;
        $safe_transactions->save();


        $safe_detail = new \stdClass();
        $safe_detail->safe_id = $request->safe_from;
        $safe_detail->process_name_ar = 'خصم تحويل';
        $safe_detail->process_name_en = 'Transfer subtraction';
        $safe_detail->process_type = 2;
        $safe_detail->value = $request->value;
        $this->add_safe_process($safe_detail);

        $detail = new \stdClass();
        $detail->safe_id = $request->safe_to;
        $detail->process_name_ar = 'إضافة تحويل';
        $detail->process_name_en = 'Transfer addition';
        $detail->process_type = 1;
        $detail->value = $request->value;
        $this->add_safe_process($detail);

        return $this->apiResponseData(new Safe_transactionsResource($safe_transactions),'success');
    }

    /*
     * @pram $request
     * @return Error message or check if user is null
    */

    private function validate_safe($request){

        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'name_ar.required' => $lang == 'ar' ? 'من فضلك ادخل اسم الخزنة بالعربية' :"name in Arabic is required",
            'name_en.required' => $lang == 'ar' ? 'من فضلك ادخل اسم الخزنة بالانجليزية' :"name in English is required",
        ];
    
        $validator = Validator::make($input, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ], $validationMessages);
    
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
    }
}
