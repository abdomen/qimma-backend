<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Http\Resources\ColorResource;
use App\Models\Order_product;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\order_locations;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\Shop\OrderResource;
use App\User;
use App\Models\Order;
use App\Models\Shop;
use App\Http\Controllers\Manage\BaseController;

class OrderController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * get all orders
     */
    public function all_Order (Request $request){
        $orders=Order::orderBy('created_at','desc')->get();
        return $this->apiResponseData(OrderResource::collection($orders),'success');
    }

    /*
     * Filter order
     */
    public function filter_Orders(Request $request)
    {
        $orders=order::orderby('created_at','desc');
        if ($request->type == 1)
        {
            $orders = $orders->whereday('created_at', now());
        }elseif ($request->type==2) {
            $orders = $orders->wheremonth('created_at', now());
        }elseif ($request->type==3){
            $order= $orders->whereyear('created_at', now());
        }else{
            return $this->apiResponseMessage(0,'هذا النوع غير صحيح',200);
        }

        $orders=$orders->get();
        return $this->apiResponseData(OrderResource::collection($orders),'success');

    }

    /*
     * filter by status
     */
    public function filter_by_status(Request $request)
    {
        $orders=Order::orderBy('created_at','desc')->where('status',$request->status)->get();
        return $this->apiResponseData(OrderResource::collection($orders),'success');

    }


    /*
    * Delete Orders
    */
    public function delete_order(Request $request,$Order_id)
    {
        $lang=$request->header('lang');
        $order=Order::find($Order_id);
        $check=$this->not_found($order,'الطلب','Order',$lang);
        if(isset($check)){
            return $check;
        }
        $order->delete();
        $msg=$lang=='ar' ? 'تم حذف الطلب بنجاح'  : 'Order Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Edit status
     */

    public function edit_status(Request $request,$order_id)
    {
        $lang=$request->header('lang');
        $order=Order::find($order_id);
        $check=$this->not_found($order,'الطلب','Order',$lang);
        if(isset($check)){
            return $check;
        }
        $order->status=$request->status;
        $order->save();
        $this->sentNotifcation($order);
        $msg=$lang=='ar' ? 'تم تعديل الحالة بنجاح'  : 'status updated successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * filrt
     */
    private function sentNotifcation($order)
    {
        $redirct_id=$order->id;
        $user=User::find($order->user_id);
        $token=$user->fire_base_token;
        $lang=$user->lang;
        if($order->status == 2)
        {
            $title=$lang=='ar' ? 'الطلب قيد التنفيذ'  : 'order in progress';
            $desc=$lang=='ar' ? 'وهو الان قيد التنفيذ '.$order->id.'تم قبول الطلب رقم '  : 'order in progress';
        }elseif ($order->status == 3)
        {
            $title=$lang=='ar' ? 'الطلب اكتمل'  : 'order  completed';
            $desc=$lang=='ar' ? $order->id.'تم اكتمال الطلب رقم '  : 'order completed';
        }
        NotificationMethods::senNotificationToSingleUser($token,$title,$desc,null,3,$redirct_id);
    }

    /*
     * Single order to show data
     */
    public function single_order(Request $request,$order_id)
    {
        $lang=$request->header('lang');
        $order=Order::find($order_id);
        $check=$this->not_found($order,'الطلب','Order',$lang);
        if(isset($check)){
            return $check;
        }
        return $this->apiResponseData(new OrderResource($order),'success');
    }

    /*
     * Add new Order
     * add general order column , add products to this order
     */

    public function add_order(Request $request)
    {
        $shop=Shop::find(1);
        $lang=$request->header('lang');
        $user=$request->user_id;
        $user_d = User::find($user);
        $representative = $request->representative_id;
        $representative_d = User::where('id' , '=' , $representative)->where('status' , '=' , '2')->first();

        $check=$this->not_found($representative_d,'المندوب','Representative',200);
        if(isset($check))
        {
            return $check;
        }

        $check=$this->not_found($user_d,'العضو','user',200);
        if(isset($check))
        {
            return $check;
        }

        $validate_address=$this->validate_address($request,$lang,$request->user_id);
        if(isset($validate_address))
        {
            return $validate_address;
        }

        $order=new Order();
        $order->user_id=$user;
        $order->representative_id=$representative;
        $order->status=$request->status;
        $order->shipping_price=$shop->shipping_price;
        $order->payment_method=$request->payment_method;
        $order->address_id=$request->address_id;
        $order->save();
        $msg=$lang=='ar' ?  'تم تسجيل الطلب بنجاح' :'order save successfully' ;
        return $this->apiResponseData(new OrderResource($order),$msg);
    }

    /*
     * custom function to validate Address To orders
     */

    private function validate_address($request,$lang,$user_id)
    {

        if(!$request->address_id)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل العنوان'  : 'please send location';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $order_location=order_locations::find($request->address_id);
        $check=$this->not_found($order_location,'العنوان','Adress',$lang);
        if(isset($check))
        {
            return $check;
        }

        if($user_id != $order_location->user_id)
        {
            $msg=$lang=='ar' ? 'العنوان لا يخص العضو'  : 'location not belongs to this user';
            return $this->apiResponseMessage(0,$msg,200);
        }

    }


    /*
     * @pram order column ,
     * @return success or validate
     */

    public function edit_order(Request $request,$order_id)
    {
        $lang=$request->header('lang');

        $order=Order::find($order_id);
        $check=$this->not_found($order,'الطلب','order',$lang);
        if(isset($check)){
            return $check;
        }
        $validate_order=$this->validate_order($request);
        if(isset($validate_order)){
            return $validate_order;
        }

        $user=$request->user_id;
        $user_d = User::find($user);
        $representative = $request->representative_id;
        $representative_d = User::where('id' , '=' , $representative)->where('status' , '=' , '2')->first();

        $check=$this->not_found($representative_d,'المندوب','Representative',200);
        if(isset($check))
        {
            return $check;
        }

        $check=$this->not_found($user_d,'العضو','user',200);
        if(isset($check))
        {
            return $check;
        }
        $order->user_id=$user;
        $order->representative_id=$representative;
        $order->name=$request->name;
        $order->status=$request->status;
        $order->email=$request->email;
        $order->phone=$request->phone;
        $order->address_id=$request->address_id;
        $order->payment_method=$request->payment_method;
        $order->shipping_price=$request->shipping_price;
        $order->save();
        $msg=$lang=='ar' ? 'تم تعديل الطلب بنجاح'  : 'Order Edited successfully';
        return $this->apiResponseData(new OrderResource($order),'success');
    }

    /*
     * Add Product To Order
     */

    public function add_product_to_order(Request $request,$order_id)
    {
        $lang=$request->header('lang');
        $order=Order::find($order_id);
        $check=$this->not_found($order,'الطلب','Order',$lang);
        if(isset($check)){
            return $check;
        }
        $product=Products::find($request->product_id);
        $check=$this->not_found($product,'المنتج','Product',$lang);
        if(isset($check)){
            return $check;
        }
        $Order_product=Order_product::where('order_id',$order_id)->where('product_id',$request->product_id)->first();
        if(!is_null($Order_product)){
            $msg=$lang == 'ar' ?'المنتج موجود في هذا الطلب' : 'Product already exist in this order';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Order_product=new Order_product;
        $Order_product->order_id=$order_id;
        $Order_product->product_id=$request->product_id;
        $Order_product->quantity=$request->quantity;
        $Order_product->color_id=$request->color_id;
        $Order_product->size_id=$request->size_id;
        $Order_product->save();
        $msg=$lang == 'ar' ?'تم اضافة المنتج بنجاح' : 'Product added successfully';
        return $this->apiResponseMessage(1,$msg,200);

    }

    /*
     * @pram $request
     * @return Error message or check if user is null
    */

    private function validate_order($request){
    $lang = $request->header('lang');
    $input = $request->all();
    $validationMessages = [
        'user_id.required' => $lang == 'ar' ?  'من فضلك ادخل رقم العميل' :"client is required" ,
        'address_id.required' => $lang == 'ar' ? 'من فضلك ادخل العنوان ' :"address is required"  ,
    ];

    $validator = Validator::make($input, [
        'user_id' => 'required',
        'address_id' => 'required',
    ], $validationMessages);

    if ($validator->fails()) {
        return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
    }

    $user=User::find($request->user_id);
    $check=$this->not_found($user,'العميل','Client',$lang);
    if(isset($check)){
        return $check;
    }
}


}
