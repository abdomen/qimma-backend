<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Models\Price;
use App\Models\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\PriceResource;
use App\Http\Controllers\Manage\BaseController;

class PriceController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * get all Prices
     */
    public function all_Price (Request $request){
        $user=Auth::user();
        $page=$request->page * 20;
        $Prices=Price::orderBy('id','desc')->get();
        return $this->apiResponseData(PriceResource::collection($Prices),'success');
    }


    /*
    * Delete Prices
    */
    public function delete_Price(Request $request,$Price_id)
    {
        $lang=$request->header('lang');
        $Price=Price::find($Price_id);
        $check=$this->not_found($Price,'السعر','Price',$lang);
        if(isset($check)){
            return $check;
        }
        $Price->delete();
        $msg=$lang=='ar' ? 'تم حذف السعر بنجاح'  : 'Price Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }



    /*
     * Single Price to show data
     */
    public function single_Price(Request $request,$Price_id)
    {
        $lang=$request->header('lang');
        $Price=Price::find($Price_id);
        $check=$this->not_found($Price,'السعر','Price',$lang);
        if(isset($check)){
            return $check;
        }
        return $this->apiResponseData(new PriceResource($Price),'success');
    }

    /*
     * Add new Price
     * add general Price column , add products to this Price
     */

    public function add_Price(Request $request)
    {
        
        $lang=$request->header('lang');
        $user=Auth::user();
        $check=$this->not_found($user,'العضو','user',200);
        if(isset($check))
        {
            return $check;
        }
        $validate_Price=$this->validate_Price($request);
        if(isset($validate_Price)){
            return $validate_Price;
        }

        $Price=new Price();
        $Price->purchasing_price=$request->purchasing_price;
        $Price->wholesale_wholesale_price=$request->wholesale_wholesale_price;
        $Price->wholesale_price=$request->wholesale_price;
        $Price->selling_price=$request->selling_price;
        $Price->save();
        
        $msg=$lang=='ar' ?  'تم تسجيل السعر للمنتج بنجاح' :'Product price saved successfully' ;
        return $this->apiResponseData(new PriceResource($Price),$msg);
    }



    /*
     * @pram Price column ,
     * @return success or validate
     */

    public function edit_Price(Request $request,$Price_id)
    {
        $lang=$request->header('lang');
        $Price=Price::find($Price_id);
        $check=$this->not_found($Price,'السعر','Price',$lang);
        if(isset($check)){
            return $check;
        }
        $validate_Price=$this->validate_Price($request);
        if(isset($validate_Price)){
            return $validate_Price;
        }

        $Price->purchasing_price=$request->purchasing_price;
        $Price->wholesale_wholesale_price=$request->wholesale_wholesale_price;
        $Price->wholesale_price=$request->wholesale_price;
        $Price->selling_price=$request->selling_price;
        $Price->save();
        $msg=$lang=='ar' ? 'تم تعديل السعر للمنتج بنجاح'  : 'Product price edited successfully';
        return $this->apiResponseData(new PriceResource($Price),$msg);
    }


    /*
     * @pram $request
     * @return Error message or check if user is null
    */

    private function validate_Price($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'purchasing_price.required' => $lang == 'ar' ? 'من فضلك ادخل سعر الشراء' :"Purchasing price is required"  ,
            'wholesale_wholesale_price.required' => $lang == 'ar' ? 'من فضلك ادخل سعر جملة الجملة' :"Wholesale wholesale price price is required"  ,
            'wholesale_price.required' => $lang == 'ar' ? 'من فضلك ادخل سعر الجملة' :"Wholesale price is required"  ,
            'selling_price.required' => $lang == 'ar' ? 'من فضلك ادخل سعر البيع' :"Selling price is required"  ,
        ];
        $validator = Validator::make($input, [
            'purchasing_price' => 'required',
            'wholesale_wholesale_price' => 'required',
            'wholesale_price' => 'required',
            'selling_price' => 'required'
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
    }
}
