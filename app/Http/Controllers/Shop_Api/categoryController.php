<?php

namespace App\Http\Controllers\Shop_Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\Shop\CategoryResource;
use App\Models\color;
use App\Models\Product_details;
use App\Models\Product_details_price;
use App\Models\Price;
use App\Models\Product_detail_inventory;
use App\Models\Product_color;
use App\Models\Produc_view_user;
use App\Models\Product_Size;
use App\Models\Product_image;
use App\Models\Products;
use App\Models\Currency;
use App\Models\Product_Currency;

class categoryController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new category to database
     */
    public function add_category(Request $request)
    {
        $user=Auth::user();
        $validate_Category=$this->validate_Category($request);
        if(isset($validate_Category)){
            return $validate_Category;
        }
        $Category=new Category();
        $Category->name_ar=$request->name_ar;
        $Category->name_en=$request->name_en;
        $Category->desc_ar=$request->desc_ar;
        $Category->desc_en=$request->desc_en;
        $Category->level=$request->level;
        $Category->status=$request->status;
        $Category->parent_id=$request->level == 1 ? 0 : $request->cat_id;
        if($request->icon){
            $name=BaseController::saveImage('Category',$request->file('icon'));
            $Category->icon=$name;
        }
        $Category->mainShop_id = $user->id;
        $Category->save();
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم اضافة القسم بنجاح'  : 'Category add successfully';
        return $this->apiResponseData(new CategoryResource($Category),$msg);

    }

    /*
     * Edit product
    */
    public function edit_category(Request $request,$Category_id)
    {
        $lang=$request->header('lang');
        $Category=Category::find($Category_id);
        $check=$this->not_found($Category,'القسم','Category',$lang);
        if(isset($check)){
            return $check;
        }
        $validate_Category=$this->validate_Category($request);
        if(isset($validate_Category)){
            return $validate_Category;
        }

        $Category->name_ar=$request->name_ar;
        $Category->name_en=$request->name_en;
        $Category->desc_ar=$request->desc_ar;
        $Category->desc_en=$request->desc_en;
        $Category->level=$request->level;
        $Category->status=$request->status;
        $Category->parent_id=$request->level == 1 ? 0 : $request->cat_id;
        if($request->icon){
            BaseController::deleteFile('Category',$Category->icon);
            $name=BaseController::saveImage('Category',$request->file('icon'));
            $Category->icon=$name;
        }
        $Category->save();
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم تعديل القسم بنجاح'  : 'Category Edited successfully';
        return $this->apiResponseData(new CategoryResource($Category),$msg);
    }

    /*
     * Show single category
     */
    public function single_category(Request $request,$Category_id){
        $lang=$request->header('lang');
        $Category=Category::find($Category_id);
        $check=$this->not_found($Category,'القسم','Category',$lang);
        if(isset($check)){
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new CategoryResource($Category),$msg);
    }

    /*
     * Delete Product ..
     */

    public function delete_category(Request $request,$Category_id){
        $lang=$request->header('lang');
        $Category=Category::where('id',$Category_id)->where('id','!=',1)->first();
        $check=$this->not_found($Category,'القسم','Product',$lang);
        if(isset($check)){
            return $check;
        }

        BaseController::deleteFile('Category',$Category->icon);

        if (Products::where('cat_id',$Category_id)->exists()){
            $pro_ids = Products::where('cat_id',$Category_id)->pluck('id');
            $this->delete_product($pro_ids);
        }

        $Category->delete();
        $msg=$lang=='ar' ? 'تم حذف القسم بنجاح'  : 'Category Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }


    public function delete_product($pro_ids){
        foreach ($pro_ids as $product_id){
            $product = Products::find($product_id);
            
            BaseController::deleteFile('Products',$product->iamge);

            if (Product_details::where('product_id',$product_id)->exists()){
                $pro_det_ids = Product_details::where('product_id',$product_id)->pluck('id');
                $this->delete_product_details($pro_det_ids);
            }

            $product->delete();
        }
    }

    public function delete_product_details ($pro_det_ids){
        foreach ($pro_det_ids as $Product_details_id){
            $Product_details=Product_details::where('id',$Product_details_id)->first();
            if(Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->exists()){
                $Product_details_price = Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->first();

                if(Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->value('price_id') != null){
                    
                    $Price_id = Product_details_price::where('product_detail_id', '=' ,$Product_details_id)->value('price_id');
                    if(Price::where('id',$Price_id)->exists()){Price::where('id',$Price_id)->delete();}

                }
                
                $Product_details_price->delete();
            }
            
            if(Product_detail_inventory::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_detail_inventory::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Product_Currency::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_Currency::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Product_color::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_color::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Produc_view_user::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Produc_view_user::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Product_Size::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_Size::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            if(Product_image::where('product_detail_id' , '=' , $Product_details_id)->exists()){
                Product_image::where('product_detail_id' , '=' , $Product_details_id)->delete();
            }

            BaseController::deleteFile('Product_details',$Product_details->iamge);
            foreach ($Product_details->images as $row)
            {
                BaseController::deleteFile('Product_image',$row->iamge);
            }
            
            $Product_details->delete();
        }
    }


    /*
     * @pram $request
     * @return Error message or check if cateogry is null
     */

    private function validate_Category($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'name_ar.required' => $lang == 'ar' ?  'من فضلك ادخل رقم اسم القسم بالعربية' :"name in arabic is required" ,
            'name_en.required' => $lang == 'ar' ? 'من فضلك ادخل رقم اسم القسم بالانجليزية' :"name in english is required"  ,
            'image.required' => $lang == 'ar' ? 'من فضلك ادخل صورة القسم ' :"image is required"  ,
            'cat_id.required' => $lang == 'ar' ?  ' من فضلك ادخل القسم الرئيسي' :"main category is required" ,
        ];
        $validator = Validator::make($input, [
            'name_ar' => 'required',
            'name_en' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'cat_id' => $request->level ==2 ?'required|numeric' : '',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        if($request->level == 2) {
            $cat = Category::where('id',$request->cat_id)->where('level',1)->first();
            $check = $this->not_found($cat, 'القسم الرئيسي', 'main Category', $lang);
            if (isset($check)) {
                return $check;
            }
        }
    }
}