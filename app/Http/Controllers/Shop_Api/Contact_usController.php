<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\Contact_usResource;
use App\Models\Contact_us;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt,DB;
use App\Http\Resources\UserResource;
use App\Http\Resources\Shop\ProductResource;
use App\Http\Controllers\Manage\BaseController;

class Contact_usController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;
    
    
    /*
     * Add Contact_us
     */
     
    public function add_Contact_us(Request $request)
    {
        $lang=$request->header('lang');
        
        $Contact_us = new Contact_us;
        $Contact_us->name=$request->name;
        $Contact_us->email=$request->email;
        $Contact_us->phone = $request->phone;
        $Contact_us->message = $request->message;
        $Contact_us->mainShop_id = $request->mainShop_id;
        $Contact_us->save();
        $msg=$lang=='ar' ? 'تم اضافة الرسالة بنجاح' : 'Contact_us added successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * Delete Contact_us
     */

    public function delete_Contact_us(Request $request,$Contact_us_id)
    {
        $lang=$request->header('lang');
        $Contact_us=Contact_us::find($Contact_us_id);
  $check=$this->not_found($Contact_us,'الرسالة','message',$lang);
        if(isset($check))
        {
            return $check;
        }        $Contact_us->delete();
        $msg=$lang=='ar' ? 'تم حذف الرسالة بنجاح' : 'message deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single Contact_us
     */

    public function single_Contact_us(Request $request,$Contact_us_id)
    {
        $lang=$request->header('lang');
        $Contact_us=Contact_us::find($Contact_us_id);
        $check=$this->not_found($Contact_us,'الرسالة','message',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData($Contact_us,'success',200);
    }

    /*
     * All Contact_uss
     */

    public function all_Contact_us(Request $request)
    {
        $lang=$request->header('lang');
        $Contact_uss=Contact_us::where('mainShop_id' , $request->mainShop_id)->get();
        return $this->apiResponseData($Contact_uss,'success',200);
    }
}