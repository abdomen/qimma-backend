<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\Shop\AboutResource;
use App\Models\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt,DB;
use App\Http\Controllers\Manage\BaseController;
class AboutController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new user to Auth Shop database
     */
    public function add_About(Request $request)
    {
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'title_ar.required' => $lang == 'ar' ?  'من فضلك ادخل العنوان بالعربية ' :" title in arabic is required" ,
            'title_en.required' => $lang == 'ar' ?  'من فضلك ادخل العنوان بالانجليزية ' :" title in english is required" ,
            'desc_ar.required' => $lang == 'ar' ?  'من فضلك ادخل المحتوى بالعربية ' :" content in arabic is required" ,
            'desc_en.required' => $lang == 'ar' ?  'من فضلك ادخل المحتوى بالانجليزية ' :" content in english is required" ,

        ];

        $validator = Validator::make($input, [
            'title_ar' => 'required',
            'title_en' => 'required',
            'desc_ar' => 'required',
            'desc_en' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $shop = Auth::user();

        $About = new About();
        $About->title_ar = $request->title_ar;
        $About->title_en = $request->title_en;
        $About->desc_ar  = $request->desc_ar;
        $About->desc_en  = $request->desc_en;
        $About->shop_id  = $shop->mainShop_id;
        $About->save();
        $msg=$lang == 'ar' ? 'تمت الاضافة بنجاح' : 'Success';
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new AboutResource($About)]);

    }

    /*
     * Edit user information
    */
    public function edit_About(Request $request,$id)
    {
        $lang = $request->header('lang');
        $About = About::find($id);
        $check=$this->not_found($About,'عنا','About',$lang);
        if(isset($check))
        {
            return $check;
        }

        $input = $request->all();
        $validationMessages = [
            'title_ar.required' => $lang == 'ar' ?  'من فضلك ادخل العنوان بالعربية ' :" title in arabic is required" ,
            'title_en.required' => $lang == 'ar' ?  'من فضلك ادخل العنوان بالانجليزية ' :" title in english is required" ,
            'desc_ar.required' => $lang == 'ar' ?  'من فضلك ادخل المحتوى بالعربية ' :" content in arabic is required" ,
            'desc_en.required' => $lang == 'ar' ?  'من فضلك ادخل المحتوى بالانجليزية ' :" content in english is required" ,

        ];

        $validator = Validator::make($input, [
            'title_ar' => 'required',
            'title_en' => 'required',
            'desc_ar' => 'required',
            'desc_en' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }


        $About->title_ar = $request->title_ar;
        $About->title_en = $request->title_en;
        $About->desc_ar = $request->desc_ar;
        $About->desc_en = $request->desc_en;
        $About->save();
        $msg=$lang=='ar' ?  'تم تعديل البيانات ' :'Edited successfully' ;
        return $this->apiResponseData(  new AboutResource($About),  $msg);
    }

    /*
     * Show single  user
    */
    public function single_About(Request $request,$about_id){
        $lang=$request->header('lang');
        $About = About::find($about_id);
        $check=$this->not_found($About,'عنا','About',$lang);
        if(isset($check))
        {
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(  new AboutResource($About),  $msg);

    }

    /*
     * Delete user ..
     */

    public function delete_About(Request $request,$about_id){
        $lang=$request->header('lang');
        $About = About::where('id',$about_id)->where('id','!=',1)->first();
        $check=$this->not_found($About,'عنا','About',$lang);
        if(isset($check))
        {
            return $check;
        }

        $About->delete();
        $msg=$lang=='ar' ? 'تم الحذف بنجاح'  : ' Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * get All user for Auth shop
     */
    public function all_About(Request $request)
    {
        $abouts=About::where('shop_id' , $request->mainShop_id)->orderBy('id','desc')->get();
        return $this->apiResponseData(AboutResource::collection($abouts),'success');
    }

}
