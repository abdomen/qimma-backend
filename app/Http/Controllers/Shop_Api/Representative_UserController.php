<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Safe;
use App\Models\Safe_details;
use App\Models\Representative_User;
use App\Http\Resources\Representative_UserResource;
use App\Http\Resources\Admin_debt_logResource;
use App\Models\Admin_debt_log;
use App\Http\Resources\User_debt_logResource;
use App\Models\User_debt_log;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;
// use App\Http\Controllers\Api\NotificationMethods;


class Representative_UserController extends Controller
{

    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new Rep_Inv to database
     */
    public function add_user_representative(Request $request, $representative_id)
    {
        $lang = $request->header('lang');
        $user = $request->user_id;

        if(Representative_User::where('user_id',$user)->where('representative_id',$representative_id)->exists()){
            $msg = $lang=='ar' ? 'هذاالعميل مضاف لقائمة عملاءهذاالمندوب من قبل'  : 'This user is already exists in this representative users list';
            return $this->apiResponseMessage(0,$msg,200);
        }elseif(Representative_User::where('user_id',$user)->exists()){
            $msg = $lang=='ar' ? 'هذاالعميل مضاف لقائمة عملاء مندوب اخر'  : 'This user is already exists in another representative users list';
            return $this->apiResponseMessage(0,$msg,200);
        }else{
            $user_representative = new Representative_User();
            $user_representative->representative_id = $representative_id;
            $user_representative->user_id = $user;
            $user_representative->save();
            
            $msg = $lang=='ar' ? 'تم اضافةالعميل للمندوب بنجاح'  : 'user_representative added successfully';
            return $this->apiResponseData(new Representative_UserResource($user_representative),$msg);
        }
    }


    /*
     * get All product for Auth shop
     */
    public function all_user_representative(Request $request)
    {
        $user=Auth::user();
        $page=$request->page * 20;
        $user_representative=Representative_User::orderBy('representative_id','DESC')->get();
        return $this->apiResponseData(Representative_UserResource::collection($user_representative),'success');
    }


    /*
     * get All product for Auth shop
     */
    public function representatives_of_user(Request $request , $user_id)
    {
        $user=Auth::user();
        $page=$request->page * 20;
        $user_representative=Representative_User::select()->where('user_id',$user_id)->orderBy('representative_id','DESC')->get();
        return $this->apiResponseData(Representative_UserResource::collection($user_representative),'success');
    }


    public function users_of_representative(Request $request , $representative_id)
    {
        $user=Auth::user();
        $user_ids=Representative_User::where('representative_id',$representative_id)->pluck('user_id');
        $users = User::select('id','first_name','last_name','debt')->whereIn('id',$user_ids)->get();
        return $this->apiResponseData($users,'success');
    }



    /*
     * Delete Rep_Inv ..
     */

    public function delete_user_representative(Request $request , $user_id)
    {
        $lang=$request->header('lang');

        $user_representative = Representative_User::where('user_id',$user_id)->first();
        $check=$this->not_found($user_representative,'عميل_مندوب','user_representative',$lang);
        if(isset($check))
        {
            return $check;
        }

        $user_representative->delete();

        $msg=$lang=='ar' ? 'تم حذف عميل_مندوب'  : 'user_representative Deleted successfully';

        return $this->apiResponseMessage(1,$msg,200);
    }


    private function validate_user_representative($request)
    {
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'representative_id.required' => $lang == 'ar' ?  'من فضلك ادخل كود المندوب' :"Representative_id is required" ,
            'user_id.required' => $lang == 'ar' ? 'من فضلك ادخل كود العميل' :"User_id is required"  ,
        ];
        $validator = Validator::make($input, [
            'representative_id' => 'required',
            'user_id' => 'required',
        ], $validationMessages);
        if ($validator->fails())
        {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }

    }
    
    public function get_representative_location(Request $request,$representative_id)
    {
       // DB::table('users');
        $user=Auth::user();
        $page=$request->page * 20;
        $represent_location=User::select('id','lat', 'lng')->where('id',$representative_id)->get();
        return $this->apiResponseData($represent_location,'success');

    }

    public function get_debt(Request $request,$id)
    {
        $representative = User::find($id);
        $representative_debt = $representative->debt;
        return $this->apiResponseData((int)$representative_debt,'success');

    }

    public function debt_set(Request $request,$id)
    {
        $admin = Auth::user();
        
        $representative = User::find($id);

        if($representative->safe_id == null){
            $safe = new Safe;
            $safe->name_ar = "خزنة $representative->first_name $representative->last_name";
            $safe->name_en = "$representative->first_name $representative->last_name Safe";
            $safe->status = 1;
            $safe->total_money = $representative->debt;
            $safe->save();

            $representative->safe_id = $safe->id;
            $representative->save();
        }

        if($request->type == '2'){
            $representative_debt = ($representative->debt)-($request->paid);
            $safe_detail = new \stdClass();
            $safe_detail->safe_id = $representative->safe_id;
            $safe_detail->process_name_ar = 'تحصيل من مديونية مندوب';
            $safe_detail->process_name_en = 'Representative Debt Setting';
            $safe_detail->process_type = 2;
            $safe_detail->value = $request->paid;
            SafeController::add_safe_process($safe_detail);

            $safe_detail = new \stdClass();
            if($request->safe_id){
                $safe_detail->safe_id = $request->safe_id;
            }else{
                $safe_detail->safe_id = 1;
            }
            $safe_detail->process_name_ar = 'تحصيل من مديونية مندوب';
            $safe_detail->process_name_en = 'Representative Debt Setting';
            $safe_detail->process_type = 1;
            $safe_detail->value = $request->paid;
            SafeController::add_safe_process($safe_detail);
        }else{
            $representative_debt = ($representative->debt)+($request->paid);
            $safe_detail = new \stdClass();
            $safe_detail->safe_id = $representative->safe_id;
            $safe_detail->process_name_ar = 'إضافة علي مديونية مندوب';
            $safe_detail->process_name_en = 'Representative Debt Addition';
            $safe_detail->process_type = 1;
            $safe_detail->value = $request->paid;
            SafeController::add_safe_process($safe_detail);

            $safe_detail = new \stdClass();
            if($request->safe_id){
                $safe_detail->safe_id = $request->safe_id;
            }else{
                $safe_detail->safe_id = 1;
            }
            $safe_detail->process_name_ar = 'تحصيل من مديونية مندوب';
            $safe_detail->process_name_en = 'Representative Debt Setting';
            $safe_detail->process_type = 2;
            $safe_detail->value = $request->paid;
            SafeController::add_safe_process($safe_detail);
        }
        
        $representative->debt = $representative_debt;
        $representative->save();

        $log = new Admin_debt_log;
        $log->representative_id = $representative->id;
        $log->admin_id = $admin->id;
        $log->type = $request->type == 2 ? 'subtraction' : 'addition'; 
        $log->value = $request->paid;
        $log->save();

        return $this->apiResponseData(new Admin_debt_logResource($log),'success');
    }


    public function user_debt_set(Request $request,$id)
    {
        $admin = Auth::user();
        
        $user = User::find($id);

        if($request->type == '2'){
            $user_debt = ($user->debt)-($request->paid);
            $safe_detail = new \stdClass();
            if($request->safe_id){
                $safe_detail->safe_id = $request->safe_id;
            }else{
                $safe_detail->safe_id = 1;
            }
            $safe_detail->process_name_ar = 'تحصيل من مديونية عميل';
            $safe_detail->process_name_en = 'User Debt Setting';
            $safe_detail->process_type = 1;
            $safe_detail->value = $request->paid;
            SafeController::add_safe_process($safe_detail);
        }else{
            $user_debt = ($user->debt)+($request->paid);
            $safe_detail = new \stdClass();
            if($request->safe_id){
                $safe_detail->safe_id = $request->safe_id;
            }else{
                $safe_detail->safe_id = 1;
            }
            $safe_detail->process_name_ar = 'إضافة علي مديونية عميل';
            $safe_detail->process_name_en = 'User Debt Addition';
            $safe_detail->process_type = 2;
            $safe_detail->value = $request->paid;
            SafeController::add_safe_process($safe_detail);
        }
        
        $user->debt = $user_debt;
        $user->save();

        $log = new User_debt_log;
        $log->user_id = $user->id;
        $log->admin_id = $admin->id;
        $log->type = $request->type == 2 ? 'subtraction' : 'addition'; 
        $log->value = $request->paid;
        $log->save();

        return $this->apiResponseData(new User_debt_logResource($log),'success');
    }
}
