<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Http\Resources\ColorResource;
use App\Models\Discount;
use App\Models\user_Discount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\Shop\CodeResource;
use App\User;
use App\Models\Shop;
use App\Http\Controllers\Manage\BaseController;
class DiscountController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new code to database
     * add this code to one or multi users
     */
    public function create_code(Request $request)
    {
        $user=Auth::user();
        $validate_code=$this->validate_code($request,0);
        if(isset($validate_code)){
            return $validate_code;
        }
        $code=new Discount();
        $code->name_ar=$request->name_ar;
        $code->name_en=$request->name_en;
        $code->amount=$request->amount;
        $code->amount_type=$request->amount_type;
        $code->end_date=$request->end_date;
        $code->code=$request->code;
        $code->desc_ar=$request->desc_ar;
        $code->desc_en=$request->desc_en;
        $code->mainShop_id = $user->id;
        $code->save();
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم اضافة الكود بنجاح'  : 'code added successfully';
        return $this->apiResponseData(new CodeResource($code),$msg);

    }

    /*
     * function to add code to coustom users
     */
    public function add_users_to_code(Request $request,$code_id)
    {
        $lang = $request->header('lang');
        $code=Discount::find($code_id);
        if(is_null($code)){
            $msg=$lang=='ar' ? 'هذا الكود غير موجود' : 'this code not found';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $user=User::find($request->user_id);
        $check=$this->not_found($user,'العضو','user',$lang);
        if(isset($check))
        {
            return $check;
        }
        $user_code=user_Discount::where('user_id',$request->user_id)->where('code_id',$code_id)->first();
        if(is_null($user_code)) {
            $user_code = new user_Discount;
            $user_code->user_id = $request->user_id;
            $user_code->code_id = $code_id;
            $user_code->save();
            $msg = $lang == 'ar' ? 'تم اضافة العضو بنجاح' : 'user added successfully';
            $title=$lang == 'ar' ? $code->name_ar: $code->name_en;
            $desc=$lang == 'ar' ? $code->desc_ar: $code->desc_en;
          return  NotificationMethods::senNotificationToSingleUser($user->fire_base_token,$title,$desc,null,3,$code->id);

            return $this->apiResponseMessage(1, $msg, 200);
        }

        $msg = $lang == 'ar' ? 'هذا العضو لديه الكود' : 'user already has this code';
        return $this->apiResponseMessage(0, $msg, 200);

    }


    /*
     * Delete user from code
     */

    public function delete_users_from_code(Request $request,$code_id)
    {
        $lang = $request->header('lang');
        $code=Discount::find($code_id);
        if(is_null($code)){
            $msg=$lang=='ar' ? 'هذا الكود غير موجود' : 'this code not found';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $user_Discount=user_Discount::where('user_id',$request->user_id)->where('code_id',$code_id)->first();
        if(is_null($user_Discount)){
            $msg=$lang=='ar' ? 'هذا العضو غير موجود' : 'this user not found';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $user_Discount->delete();
        $msg=$lang=='ar' ? 'تم الحذف بنجاح' : 'deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);

    }

    /*
     * @pram code_id , Request
     * @return Vslidate message , success message
     */
    public function edit_code(Request $request,$code_id)
    {
        $lang=$request->header('lang');
        $code=Discount::find($code_id);
        if(is_null($code)){
            $msg=$lang=='ar' ? 'هذا الكود غير موجود' : 'this code not found';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $validate_code=$this->validate_code($request,$code_id);
        if(isset($validate_code)){
            return $validate_code;
        }
        $code->name_ar=$request->name_ar;
        $code->name_en=$request->name_en;
        $code->amount=$request->amount;
        $code->amount_type=$request->amount_type;
        $code->end_date=$request->end_date;
        $code->code=$request->code;
        $code->desc_ar=$request->desc_ar;
        $code->desc_en=$request->desc_en;
        $code->save();
        $msg=$lang=='ar' ? 'تم تعديل الكود بنجاح'  : 'code edited successfully';
        return $this->apiResponseData(new CodeResource($code),$msg);
    }


    /*
     * Show single  product
     */
    public function single_code(Request $request,$code_id){
        $lang=$request->header('lang');
        $code=Discount::find($code_id);
        $check=$this->not_found($code,'الكود','code',$lang);
        if(isset($check)){
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new CodeResource($code),$msg);

    }

    /*
     * Delete Product ..
     */

    public function delete_code(Request $request,$code_id){
        $lang=$request->header('lang');

        $code=Discount::find($code_id);
        $check=$this->not_found($code,'الكود','code',$lang);
        if(isset($check)){
            return $check;
        }
        $code->delete();
        $msg=$lang=='ar' ? 'تم حذف الكود بنجاح'  : 'code Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * get All product for Auth shop
     */
    public function get_codes(Request $request)
    {
        $user=Auth::user();
        $page=$request->page * 20;
        $codes=Discount::skip($page)->take(20)->where('mainShop_id' , $user->id)->get();
        return $this->apiResponseData(CodeResource::collection($codes),'success');
    }


    /*
     * @pram $request
     * @return Error message or check if cateogry is null
     */

    private function validate_code($request,$id){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'name_ar.required' => $lang == 'ar' ?  'من فضلك ادخل رقم اسم الكود بالعربية' :"name in arabic is required" ,
            'name_en.required' => $lang == 'ar' ? 'من فضلك ادخل رقم اسم الكود بالانجليزية' :"name in english is required"  ,
            'amount.numeric' => $lang == 'ar' ?  'يجب ان تكون القيمة رقم صحيح' :"The amount must be a number" ,
            'amount.required' => $lang == 'ar' ? 'من فضلك ادخل قيمة الخصم ' :"amount is required"  ,
            'amount_type.in' => $lang == 'ar' ?  'القيمة المدخلة غير صحيحة' :"The selected amount type is invalid" ,
            'amount_type.required' => $lang == 'ar' ? 'من فضلك ادخل نوع الخصم ' :"amount type is required"  ,
            'code.unique' => $lang == 'ar' ?  'هذا الكود مستخدم لدينا من قبل' :"This code is already used" ,
            'code.required' => $lang == 'ar' ? 'من فضلك ادخل الكود ' :"code type is required"  ,


        ];

        $validator = Validator::make($input, [
            'name_ar' => 'required',
            'name_en' => 'required',
            'amount' => 'required|numeric',
            'amount_type'=>'in:number,percentage|required',
            'code'=>$id != 0 ? 'required|unique:discounts,code,'.$id : 'required|unique:discounts',

        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }

    }


}