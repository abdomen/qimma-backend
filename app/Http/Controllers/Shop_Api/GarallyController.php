<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\GarallyResource;
use App\Models\Garally;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;

class GarallyController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
        * Add new Garally
    */

    public function add_Garally(Request $request)
    {
        $lang=$request->header('lang');
        if(!$request->image)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل الصورة' : 'image is required';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $Garally = new Garally;
        $Garally->image=BaseController::saveImage('Garally',$request->image);
        $Garally->mainShop_id = $request->mainShop_id;
        $Garally->save();
        $msg=$lang=='ar' ? 'تم اضافة البراند' : 'image added successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Delete Garally
     */

    public function delete_Garally(Request $request,$Garally_id)
    {
        $lang=$request->header('lang');
        $Garally=Garally::find($Garally_id);
        $check=$this->not_found($Garally,'البراند','image',$lang);
        if(isset($check))
        {
            return $check;
        }
        BaseController::deleteFile('Garally',$Garally->image);
        $Garally->delete();
        $msg=$lang=='ar' ? 'تم حذف البراند بنجاح' : 'image deleted successfully';
        return $this->apiResponseMessage(0,$msg,200);
    }

    /*
     * single Garally
     */

    public function single_Garally(Request $request,$Garally_id)
    {
        $lang=$request->header('lang');
        $Garally=Garally::find($Garally_id);
        $check=$this->not_found($Garally,'البراند','image',$lang);
        if(isset($check))
        {
            return $check;
        }

        return $this->apiResponseData(new GarallyResource($Garally),'success',200);
    }

    /*
     * All Garallys
     */

    public function all_Garally(Request $request)
    {
        $lang=$request->header('lang');
        $Garallys=Garally::where('mainShop_id' , $request->mainShop_id )->get();
        return $this->apiResponseData(GarallyResource::collection($Garallys),'success',200);
    }
}
