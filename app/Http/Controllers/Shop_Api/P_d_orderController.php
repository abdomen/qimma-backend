<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Controllers\Api\NotificationMethods;
use App\Http\Controllers\Shop_Api\SafeController;
use App\Http\Resources\ColorResource;
use App\Http\Resources\Order_product_contResource;
use App\Http\Resources\Product_detailsCollectionResource;
use App\Http\Resources\Product_detailsResource;
use App\Models\Order_product_details;
use App\Models\Product_details;
use App\Models\Products;
use App\Models\Price;
use App\Models\Product_details_price;
use App\Models\Inventory;
use App\Models\Product_detail_inventory;
use App\Models\Order_status;
use App\Models\Safe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\order_locations;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\P_d_ordersResource;
use App\Http\Resources\Order_product_detailsResource;
use App\Http\Resources\locationResource;
use App\Http\Resources\Order_returnResource;
use App\Http\Resources\ProductInventoriesResource;
use App\Http\Resources\MostsellResource;
use App\Http\Resources\BillResource;
use App\User;
use App\Models\P_d_orders;
use App\Models\Shop;
use App\Models\color;
use App\Models\Sizes;
use App\Models\Order_return;
use DB;
use App\Http\Controllers\Manage\BaseController;
use Carbon\Carbon;

class P_d_orderController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * get all P_d_orders
     */
    public function all_p_d_Order (Request $request){
        $ord_pro = Order_product_details::pluck('p_d_order_id');
        $P_d_orders = P_d_orders::whereIn('id', $ord_pro)->orderBy('created_at','desc')->get();
        return $this->apiResponseData(P_d_ordersResource::collection($P_d_orders),'success');
    }



    /*
     * Filter P_d_order
     */
    public function filter_P_d_orders(Request $request)
    {
        // $P_d_orders=P_d_orders::orderby('created_at','desc');
        if ($request->type == 1){
            $P_d_orders = P_d_orders::whereDate('created_at', Carbon::now())->get();
            // return $P_d_orders;
        }elseif ($request->type==2) {
            $P_d_orders = P_d_orders::whereMonth('created_at', Carbon::now())->get();
        }elseif ($request->type==3){
            $P_d_orders= P_d_orders::whereYear('created_at', Carbon::now())->get();
        }else{
            return $this->apiResponseMessage(0,'هذا النوع غير صحيح',200);
        }

        // $P_d_orders=$P_d_orders->get();
        return $this->apiResponseData(P_d_ordersResource::collection($P_d_orders),'success');

    }


    
    /*
     * Filter P_d_order
     */
    public function filter_P_d_orders_by_date(Request $request)
    {
        
        $from = date($request->from);
        $to = date($request->to);
        $dated_orders = P_d_orders::whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->get();
        return $this->apiResponseData(P_d_ordersResource::collection($dated_orders),'success');

    }

    /*
     * Order status
     */
    
    public function add_order_status(Request $request)
    {
        $status = new Order_status;
        $status->status_ar = $request->status_ar;
        $status->status_en = $request->status_en;
        $status->save();
        return $this->apiResponseData($status,'success');
    }

    public function edit_order_status(Request $request, $status_id)
    {
        $lang=$request->header('lang');

        $status = Order_status::find($status_id);
        $check=$this->not_found($status,'الحالة','Status',$lang);
        if(isset($check)){
            return $check;
        }
        $status->status_ar = $request->status_ar;
        $status->status_en = $request->status_en;
        $status->save();
        return $this->apiResponseData($status,'success');
    }

    public function delete_order_status(Request $request,$status_id)
    {
        $lang=$request->header('lang');

        $status = Order_status::find($status_id);
        $check=$this->not_found($status,'الحالة','Status',$lang);
        if(isset($check)){
            return $check;
        }
        $status->delete();
        $msg=$lang=='ar' ? 'تم حذف الحالة بنجاح'  : 'Status Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }


    public function show_order_status(Request $request)
    {
        $status = Order_status::get();
        return $this->apiResponseData($status,'success');
    }


    public function filter_by_status(Request $request)
    {
        if ($request->status == 0){
            $P_d_orders=P_d_orders::orderBy('created_at','desc')->get();
        }else{
            $P_d_orders=P_d_orders::orderBy('created_at','desc')->where('status',$request->status)->get();
        }
        return $this->apiResponseData(P_d_ordersResource::collection($P_d_orders),'success');
    }


    /*
    * Delete P_d_orders 
    */
    public function delete_P_d_order(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');
        $P_d_order=P_d_orders::find($P_d_order_id);
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }

        if($P_d_order->bank_trans_receipt != null) {
            BaseController::deleteFile('Order',$P_d_order->bank_trans_receipt);
        }

        $old_products = Order_product_details::where('p_d_order_id',$P_d_order_id)->get();
        
        if(isset($old_products)){
            foreach($old_products as $row){
                
                $return_quntuty = $row->quantity;

                $qty_handling = Product_detail_inventory::where('product_detail_id',$row->product_detail_id)->where('inventory_id',$row->inventory_id)->first();
                if(!is_null($qty_handling)){
                    $qty_handling->quantity += $return_quntuty;
                    $qty_handling->save();
                }else{
                    $proInInv = Product_detail_inventory::where('product_detail_id',$row->product_detail_id)->first();
                    $proInInv->quantity += $return_quntuty;
                    $proInInv->save();
                }

                $row->delete();
            }
        }
        if(User::find($P_d_order->user_id) != null){
            $user = User::find($P_d_order->user_id);
            $user_debt = ($user->debt)-($P_d_order->rest);
            $user->debt = $user_debt;
            $user->save();
        }

        if(User::find($P_d_order->representative_id) != null){
            $rep = User::find($P_d_order->representative_id);
            $representative_debt = ($rep->debt)-($P_d_order->paid);
            $rep->debt = $representative_debt;
            $rep->save();
        }

        if($P_d_order->paid != 0 ){
            $safe_detail = new \stdClass();
            $safe_detail->safe_id = $P_d_order->safe_id;
            $safe_detail->process_name_ar = 'حذف طلب';
            $safe_detail->process_name_en = 'Order Deleting';
            $safe_detail->process_type = 2;
            $safe_detail->value = $P_d_order->paid;
            $safe_detail->notes = "حذف طلب رقم $P_d_order->id";
            SafeController::add_safe_process($safe_detail);
        }

        $P_d_order->delete();

        $msg=$lang=='ar' ? 'تم حذف الطلب بنجاح'  : 'P_d_order Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * Edit status
     */

    public function edit_status(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');

        $P_d_order=P_d_orders::find($P_d_order_id);
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }

        $P_d_order->status=$request->status;
        $P_d_order->save();

        $msg=$lang=='ar' ? 'تم تعديل الحالة بنجاح'  : 'status updated successfully';
        return $this->apiResponseData(new P_d_ordersResource($P_d_order),$msg);
    }


    /*
     * Paying client order
     */
    public function paying_client_order(Request $request , $P_d_order_id)
    {
        $lang=$request->header('lang');

        $P_d_order=P_d_orders::find($P_d_order_id);
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }

        if($P_d_order->client_order == true){

            $payValue = $request->pay_value ? $request->pay_value : $P_d_order->total_price;

            if($request->safe_id){
                $safe = Safe::find($request->safe_id);
                $check=$this->not_found($safe,'الطلب','P_d_order',$lang);
                if(isset($check)){
                    return $check;
                }
                $safeId = $request->safe_id;
            }else{
                $safeId = 1;
            }

            if($P_d_order->paid == 0){

                $P_d_order->paid = $payValue;
                $P_d_order->rest -= $payValue;

            }elseif($P_d_order->paid > 0){

                $paid_diff = $P_d_order->total_price - $P_d_order->paid;

                if($paid_diff >= $payValue){

                    $P_d_order->paid += $payValue;
                    $P_d_order->rest -= $payValue;

                }else{

                    $msg=$lang=='ar' ? 'القيمة المدخلة اكبر من المبلغ المتبقي بالطلب'  : 'Entered value is greater than the rest money in order';
                    return $this->apiResponseMessage(1,$msg,200);

                }
            }

            $P_d_order->safe_id=$safeId;
            $P_d_order->save();

            $safe_detail = new \stdClass();
            $safe_detail->safe_id = $safeId;
            $safe_detail->process_name_ar = 'مبيعات طلب عميل';
            $safe_detail->process_name_en = 'Sales client order';
            $safe_detail->process_type = 1;
            $safe_detail->value = $payValue;
            SafeController::add_safe_process($safe_detail);

            $msg=$lang=='ar' ? 'تم دفع الطلب بنجاح'  : 'Order paid successfully';
            return $this->apiResponseData(new P_d_ordersResource($P_d_order),$msg);

        }else{

            $msg=$lang=='ar' ? 'هذا الطلب ليس مطلوب من تطبيق العميل'  : 'This order is not ordered from client application';
            return $this->apiResponseMessage(1,$msg,200);
        
        }

    }


    /*
     * filrt
     */
    private function sentNotifcation($P_d_order)
    {
        $redirct_id=$P_d_order->id;
        $user=User::find($P_d_order->user_id);
        $token=$user->fire_base_token;
        $lang=$user->lang;
        if($P_d_order->status == 2)
        {
            $title=$lang=='ar' ? 'الطلب قيد التنفيذ'  : 'P_d_order in progress';
            $desc=$lang=='ar' ? 'وهو الان قيد التنفيذ '.$P_d_order->id.'تم قبول الطلب رقم '  : 'P_d_order in progress';
        }elseif ($P_d_order->status == 3)
        {
            $title=$lang=='ar' ? 'الطلب اكتمل'  : 'P_d_order  completed';
            $desc=$lang=='ar' ? $P_d_order->id.'تم اكتمال الطلب رقم '  : 'P_d_order completed';
        }
        NotificationMethods::senNotificationToSingleUser($token,$title,$desc,null,3,$redirct_id);
    }

    /*
     * Single P_d_order to show data
     */
    public function single_P_d_order(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');
        $P_d_order=P_d_orders::find($P_d_order_id);
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }
        return $this->apiResponseData(new P_d_ordersResource($P_d_order),'success');
    }


///////////////Make_Order//////////////////////////////////////////////////////////

public function make_p_d_order(Request $request)
{
    $shop=Auth::user();
    $lang=$request->header('lang');

    $order_req = $request->all();

    $validate_P_d_order=$this->validate_P_d_order($request);
    if(isset($validate_P_d_order)){
        return $validate_P_d_order;
    }

    $user = $order_req['user_id'];
    $user_d = User::find($user);
    $representative = $order_req['representative_id'];
    $representative_d = User::where('id' , '=' , $representative)->where('status' , '=' , '2')->first();

    $check=$this->not_found($representative_d,'المندوب','Representative',200);
    if(isset($check))
    {
        return $check;
    }

    $check=$this->not_found($user_d,'العضو','user',200);
    if(isset($check))
    {
        return $check;
    }

    $validate_address=$this->validate_address($request,$lang,$user);
    if(isset($validate_address))
    {
        return $validate_address;
    }

    $price_type_req=$order_req['price_type'];
    if($price_type_req == '1'){
        $price_type = 'wholesale_wholesale_price';
    }elseif($price_type_req == '2'){
        $price_type = 'wholesale_price';
    }elseif($price_type_req == '3'){
        $price_type = 'selling_price';
    }

    $P_d_order=new P_d_orders();
    $P_d_order->user_id=$user;
    $P_d_order->representative_id=$representative;
    $P_d_order->status=$order_req['status'];
    if(isset($order_req['shipping_price'])){
        $P_d_order->shipping_price=$order_req['shipping_price'];
    }else{
        $P_d_order->shipping_price = 0;
    }
    $P_d_order->payment_method=$order_req['payment_method'];
    if(isset($order_req['safe_id'])){
        $safe = Safe::find($order_req['safe_id']);
        $check=$this->not_found($user_d,'الخزنة','Safe',200);
        if(isset($check))
        {
            return $check;
        }
        $P_d_order->safe_id = $order_req['safe_id'];
    }else{
        $P_d_order->safe_id = 1;
    }
    $P_d_order->address_id=$order_req['address_id'];
    $P_d_order->price_type=$price_type;
    if(isset($order_req['notes'])){
        $P_d_order->notes = $order_req['notes'];
    }else{
        $P_d_order->notes = null;
    }
    $P_d_order->save();

    $p_d_order_id = $P_d_order->id;

    $this->addingProducts($order_req['orders'],$p_d_order_id,$lang);
    $this->makeBill($order_req,$p_d_order_id,$lang);

    $order_data = P_d_orders::find($p_d_order_id);

    $msg=$lang=='ar' ?  'تم تسجيل الطلب بنجاح' :'Order added successfully' ;
    return $this->apiResponseData(new P_d_ordersResource($order_data),$msg);
}

        
    private function addingProducts($products,$p_d_order_id,$lang)
    {

        foreach($products as $orders)
        {
            $P_d_order = P_d_orders::find($p_d_order_id);
            
            $product_detail = Product_detail_inventory::where('product_detail_id',$orders['product_detail_id'])->where('inventory_id',$orders['inventory_id'])->where('inventory_id','!=',0)->first();
            $check=$this->not_found($product_detail,' المنتج بالمخزن','Product_detail in inventory',$lang);
            if(isset($check)){
                $ch = $orders['product_detail_id'];
                $msg=$lang == 'ar' ?"المنتج رقم $ch غير موجود بالمخزن":"Product number $ch not found in inventory";
                return $this->apiResponseMessage(1,$msg,200);
            }

            if($orders['color_id']){
                $color=color::find($orders['color_id']);
                $check=$this->not_found($color,'اللون','Color',$lang);
                if(isset($check)){
                    $ch = $orders['color_id'];
                    $msg=$lang == 'ar' ?"اللون رقم $ch غير موجود":"Color number $ch not found";
                    return $this->apiResponseMessage(1,$msg,200);
                }
            }

            if($orders['size_id']){
                $size=Sizes::find($orders['size_id']);
                $check=$this->not_found($size,'الحجم','Size',$lang);
                if(isset($check)){
                    $ch = $orders['size_id'];
                    $msg=$lang == 'ar' ?"الحجم رقم $ch غير موجود":"Size number $ch not found";
                    return $this->apiResponseMessage(1,$msg,200);
                }
            }

            $P_d_order_product=Order_product_details::where('p_d_order_id',$p_d_order_id)->where('product_detail_id',$orders['product_detail_id'])->first();
            if(!is_null($P_d_order_product)){
                $msg=$lang == 'ar' ?'المنتج موجود في هذا الطلب' : 'Product already exist in this P_d_order';
                return $this->apiResponseMessage(0,$msg,200);
            }

            $qty = $product_detail->quantity;
            if ($orders['quantity'] > $qty){
                $msg=$lang == 'ar' ?'الكمية بالمخزن أقل من المطلوبة' : 'Quantity in inventory less than demanded';
                return $this->apiResponseMessage(0,$msg,200);
            }
            $n_qty = $qty-$orders['quantity'];
            
            $product_detail->quantity = $n_qty;
            $product_detail->save();

            $price_type = $P_d_order->price_type;
            $price_id = Product_details_price::where('product_detail_id',$orders['product_detail_id'])->value('price_id');
            $price = Price::where('id',$price_id)->value($price_type);

            $order_products = new Order_product_details;
            $order_products->p_d_order_id = $p_d_order_id;
            $order_products->product_detail_id = $orders['product_detail_id'];
            $order_products->inventory_id = $orders['inventory_id'];
            $order_products->quantity = $orders['quantity'];
            $order_products->price = $price;
            $order_products->color_id = $orders['color_id'];
            $order_products->size_id = $orders['size_id'];
            $order_products->save();

        }
    }

    private function makeBill($order_req,$p_d_order_id,$lang)
    {
        $order = P_d_orders::find($p_d_order_id);

        $shipp = $order->shipping_price;
        
        $pro_qtys = Order_product_details::select()->where('p_d_order_id',$p_d_order_id)->get();
        $price = 0;
        foreach ($pro_qtys as $row) {
            $pro_price = ($row->quantity)*($row->price);
            $price += $pro_price;
        }

        $discount_type = $order_req['discount_type'];
        $disV = $order_req['discount'];
        $tax1_type = $order_req['tax1_type'];
        $tax1V = $order_req['tax1'];
        $tax2_type = $order_req['tax2_type'];
        $tax2V = $order_req['tax2'];

        if ($discount_type == '2'){
            $dis = $price*($disV/100);
        }else{
            $dis = $disV;
        }

        if ($tax1_type == '2'){
            $tax1 = $price*($tax1V/100);
        }else{
            $tax1 = $tax1V;
        }

        if ($tax2_type == '2'){
            $tax2 = $price*($tax2V/100);
        }else{
            $tax2 = $tax2V;
        }

        $total = $price+$shipp+$tax1+$tax2-$dis;

        $paid = $order_req['paid'];
        if($paid > $total){
            $msg=$lang == 'ar' ?"($total) المدفوع اكبر من اجمالى الفاتورة" : "Cash is greater than bill cost ($total)";
            return $this->apiResponseMessage(0,$msg,200);
        }

        $rest = $total-$paid;

        //handle paid, rest, user_debt and representative_debt

        $order->pre_price = $price;
        $order->total_price = $total;
        $order->paid = $paid;
        $order->rest = $rest;
        $order->dis_type = $discount_type;
        $order->discount = $disV;
        $order->tax1_type = $tax1_type;
        $order->tax1 = $tax1V;
        $order->tax2_type = $tax2_type;
        $order->tax2 = $tax2V;
        $order->save();

        $user_d = User::find($order->user_id);
        $representative_d = User::find($order->representative_id);
        
        $user_new_debt = ($user_d->debt)+$rest;
        $representative_new_debt = ($representative_d->debt)+$paid;
        
        $user_d->debt =  $user_new_debt;
        $user_d->save();

        $representative_d->debt = $representative_new_debt;
        $representative_d->save();

        $safe_detail = new \stdClass();
        if(isset($order_req['safe_id'])){
            $safe = Safe::find($order_req['safe_id']);
            $check=$this->not_found($user_d,'الخزنة','Safe',200);
            if(isset($check))
            {
                return $check;
            }
            $safe_detail->safe_id = $order_req['safe_id'];
        }else{
            $safe_detail->safe_id = 1;
        }
        $safe_detail->process_name_ar = 'مبيعات';
        $safe_detail->process_name_en = 'Sales';
        $safe_detail->process_type = 1;
        $safe_detail->value = $paid;
        SafeController::add_safe_process($safe_detail);

    }
        

    /*
     * Add new P_d_order
     * add general P_d_order column , add products to this P_d_order
     */

    public function add_P_d_order(Request $request)
    {
        $shop=Auth::user();
        $lang=$request->header('lang');

        $validate_P_d_order=$this->validate_P_d_order($request);
        if(isset($validate_P_d_order)){
            return $validate_P_d_order;
        }

        $user=$request->user_id;
        $user_d = User::find($user);
        $representative = $request->representative_id;
        $representative_d = User::where('id' , '=' , $representative)->where('status' , '=' , '2')->first();

        $check=$this->not_found($representative_d,'المندوب','Representative',200);
        if(isset($check))
        {
            return $check;
        }

        $check=$this->not_found($user_d,'العضو','user',200);
        if(isset($check))
        {
            return $check;
        }

        $validate_address=$this->validate_address($request,$lang,$request->user_id);
        if(isset($validate_address))
        {
            return $validate_address;
        }

        $price_type_req=$request->price_type;
        if($price_type_req == '1'){
            $price_type = 'wholesale_wholesale_price';
        }elseif($price_type_req == '2'){
            $price_type = 'wholesale_price';
        }elseif($price_type_req == '3'){
            $price_type = 'selling_price';
        }

        $P_d_order=new P_d_orders();
        $P_d_order->user_id=$user;
        $P_d_order->representative_id=$representative;
        $P_d_order->status=$request->status;
        $P_d_order->shipping_price=$shop->shipping_price;
        $P_d_order->payment_method=$request->payment_method;
        $P_d_order->address_id=$request->address_id;
        $P_d_order->price_type=$price_type;
        $P_d_order->save();
        $msg=$lang=='ar' ?  'تم تسجيل الطلب بنجاح' :'P_d_order saved successfully' ;
        return $this->apiResponseData(new P_d_ordersResource($P_d_order),$msg);
    }

    /*
     * custom function to validate Address To P_d_orders
     */

    private function validate_address($request,$lang,$user_id)
    {

        if(!$request->address_id)
        {
            $msg=$lang=='ar' ? 'من فضلك ادخل العنوان'  : 'please send location';
            return $this->apiResponseMessage(0,$msg,200);
        }
        $P_d_order_location=order_locations::find($request->address_id);
        $check=$this->not_found($P_d_order_location,'العنوان','Adress',$lang);
        if(isset($check))
        {
            return $check;
        }

        if($user_id != $P_d_order_location->user_id)
        {
            $msg=$lang=='ar' ? 'العنوان لا يخص العضو'  : 'location not belongs to this user';
            return $this->apiResponseMessage(0,$msg,200);
        }

    }


    /*
     * @pram P_d_order column ,
     * @return success or validate
     */

    public function edit_P_d_order(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');

        $P_d_order = P_d_orders::where('id',$P_d_order_id)->whereIn('status',[1,2])->first();
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            $msg=$lang == 'ar' ?'الطلب غير موجود او تم شحنه او تسليمه' : 'Order not found or shipped or delivered';
                return $this->apiResponseMessage(0,$msg,200);
        }

        $user=$request->user_id;
        $user_d = User::find($user);
        $representative = $request->representative_id;
        $representative_d = User::where('id' , '=' , $representative)->where('status' , '=' , '2')->first();

        $check=$this->not_found($representative_d,'المندوب','Representative',200);
        if(isset($check))
        {
            return $check;
        }

        $check=$this->not_found($user_d,'العضو','user',200);
        if(isset($check))
        {
            return $check;
        }

        $price_type_req=$request->price_type;
        if($price_type_req == '1'){
            $price_type = 'wholesale_wholesale_price';
        }elseif($price_type_req == '2'){
            $price_type = 'wholesale_price';
        }elseif($price_type_req == '3'){
            $price_type = 'selling_price';
        }

        $P_d_order->user_id=$user;
        $P_d_order->representative_id=$representative;
        $P_d_order->name=$request->name;
        $P_d_order->status=$request->status;
        $P_d_order->email=$request->email;
        $P_d_order->phone=$request->phone;
        $P_d_order->address_id=$request->address_id;
        $P_d_order->payment_method=$request->payment_method;
        $P_d_order->shipping_price=$request->shipping_price;
        $P_d_order->price_type=$price_type;
        $P_d_order->total_price=$request->total_price;
        $P_d_order->save();
        $msg=$lang=='ar' ? 'تم تعديل الطلب بنجاح'  : 'P_d_order Edited successfully';
        return $this->apiResponseData(new P_d_ordersResource($P_d_order),'success');
    }

    /*
     * Add Product To P_d_order
     */

    public function add_product_to_P_d_order(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');
        $P_d_order=P_d_orders::find($P_d_order_id);
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }

        $order_req = $request->all();
        foreach($order_req['orders'] as $orders)
        {
            $product_detail = Product_detail_inventory::where('product_detail_id',$orders['product_detail_id'])->where('inventory_id',$orders['inventory_id'])->first();
            $check=$this->not_found($product_detail,' المنتج بالمخزن','Product_detail in inventory',$lang);
            if(isset($check)){
                $ch = $orders['product_detail_id'];
                $msg=$lang == 'ar' ?"المنتج رقم $ch غير موجود بالمخزن":"Product number $ch not found in inventory";
                return $this->apiResponseMessage(1,$msg,200);
            }

            if($orders['color_id']){
                $color=color::find($orders['color_id']);
                $check=$this->not_found($color,'اللون','Color',$lang);
                if(isset($check)){
                    $ch = $orders['color_id'];
                    $msg=$lang == 'ar' ?"اللون رقم $ch غير موجود":"Color number $ch not found";
                    return $this->apiResponseMessage(1,$msg,200);
                }
            }

            if($orders['size_id']){
                $size=Sizes::find($orders['size_id']);
                $check=$this->not_found($size,'الحجم','Size',$lang);
                if(isset($check)){
                    $ch = $orders['size_id'];
                    $msg=$lang == 'ar' ?"الحجم رقم $ch غير موجود":"Size number $ch not found";
                    return $this->apiResponseMessage(1,$msg,200);
                }
            }

            $P_d_order_product=Order_product_details::where('P_d_order_id',$P_d_order_id)->where('product_detail_id',$orders['product_detail_id'])->first();
            if(!is_null($P_d_order_product)){
                $msg=$lang == 'ar' ?'المنتج موجود في هذا الطلب' : 'Product already exist in this P_d_order';
                return $this->apiResponseMessage(0,$msg,200);
            }

            // $product_detail = Product_detail_inventory::where('product_detail_id',$orders['product_detail_id'])->where('inventory_id',$orders['inventory_id'])->first();
            $qty = $product_detail->quantity;
            if ($orders['quantity'] >= $qty){
                $msg=$lang == 'ar' ?'الكمية بالمخزن أقل من المطلوبة' : 'Quantity in inventory less than demanded';
                return $this->apiResponseMessage(0,$msg,200);
            }
            $n_qty = $qty-$orders['quantity'];
            // return $n_qty;
            
            $product_detail->quantity = $n_qty;
            $product_detail->save();

            $price_type = $P_d_order->price_type;
            $price_id = Product_details_price::where('product_detail_id',$orders['product_detail_id'])->value('price_id');
            $price = Price::where('id',$price_id)->value($price_type);

            $order_products = new Order_product_details;
            $order_products->P_d_order_id = $P_d_order_id;
            $order_products->product_detail_id = $orders['product_detail_id'];
            $order_products->inventory_id = $orders['inventory_id'];
            $order_products->quantity = $orders['quantity'];
            $order_products->price = $price;
            $order_products->color_id = $orders['color_id'];
            $order_products->size_id = $orders['size_id'];
            $order_products->save();

        }

        $msg=$lang == 'ar' ?'تم اضافة المنتجات للطلب بنجاح' : 'Products added to order successfully';
        return $this->apiResponseMessage(0,$msg,200);

    }

    public function make_bill(Request $request, $p_d_order_id){
        $lang=$request->header('lang');
        $p_d_order=Order_product_details::where('p_d_order_id',$p_d_order_id)->first();
        $check=$this->not_found($p_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }

        $pro_qtys = Order_product_details::select()->where('p_d_order_id',$p_d_order_id)->get();
        $price = 0;
        foreach ($pro_qtys as $row) {
            $pro_price = ($row->quantity)*($row->price);
            $price += $pro_price;
        }

        $discount_type = $request->discount_type;
        $disV = $request->discount;
        $tax1_type = $request->tax1_type;
        $tax1V = $request->tax1;
        $tax2_type = $request->tax2_type;
        $tax2V = $request->tax2;

        if ($discount_type == '2'){
            $dis = $price*($disV/100);
        }else{
            $dis = $disV;
        }

        if ($tax1_type == '2'){
            $tax1 = $price*($tax1V/100);
        }else{
            $tax1 = $tax1V;
        }

        if ($tax2_type == '2'){
            $tax2 = $price*($tax2V/100);
        }else{
            $tax2 = $tax2V;
        }

        $total = $price+$tax1+$tax2-$dis;

        $paid = $request->paid;
        if($paid > $total){
            $msg=$lang == 'ar' ?"($total) المدفوع اكبر من اجمالى الفاتورة" : "Cash is greater than bill cost ($total)";
            return $this->apiResponseMessage(0,$msg,200);
        }

        $rest = $total-$paid;

        $order = P_d_orders::find($p_d_order_id);

        //handle paid, rest, user_debt and representative_debt

        $order->total_price = $total;
        $order->paid = $paid;
        $order->rest = $rest;
        $order->dis_type = $discount_type;
        $order->discount = $disV;
        $order->tax1_type = $tax1_type;
        $order->tax1 = $tax1V;
        $order->tax2_type = $tax2_type;
        $order->tax2 = $tax2V;
        $order->save();

        $user = User::find($order->user_id);
        $representative = User::find($order->representative_id);

        $user_new_debt = ($user->debt)+$rest;
        $representative_new_debt = ($representative->debt)+$paid;
        
        $user->debt =  $user_new_debt;
        $user->save();

        $representative->debt = $representative_new_debt;
        $representative->save();

        $msg=$lang=='ar' ? 'تم إضافة الفاتورة بنجاح'  : 'Bill added successfully';
        return $this->apiResponseData(new P_d_ordersResource($order),$msg);

    }

    public function order_user_addresses (Request $request , $user_id){
        $lang=$request->header('lang');
        $user=User::where('id',$user_id)->first();
        $msg=$lang=='ar' ? 'تمت العملية بنجاح'  : 'success';
        return $this->apiResponseData(locationResource::collection($user->my_address),$msg);
    }

    public function add_user_address(Request $request , $user_id)
    {
        $lang=$request->header('lang');
        $address=new order_locations();
        $address->lat=$request->lat;
        $address->lng=$request->lng;
        $address->address=$request->address;
        $address->user_id=$user_id;
        $address->save();
        $msg=$lang=='ar' ? 'تم تسجيل العنوان بنجاح' : 'location saved successfully';
        return $this->apiResponseData(new locationResource($address),$msg,200);
    }



    public function all_p_d_Order_with_products (Request $request){
        $P_d_orders=Order_product_details::orderBy('created_at','desc')->get();
        return $this->apiResponseData(Order_product_detailsResource::collection($P_d_orders),'success');
    }

    public function single_P_d_order_with_products(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');
        $P_d_order=Order_product_details::where('P_d_order_id', '=' ,$P_d_order_id)->first();
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }
        $query = Order_product_details::select()->where('P_d_order_id', $P_d_order_id)->get();
        return $this->apiResponseData(Order_product_detailsResource::collection($query),'success');
    }

    public function bill_show(Request $request,$P_d_order_id)
    {
        $lang=$request->header('lang');
        $P_d_order=P_d_orders::where('id', '=' ,$P_d_order_id)->first();
        $check=$this->not_found($P_d_order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }
        // $query = Order_product_details::select()->where('P_d_order_id', $P_d_order_id)->get();
        return $this->apiResponseData(new BillResource($P_d_order),'success');
    }
    


    public function representative_orders(Request $request,$representative_id)
    {
        $lang=$request->header('lang');
        $P_d_order=P_d_orders::where('representative_id', '=' ,$representative_id)->orderBy('created_at' , 'DESC')->get();
    
        return $this->apiResponseData(P_d_ordersResource::collection($P_d_order),'success');
    }

    public function user_orders(Request $request,$user_id)
    {
        $lang=$request->header('lang');
        $P_d_order=P_d_orders::where('user_id', '=' ,$user_id)->orderBy('created_at' , 'DESC')->get();
    
        return $this->apiResponseData(P_d_ordersResource::collection($P_d_order),'success');
    }

    /*
     * @pram $request
     * @return Error message or check if user is null
    */

    private function validate_P_d_order($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'user_id.required' => $lang == 'ar' ?  'من فضلك ادخل رقم العميل' :"client is required" ,
            'address_id.required' => $lang == 'ar' ? 'من فضلك ادخل العنوان ' :"address is required",
            'representative_id.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الندوب' :"representative is required" ,
        ];

        $validator = Validator::make($input, [
            'user_id' => 'required',
            'address_id' => 'required',
            'representative_id' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }

        $user=User::find($request->user_id);
        $check=$this->not_found($user,'العميل','Client',$lang);
        if(isset($check)){
            return $check;
        }
    }

    public function order_return(Request $request , $order_id)
    {
        
        $lang=$request->header('lang');

        $order = P_d_orders::find($order_id);
        $check=$this->not_found($order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            return $check;
        }

        $order_pieces = Order_product_details::select('quantity')->where('p_d_order_id',$order_id)->get();
        $qty_suuum = 0;
        foreach ($order_pieces as $row){
            $qty_suuum += $row->quantity;
        }

        $total_price = $order->total_price;
        $pre_price = $order->pre_price;
        $old_rest = $order->rest;
        $old_paid = $order->paid;

        $diff = $total_price - $pre_price;
        $suuum = 0;
        $pre_sum = 0;
        
        $order_req = $request->all();
        foreach($order_req['orders'] as $orders)
        {
            $pro_id = $orders['product_detail_id'];

            $product_detail=Order_product_details::where('product_detail_id',$orders['product_detail_id'] )->where('p_d_order_id', $order_id)->first();
            $check=$this->not_found($product_detail,'المنتج','Product_detail',$lang);
            if(isset($check)){
                $ch = $orders['product_detail_id'];
                $msg=$lang == 'ar' ?"المنتج رقم $ch غير موجود":"Product number $ch not found";
                return $this->apiResponseMessage(1,$msg,200);
            }
            
            $return_quntuty = $orders['ret_quantity'];
            $old_quantity = $product_detail->quantity;
            if($return_quntuty > $old_quantity){
                $msg=$lang == 'ar' ?"الكمية بالمنتج $pro_id أقل من المطلوبة" : "Quantity in product $pro_id less than demanded";
                return $this->apiResponseMessage(0,$msg,200);
            }

            $price = $product_detail->price;

            $diff_percentage = $price*$diff/$pre_price;

            $retsum = ($price + $diff_percentage) * $return_quntuty;

            $new_quntity = $old_quantity - $return_quntuty;
            
            if($new_quntity == 0){
                $product_detail->delete();
            }else{
                $product_detail->quantity=$new_quntity;
                $product_detail->save();
            }

            $return_order=new Order_return;
            $return_order->ret_quantity= $return_quntuty;
            $return_order->ret_sum= $retsum;
            $return_order->representative_id=$order->representative_id;
            $return_order->user_id=$order->user_id;
            $return_order->product_detail_id = $pro_id;
            $return_order->p_d_order_id=$order_id;
            $return_order->save();


            if(Inventory::find($orders['inventory_id']) != null){
                $qty_handling = Product_detail_inventory::where('product_detail_id',$pro_id)->where('inventory_id',$orders['inventory_id'])->first();
                if(!$qty_handling){
                    $Product_detail_inventory=new Product_detail_inventory();
                    $Product_detail_inventory->inventory_id = $orders['inventory_id'];
                    $Product_detail_inventory->product_detail_id = $pro_id;
                    $Product_detail_inventory->quantity = $return_quntuty;
                    $Product_detail_inventory->save();
                }
                $qty_handling->quantity += $return_quntuty;
                $qty_handling->save();
            }else{
                $qty_handling = Product_detail_inventory::where('product_detail_id',$pro_id)->where('inventory_id',$product_detail->inventory_id)->first();
                $qty_handling->quantity += $return_quntuty;
                $qty_handling->save();
            }

            $suuum += $return_order->ret_sum;

            $soft_sum = $price * $return_quntuty;
            $pre_sum += $soft_sum;

            $retData[] = $return_order;
        }
        
        $new_total_sum = $order->total_price - $suuum;

        if($suuum <= $old_rest){
            $new_rest = $old_rest - $suuum;
            $ret_cash = 0;
        }else{
            $new_rest = 0;
            $ret_cash = $suuum - $old_rest;
        }

        $order->total_price = $new_total_sum;
        $order->pre_price = $pre_price - $pre_sum;
        $order->rest = $new_rest;
        $order->paid = $old_paid-$ret_cash;
        $order->save();

        $user = User::find($order->user_id);
        $userDebt = $user->debt;
        $user->debt = $userDebt - $old_rest + $new_rest;
        $user->save();

        if($ret_cash != 0){
            $safe_detail = new \stdClass();
            if(isset($order_req['safe_id'])){
                $safe = Safe::find($order_req['safe_id']);
                $check=$this->not_found($safe,'الخزنة','Safe',200);
                if(isset($check))
                {
                    return $check;
                }
                $safe_detail->safe_id = $order_req['safe_id'];
            }else{
                $safe_detail->safe_id = $order->safe_id;
            }
            $safe_detail->process_name_ar = 'مرتجعات';
            $safe_detail->process_name_en = 'Returns';
            $safe_detail->process_type = 2;
            $safe_detail->value = $ret_cash;
            $safe_detail->notes = "مرتجع طلب رقم $order_id";
            SafeController::add_safe_process($safe_detail);
        }

        $data = ['Order total price' => $order->total_price , 'Return Cost' => (double)$suuum ,'Return data' => $retData , 'User debt' => $user->debt];

        return  $this->apiResponseData($data,'success');


    }

    public function most_sell()
    {
        $most_sell = DB::table('order_product_details')->select(DB::raw('COUNT(product_detail_id) as count'),'product_detail_id')
        ->groupBy('product_detail_id')
        ->orderBy('count','desc')
        ->take(5)
        ->get(); 

        $mosts_ids = $most_sell->pluck('product_detail_id');

        $products = Product_details::whereIn('id', $mosts_ids)->get();
        
        return  $this->apiResponseData(MostsellResource::collection($products),'success');

    }


    public function product_inventories(Request $request){
        // $products = Product_detail_inventory::paginate(10);
        // return ProductInventoriesResource::collection($products)->response()->getData(true);
        $products = Product_detail_inventory::where('quantity','!=',0)->where('inventory_id','!=',0)->orderBy('created_at' , 'DESC')->get();
        return  $this->apiResponseData(ProductInventoriesResource::collection($products),'success');
    }

    public function searchBy_nameORbarcode(Request $request)
    {
        $lang=$request->header('lang');
        
        if (preg_match('/[0-9]/', $request->name_barcode)){
            $products_ids=Product_details::where('barcode','like','%' .$request->name_barcode. '%')->pluck('id');
        }else{
            $mainProName_IDs = Products::Where('name_en','like','%' .$request->name_barcode. '%')->orWhere('name_ar','like','%' .$request->name_barcode. '%')->pluck('id');
            $mainProNameProDetIds = Product_details::where('product_id' , $mainProName_IDs)->pluck('id');
            $proDetNameIds = Product_details::Where('difference_ar','like','%' .$request->name_barcode. '%')->orWhere('difference_en','like','%' .$request->name_barcode. '%')->pluck('id');
            $products_ids = Product_details::whereIn('id',$proDetNameIds)->orWhereIn('id',$mainProNameProDetIds)->pluck('id');
            // $products=products::where('name_ar','like','%' .$request->name_barcode. '%')->pluck('id');
            // $products_ids = Product_details::whereIn('product_id' , $products)->where('status',1)->pluck('id');
            // }else{
            //     $mainProName_IDs = Products::Where('name_en','like','%' .$name. '%')->orWhere('name_ar','like','%' .$name. '%')->pluck('id');
            //     $mainProNameProDetIds = Product_details::where('product_id' , $mainProName_IDs)->pluck('id');
            //     $proDetNameIds = Product_details::Where('difference_ar','like','%' .$name. '%')->orWhere('difference_en','like','%' .$name. '%')->pluck('id');
            //     $nameIds = Product_details::whereIn('id',$proDetNameIds)->orWhereIn('id',$mainProNameProDetIds)->pluck('id');
            //     // $products=products::where('name_en','like','%' .$request->name_barcode. '%')->pluck('id');
            //     // $products_ids = Product_details::whereIn('product_id' , $products)->where('status',1)->pluck('id');
        }
        
        $product_details = Product_detail_inventory::whereIn('product_detail_id' , $products_ids)->where('inventory_id','!=',0)->where('quantity','!=',0)->orderBy('product_detail_id' , 'DESC')->get();

        return  $this->apiResponseData(ProductInventoriesResource::collection($product_details),'success');
    }




    public function edit_order_product(Request $request,$order_id)
    {
        $shop=Auth::user();
        $lang=$request->header('lang');

        $product_id = $request->product_detail_id;
        $qty = $request->quantity;

        $order = P_d_orders::where('id',$order_id)->whereIn('status',[1,2,3,4,5,6])->first();
        $check=$this->not_found($order,'الطلب','P_d_order',$lang);
        if(isset($check)){
            $msg=$lang == 'ar' ?'الطلب غير موجود او تم شحنه او تسليمه' : 'Order not found or shipped or delivered';
                return $this->apiResponseMessage(0,$msg,200);
        }

        // return new P_d_ordersResource($order);

        $old_rest = $order->rest;
        $old_paid = $order->paid;

        if($request->type == 'd'){
            Order_product_details::where('p_d_order_id',$order_id)->where('product_detail_id',$product_id)->delete();
        }elseif($request->type == 'e'){
            $product = Order_product_details::where('p_d_order_id',$order_id)->where('product_detail_id',$product_id)->first();
            $check=$this->not_found($product,'المنتج','product',$lang);
            if(isset($check)){
                $msg=$lang == 'ar' ?'المنتج غير موجود في الطلب' : 'product not found in order';
                    return $this->apiResponseMessage(0,$msg,200);
            }

            if($product->inventory_id == null){
                $msg=$lang == 'ar' ?'المخزن غير موجود' : 'inventorty not existed';
                return $this->apiResponseMessage(0,$msg,200);
            }

            $old_quantityyy = $product->quantity;

            $product_detail = Product_detail_inventory::where('product_detail_id',$product_id)->where('inventory_id',$product->inventory_id)->first();
            $check=$this->not_found($product,'المنتج','product',$lang);
            if(isset($check)){
                $msg=$lang == 'ar' ?'المنتج غير موجود في المخزن' : 'product not found in inventorty';
                return $this->apiResponseMessage(0,$msg,200); 
            }
            $pro_inv_newQty = $product_detail->quantity + $old_quantityyy;

            $product_detail->quantity = $pro_inv_newQty;
            $product_detail->save();

            if ($qty >= $pro_inv_newQty){
                $msg=$lang == 'ar' ?'الكمية بالمخزن أقل من المطلوبة' : 'Quantity in inventory less than demanded';
                return $this->apiResponseMessage(0,$msg,200);
            }
            
            $product->quantity = $qty;
            $product->save();

            $pro_inv_new = Product_detail_inventory::where('product_detail_id',$product_id)->where('inventory_id',$product->inventory_id)->first();
            $pro_inv_new->quantity -= $qty;
            $pro_inv_new->save();
        }

            $shipp = $order->shipping_price;
        
            $pro_qtys = Order_product_details::where('p_d_order_id',$order_id)->get();
            $price = 0;
            foreach ($pro_qtys as $row) {
                $pro_price = ($row->quantity)*($row->price);
                $price += $pro_price;
            }

            $discount_type = $order->discount_type;
            $disV = $order->discount;
            $tax1_type = $order->tax1_type;
            $tax1V = $order->tax1;
            $tax2_type = $order->tax2_type;
            $tax2V = $order->tax2;

            if ($discount_type == '2'){
                $dis = $price*($disV/100);
            }else{
                $dis = $disV;
            }

            if ($tax1_type == '2'){
                $tax1 = $price*($tax1V/100);
            }else{
                $tax1 = $tax1V;
            }

            if ($tax2_type == '2'){
                $tax2 = $price*($tax2V/100);
            }else{
                $tax2 = $tax2V;
            }

            $total = $price+$shipp+$tax1+$tax2-$dis;
            // return $shipp;

            $paid = $order->paid;

            $rest = $total-$paid;

            //handle paid, rest, user_debt and representative_debt

            $order->pre_price = $price;
            $order->total_price = $total;
            $order->paid = $paid;
            $order->rest = $rest;
            $order->dis_type = $discount_type;
            $order->discount = $disV;
            $order->tax1_type = $tax1_type;
            $order->tax1 = $tax1V;
            $order->tax2_type = $tax2_type;
            $order->tax2 = $tax2V;
            $order->save();

            if(User::find($order->user_id) != null){
                $user_d = User::find($order->user_id);
                $user_new_debt = ($user_d->debt)-$old_rest+$rest;
                $user_d->debt =  $user_new_debt;
                $user_d->save();
            }

            if(User::find($order->representative_id) != null){
                $representative_d = User::find($order->representative_id);
                $representative_new_debt = ($representative_d->debt)-$old_paid+$paid;
                $representative_d->debt = $representative_new_debt;
                $representative_d->save();
            }
        

        $order_data = P_d_orders::find($order_id);

        $msg=$lang=='ar' ?  'تمت العملية بنجاح' :'process done successfully' ;
        return $this->apiResponseData(new P_d_ordersResource($order_data),$msg);
    }

        
    // private function addingProducts($products,$p_d_order_id,$lang)
    // {

    //     foreach($products as $orders)
    //     {
    //         $P_d_order = P_d_orders::find($p_d_order_id);
            
    //         $product_detail = Product_detail_inventory::where('product_detail_id',$orders['product_detail_id'])->where('inventory_id',$orders['inventory_id'])->first();
    //         $check=$this->not_found($product_detail,' المنتج بالمخزن','Product_detail in inventory',$lang);
    //         if(isset($check)){
    //             $ch = $orders['product_detail_id'];
    //             $msg=$lang == 'ar' ?"المنتج رقم $ch غير موجود بالمخزن":"Product number $ch not found in inventory";
    //             return $this->apiResponseMessage(1,$msg,200);
    //         }

    //         if($orders['color_id']){
    //             $color=color::find($orders['color_id']);
    //             $check=$this->not_found($color,'اللون','Color',$lang);
    //             if(isset($check)){
    //                 $ch = $orders['color_id'];
    //                 $msg=$lang == 'ar' ?"اللون رقم $ch غير موجود":"Color number $ch not found";
    //                 return $this->apiResponseMessage(1,$msg,200);
    //             }
    //         }

    //         if($orders['size_id']){
    //             $size=Sizes::find($orders['size_id']);
    //             $check=$this->not_found($size,'الحجم','Size',$lang);
    //             if(isset($check)){
    //                 $ch = $orders['size_id'];
    //                 $msg=$lang == 'ar' ?"الحجم رقم $ch غير موجود":"Size number $ch not found";
    //                 return $this->apiResponseMessage(1,$msg,200);
    //             }
    //         }

    //         $P_d_order_product=Order_product_details::where('p_d_order_id',$p_d_order_id)->where('product_detail_id',$orders['product_detail_id'])->first();
    //         if(!is_null($P_d_order_product)){
    //             $msg=$lang == 'ar' ?'المنتج موجود في هذا الطلب' : 'Product already exist in this P_d_order';
    //             return $this->apiResponseMessage(0,$msg,200);
    //         }

    //         $qty = $product_detail->quantity;
    //         if ($orders['quantity'] > $qty){
    //             $msg=$lang == 'ar' ?'الكمية بالمخزن أقل من المطلوبة' : 'Quantity in inventory less than demanded';
    //             return $this->apiResponseMessage(0,$msg,200);
    //         }
    //         $n_qty = $qty-$orders['quantity'];
            
    //         $product_detail->quantity = $n_qty;
    //         $product_detail->save();

    //         $price_type = $P_d_order->price_type;
    //         $price_id = Product_details_price::where('product_detail_id',$orders['product_detail_id'])->value('price_id');
    //         $price = Price::where('id',$price_id)->value($price_type);

    //         $order_products = new Order_product_details;
    //         $order_products->p_d_order_id = $p_d_order_id;
    //         $order_products->product_detail_id = $orders['product_detail_id'];
    //         $order_products->inventory_id = $orders['inventory_id'];
    //         $order_products->quantity = $orders['quantity'];
    //         $order_products->price = $price;
    //         $order_products->color_id = $orders['color_id'];
    //         $order_products->size_id = $orders['size_id'];
    //         $order_products->save();

    //     }
    // }

    // private function makeBill($order_req,$p_d_order_id,$lang)
    // {
    //     $order = P_d_orders::find($p_d_order_id);

    //     $shipp = $order->shipping_price;
        
    //     $pro_qtys = Order_product_details::select()->where('p_d_order_id',$p_d_order_id)->get();
    //     $price = 0;
    //     foreach ($pro_qtys as $row) {
    //         $pro_price = ($row->quantity)*($row->price);
    //         $price += $pro_price;
    //     }

    //     $discount_type = $order_req['discount_type'];
    //     $disV = $order_req['discount'];
    //     $tax1_type = $order_req['tax1_type'];
    //     $tax1V = $order_req['tax1'];
    //     $tax2_type = $order_req['tax2_type'];
    //     $tax2V = $order_req['tax2'];

    //     if ($discount_type == '2'){
    //         $dis = $price*($disV/100);
    //     }else{
    //         $dis = $disV;
    //     }

    //     if ($tax1_type == '2'){
    //         $tax1 = $price*($tax1V/100);
    //     }else{
    //         $tax1 = $tax1V;
    //     }

    //     if ($tax2_type == '2'){
    //         $tax2 = $price*($tax2V/100);
    //     }else{
    //         $tax2 = $tax2V;
    //     }

    //     $total = $price+$shipp+$tax1+$tax2-$dis;

    //     $paid = $order_req['paid'];
    //     if($paid > $total){
    //         $msg=$lang == 'ar' ?"($total) المدفوع اكبر من اجمالى الفاتورة" : "Cash is greater than bill cost ($total)";
    //         return $this->apiResponseMessage(0,$msg,200);
    //     }

    //     $rest = $total-$paid;

    //     //handle paid, rest, user_debt and representative_debt

    //     $order->pre_price = $price;
    //     $order->total_price = $total;
    //     $order->paid = $paid;
    //     $order->rest = $rest;
    //     $order->dis_type = $discount_type;
    //     $order->discount = $disV;
    //     $order->tax1_type = $tax1_type;
    //     $order->tax1 = $tax1V;
    //     $order->tax2_type = $tax2_type;
    //     $order->tax2 = $tax2V;
    //     $order->save();

    //     $user_d = User::find($order->user_id);
    //     $representative_d = User::find($order->representative_id);
        
    //     $user_new_debt = ($user_d->debt)+$rest;
    //     $representative_new_debt = ($representative_d->debt)+$paid;
        
    //     $user_d->debt =  $user_new_debt;
    //     $user_d->save();

    //     $representative_d->debt = $representative_new_debt;
    //     $representative_d->save();

    // }
}
