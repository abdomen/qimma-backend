<?php

namespace App\Http\Controllers\Shop_Api;

use App\Models\Groups;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\GroupsResource;

class GroupsController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new group to database
     */
    public function add_group(Request $request)
    {
        $user = Auth::user();
        $validate_group=$this->validate_group($request);
        if(isset($validate_group)){
            return $validate_group;
        }
        $group=new groups();
        $group->name_ar=$request->name_ar;
        $group->name_en=$request->name_en;
        $group->desc_ar=$request->desc_ar;
        $group->desc_en=$request->desc_en;
        if($request->image){
            $name=BaseController::saveImage('Group',$request->file('image'));
            $group->image=$name;
        }
        $group->mainShop_id = $user->id;
        $group->save();
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم اضافة المجموعة بنجاح'  : 'group added successfully';
        return $this->apiResponseData(new GroupsResource($group),$msg);

    }

    /*
     * Edit product
    */
    public function edit_group(Request $request,$group_id)
    {
        $lang=$request->header('lang');
        $group=Groups::find($group_id);
        $check=$this->not_found($group,'المجموعة','group',$lang);
        if(isset($check)){
            return $check;
        }
        $validate_group=$this->validate_group($request);
        if(isset($validate_group)){
            return $validate_group;
        }

        $group->name_ar=$request->name_ar;
        $group->name_en=$request->name_en;
        $group->desc_ar=$request->desc_ar;
        $group->desc_en=$request->desc_en;
        if($request->image){
            BaseController::deleteFile('Group',$group->image);
            $name=BaseController::saveImage('Group',$request->file('image'));
            $group->image=$name;
        }
        $group->save();
        $lang = $request->header('lang');
        $msg=$lang=='ar' ? 'تم تعديل المجموعة بنجاح'  : 'Group edited successfully';
        return $this->apiResponseData(new groupsResource($group),$msg);
    }


    /*
     * get All product for Auth shop
     */
    public function all_groups(Request $request)
    {
        $user=Auth::user();
        $groups=Groups::orderBy('id','desc')->get();
        return $this->apiResponseData(groupsResource::collection($groups),'success');
    }


    /*
     * Show single group
     */
    public function single_group(Request $request,$group_id){
        $lang=$request->header('lang');
        $group=Groups::find($group_id);
        $check=$this->not_found($group,'المجموعة','group',$lang);
        if(isset($check)){
            return $check;
        }
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData($group,$msg);
    }

    /*
     * Delete Group ..
     */

    public function delete_group(Request $request,$group_id){
        $lang=$request->header('lang');
        $group=Groups::where('id',$group_id)->where('id','!=',1)->first();
        $check=$this->not_found($group,'المجموعة','Group',$lang);
        if(isset($check)){
            return $check;
        }
        
        BaseController::deleteFile('Group',$group->image);
        $group->delete();
        $msg=$lang=='ar' ? 'تم حذف المجموعة بنجاح'  : 'group Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }


    /*
     * @pram $request
     * @return Error message or check if cateogry is null
     */

    private function validate_group($request){
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'name_ar.required' => $lang == 'ar' ?  'من فضلك ادخل اسم المجموعة بالعربية' :"name in arabic is required" ,
            'name_en.required' => $lang == 'ar' ? 'من فضلك ادخل اسم المجموعة بالانجليزية' :"name in english is required"  ,
            'image.required' => $lang == 'ar' ? 'من فضلك ادخل صورة المجموعة ' :"image is required"  ,
        ];
        $validator = Validator::make($input, [
            'name_ar' => 'required',
            'name_en' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 400);
        }
        
    }
}
