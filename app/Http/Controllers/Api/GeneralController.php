<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CurrencyResource;
use App\Models\About;
use App\Models\Currency;
use App\Models\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt;
use App\Http\Resources\ContactUsResource;
use App\Http\Resources\AboutResource;
use App\Models\Contact_us;
use App\Http\Controllers\Manage\BaseController;
use App\Models\Shop;
class GeneralController extends Controller
{
    use ApiResponseTrait;

    /*
     * @pram request array
     * @return  response()->json
     */

    public function contact_us(Request $request)
    {
        $lang = $request->header('lang');
        $input = $request->all();

        $validationMessages = [
            'message.required' => $lang == 'ar' ?  'من فضلك ادخل الرسالة' :"message is required" ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضلك ادخل رقم الهاتف' :"phone is required"  ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  ' الهاتف يجب ان يكون رقما' :"The phone must be a number" ,
        ];

        $validator = Validator::make($input, [
            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
            'phone' => 'required|min:7|max:20',
            'message' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $Contact_us = new Contact_us();
        $Contact_us->phone = $request->phone;
        $Contact_us->email = $request->email;
        $Contact_us->message = $request->message;
        $Contact_us->name = $request->name;
        $Contact_us->save();
        $msg=$lang == 'ar' ? 'تم ارسال الرسالة بنجاح' : 'The message was sent successfully';
        return $this->apiResponseMessage(1,$msg, 200);
    }

    /*
     * General Info of shop
     * To Do find way send shop_id of custom shop
     */

    public function about_us(Request $request)
    {
        $lang = $request->header('lang');
        $shop=Shop::find($request->shop_id);
        $About=About::where('shop_id',$request->shop_id)->get();
        $check=$this->not_found($shop,'المحل','shop',$lang);
        if(isset($check))
        {
            return $check;
        }
        $msg=$lang == 'ar' ? 'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(AboutResource::collection($About),$msg);
    }

    /*
     * get social and other info of shop
     */

    public function get_info()
    {
        $shop=Settings::first();
        $social=new ContactUsResource($shop);
        return $this->apiResponseData($social,'success',200);
    }

    /*
     * about us web site
     */
    public function about_us_web_site(Request $request)
    {
        $lang = $request->header('lang');
        $About=About::where('shop_id',$request->shop_id)->get();
        $shop=Settings::first();
        $social=new ContactUsResource($shop);
        $data=['about_us'=>AboutResource::collection($About),'other_info'=>$social];
        $msg=$lang == 'ar' ? 'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData($data,$msg);
    }

    /*
     * get currencies
     */
    public function get_currencies(Request $request)
    {
        $currency=Currency::get();
        return $this->apiResponseData(CurrencyResource::collection($currency),'success',200);
    }
}