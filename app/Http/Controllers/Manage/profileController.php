<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Auth,Response,Hash;
use App\User;

class profileController extends Controller
{



    public function index() 
    {
      return view('manage.profile.index');
    }



    public function getInfo ($id)
    {
      $user=User::find($id);
      return $user;
    }


    public function update(Request $request){
        $this->validate(

                 $request,
                    [
                    'email'    => 'required|unique:users,email,'.$request->id,
                    'name'    => 'required|unique:users,name,'.$request->id,

                    ],
                    [
                      'name.unique'=>trans("main.nameunique")
                    ]

                );

        //dd($request->id);
       $user =User::find($request->id);
            if($request->img)
            {
                // $this->deleteFile();
                $image = $request->file('img');
                $input['image'] = time().'.'.$image->getClientOriginalExtension();
                $dist = public_path('/images/user/');
                $image->move($dist, $input['image']);
                $user->img=$input['image'];
            }


        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->jop = $request->jop;
        $user->name = $request->name;
        if($request->password){
        $user->password = Hash::make($request->password);
          }

        $user->save();

        return ['user'=>$user,'success'=>1];
    }








}
