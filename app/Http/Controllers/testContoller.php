<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\NotificationMethods;
use App\Models\Products;
use App\Models\Product_details;
use Illuminate\Http\Request;
use App\Events\MessagesSubmit;
class testContoller extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    public function test(Request $request)
    {
        $text= $request->conent;
        event(new MessagesSubmit($text));
        return 'success';
    }

    public function test_n(Request $request)
    {
        $product=Product_details::first();
        $id=$request->type == 1 ? 0 : $product->id;
        $click_action = $request->type == 1 ? 1 : 2;
       return NotificationMethods::senNotificationToMultiUsers('to all users','test to all users notifcation',null,1,$click_action);
    }
}