<?php

namespace App\Http\Controllers\Fronted;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Products;
use Auth,File;
use Illuminate\Http\Request;
use App\Models\Cart;


class UserController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    public function show_login()
    {
        return view('Fronted.User.login');
    }

    public function forget_password ()
    {
        return view('Fronted.User.forget_password');
    }

    public function show_Register()
    {
        return view('Fronted.User.show_Register');
    }

    public function my_wishlist()
    {
        return view('Fronted.User.my_wishlist');
    }

    public function my_cart()
    {
        return view('Fronted.User.my_cart');
    }

    public function make_order()
    {
        return view('Fronted.User.make_order');
    }

    public function orderSuccess()
    {
        return view('Fronted.User.orderSuccess');

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('/');
    }
    
    public function update_cart(Request $request)
    {
        $user=Auth::user();
         $lang=$request->header('lang');
        $data=$request->values;
        foreach($data as $key=>$row){
            if($key != 'undefined'){
                  $product=Products::find($key);
            if(is_null($product)) {
                $msg=$lang=='ar' ? ' المنتج رقم ' .$key.' غير موجود ': 'product number ' . $key .' not found';
                Cart::where('user_id',$user->id)->where('type',0)->delete();
                return $this->apiResponseMessage('0',$msg,200);
            }else{
                 if($row > $product->quantity)
                {
                    $product=Products::find($key);
                    $name=$lang=='ar' ? $product->name_ar : $product->name_en;
                $msg=$lang=='ar' ?  $name.' الكمية المطلوبة غير متوفرة للمنتج  ': 'The required quantity of the product  ' . $name .' is not available';
                return $this->apiResponseMessage('0',$msg,200);
                }
         
            }
        }
        }
        
        Cart::where('user_id',$user->id)->where('type',0)->delete();
        foreach($data as $key=>$value){
            if($key != 'undefined'){
                   $cart=new Cart;
                $cart->user_id=$user->id;
                $cart->product_id=$key;
                $cart->type=0;
                $cart->quantity=$value;
                $cart->save();
            }
        }
                $msg=$lang=='ar' ? 'تم تعديل السلة بنجلح' : 'cart edited successfully';

        return $this->apiResponseMessage(1,$msg,200);
    }

}
