<?php

namespace App\Http\Resources\Shop;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;


class AdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $arryOfRole=$this->getRoleIds();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'token'=>$this->token,
            'main_shop' => $this->mainShop_id,
            'logo' => BaseController::getImageUrl('Shop',$this->logo),
            'role_1'=>in_array(1,$arryOfRole) ? true : false,
            'role_2'=>in_array(2,$arryOfRole) ? true : false,
            'role_3'=>in_array(3,$arryOfRole) ? true : false,
            'role_4'=>in_array(4,$arryOfRole) ? true : false,
            'role_5'=>in_array(5,$arryOfRole) ? true : false,
            'role_6'=>in_array(6,$arryOfRole) ? true : false,
            'role_7'=>in_array(7,$arryOfRole) ? true : false,
            'role_8'=>in_array(8,$arryOfRole) ? true : false,
            'role_9'=>in_array(9,$arryOfRole) ? true : false,
            'role_10'=>in_array(10,$arryOfRole) ? true : false,
            'role_11'=>in_array(11,$arryOfRole) ? true : false,
            'role_12'=>in_array(12,$arryOfRole) ? true : false,
            'role_13' =>in_array(13,$arryOfRole) ? true : false,
            'role_14' =>in_array(14,$arryOfRole) ? true : false,
            'role_15' =>in_array(15,$arryOfRole) ? true : false,
            'role_16' =>in_array(16,$arryOfRole) ? true : false,
            'roles'=>$this->roles,


        ];
    }
}
