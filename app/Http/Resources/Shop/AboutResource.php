<?php

namespace App\Http\Resources\Shop;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use DB,Auth;
use App\Models\color;
use App\Models\Sizes;
class AboutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        return [
            'id' => $this->id,
            'title_en' => $this->title_en,
            'desc_en' =>  $this->desc_en,
            'title_ar' => $this->title_ar,
            'desc_ar' =>  $this->desc_ar,

        ];
    }
}
