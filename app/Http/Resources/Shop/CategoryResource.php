<?php

namespace App\Http\Resources\Shop;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use DB;
class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        return [
            'id' => $this->id,
            'name_ar' =>  $this->name_ar ,
            'name_en' =>  $this->name_en ,
            'desc_ar' =>  $this->desc_ar ,
            'desc_en' =>  $this->desc_en ,
                        'status'=>$this->status,
            'image' => BaseController::getImageUrl('Category',$this->icon),
        ];
    }
}
