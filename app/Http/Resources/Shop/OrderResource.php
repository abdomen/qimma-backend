<?php

namespace App\Http\Resources\Shop;

use App\Http\Resources\locationResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\UserResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $adress=$this->address;

        return [
            'id' => $this->id,
            'total_price'=>(double)$this->total_price,
            'shipping_price'=>(double)$this->shipping_price,
            'status'=>$this->status,
            'email'=>$this->user->email,
            'phone'=>$this->user->phone,
            'payment_method'=>$this->payment_method,
            'rate'=>$this->rate,
            'report'=>$this->report,
            'date'=>$this->created_at,
            'name'=>$this->user->first_name . ' '. $this->user->last_name,
            'user_id'=>$this->user->id,
            'representative_id'=>$this->representative_id,
            'lat'=>!is_null($adress) ? $adress->lat : '',
            'lng'=>$adress ? $adress->lng : '',
            'address'=>$adress ? $adress->address: '',
            'products' => CartResource::collection($this->products),
            'user'=>$this->user,
            'representative'=>$this->representative,
        ];
    }
}