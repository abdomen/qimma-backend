<?php

namespace App\Http\Controllers\Shop_Api;

use App\Http\Resources\ColorResource;
use App\Http\Resources\Shop\AboutResource;
use App\Http\Resources\Shop\AdminResource;
use App\Models\About;
use App\Models\role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Auth,Artisan,Hash,File,Crypt,DB;
use App\Http\Resources\UserResource;
use App\Models\Shop;
use App\Http\Controllers\Manage\BaseController;
class AboutController extends Controller
{
    use \App\Http\Controllers\Api\ApiResponseTrait;

    /*
     * Add new user to Auth Shop database
     */
    public function add_About(Request $request)
    {
        $lang = $request->header('lang');
        $input = $request->all();
        $validationMessages = [
            'title_ar.required' => $lang == 'ar' ?  'من فضلك ادخل العنوان بالعربية ' :" title in arabic is required" ,
            'title_en.required' => $lang == 'ar' ?  'من فضلك ادخل العنوان بالانجليزية ' :" title in english is required" ,
            'desc_ar.required' => $lang == 'ar' ?  'من فضلك ادخل المحتوى بالعربية ' :" content in arabic is required" ,
            'desc_en.required' => $lang == 'ar' ?  'من فضلك ادخل المحتوى بالانجليزية ' :" content in english is required" ,

        ];

        $validator = Validator::make($input, [
            'title_ar' => 'required',
            'title_en' => 'required',
            'desc_ar' => 'required',
            'desc_en' => 'required',
        ], $validationMessages);

        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }

        $About = new About();
        $About->title_ar = $request->title_ar;
        $About->title_en = $request->title_en;
        $About->desc_ar = $request->desc_ar;
        $About->desc_en = $request->desc_en;
        $About->save();
        $msg=$lang == 'ar' ? 'تمت الاضافة بنجاح' : 'Success';
        return response()->json([ 'status'=>1,'message'=> $msg, 'data'=>new AboutResource($About)]);

    }

    /*
     * Edit user information
    */
    public function edit_Admin(Request $request,$id)
    {
        $lang = $request->header('lang');
        $user = Shop::find($id);
        $check=$this->not_found($user,'المدير','Admin',$lang);
        if(isset($check))
        {
            return $check;
        }
        $input = $request->all();
        $validationMessages = [
            'name.required' => $lang == 'ar' ?  'من فضلك ادخل رقم الاسم ' :" name is required" ,
            'password.required' => $lang == 'ar' ? 'من فضلك ادخل كلمة السر' :"password is required"  ,
            'email.required' => $lang == 'ar' ? 'من فضلك ادخل البريد الالكتروني' :"email is required"  ,
            'email.unique' => $lang == 'ar' ? 'هذا البريد الالكتروني موجود لدينا بالفعل' :"email is already teken" ,
            'email.regex'=>$lang=='ar'? 'من فضلك ادخل بريد الكتروني صالح' : 'The email must be a valid email address',
            'phone.required' => $lang == 'ar' ? 'من فضلك ادخل البريد رقم الهاتف' :"phone is required"  ,
            'phone.unique' => $lang == 'ar' ? 'رقم الهاتف موجود لدينا بالفعل' :"phone is already teken" ,
            'name.unique' => $lang == 'ar' ?  'الاسم موجود لدينا بالفعل' :"name is already teken" ,
            'phone.min' => $lang == 'ar' ?  'رقم الهاتف يجب ان لا يقل عن 7 ارقام' :"The phone must be at least 7 numbers" ,
            'phone.numeric' => $lang == 'ar' ?  'رقم الهاتف يجب ان يكون رقما' :"The phone must be a number" ,

        ];

        $validator = Validator::make($input, [
            'phone' => 'required|min:7|numeric|unique:shops,phone,'.$id,
            'email' => 'required|unique:shops,email,'.$id.'|regex:/(.+)@(.+)\.(.+)/i',
            'name' => 'required|unique:shops,name,'.$id,
        ], $validationMessages);
        if ($validator->fails()) {
            return $this->apiResponseMessage(0,$validator->messages()->first(), 200);
        }


        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->name = $request->name;
        if($request->password){
            $user->password = Hash::make($request->password);
        }
        if($request->image){
            BaseController::deleteFile('Shop',$user->logo);
            $name=BaseController::saveImage('Shop',$request->file('image'));
            $user->logo=$name;
        }
        $user->save();
        $user['token']=null;
        $msg=$lang=='ar' ?  'تم تعديل بيانات المدير' :'Client Edited successfully' ;
        return $this->apiResponseData(  new AdminResource($user),  $msg);
    }

    /*
     * Show single  user
    */
    public function single_Admin(Request $request,$user_id){
        $lang=$request->header('lang');
        $user = Shop::find($user_id);
        $check=$this->not_found($user,'المدير','Admin',$lang);
        if(isset($check))
        {
            return $check;
        }
        $user['token']=null;
        $msg=$lang=='ar' ?'تمت العملية بنجاح' : 'success';
        return $this->apiResponseData(new AdminResource($user),$msg);

    }

    /*
     * Delete user ..
     */

    public function delete_Admin(Request $request,$user_id){
        $lang=$request->header('lang');
        $user=Shop::find($user_id);
        $check=$this->not_found($user,'المدير','Admin',$lang);
        if(isset($check)){
            return $check;
        }
        BaseController::deleteFile('Shop',$user->logo);
        if($user_id ==1)
        {
            $msg=$lang=='ar' ? 'لا يمكن حذف المدير الرئيسي'  : 'cannot delete super admin';
            return $this->apiResponseMessage(1,$msg,200);

        }
        $user->delete();
        $msg=$lang=='ar' ? 'تم حذف المدير بنجاح'  : 'Admin Deleted successfully';
        return $this->apiResponseMessage(1,$msg,200);
    }

    /*
     * get All user for Auth shop
     */
    public function all_Admins(Request $request)
    {
        $users=Shop::orderBy('id','desc')->get();
        foreach($users as $row){$row['token']=null;}
        return $this->apiResponseData(AdminResource::collection($users),'success');
    }

}