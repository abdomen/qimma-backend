<?php

namespace App\Http\Resources\Shop;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;


class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        $address=$lang=='ar' ? $this->address_ar : $this->address_en;
        return [
            'id' => $this->id,
            'address_ar' => $this->address_ar,
            'name_ar' => $this->name_ar,
            'name_en' => $this->name_en,
            'address_en' => $this->address_en,
            'about_ar' => $this->about_ar,
            'about_en' => $this->about_en,
            'email' => $this->email,
            'phone' => $this->phone,
            'facebook' => $this->facebook,
            'snap' => $this->snap,
            'twitter' => $this->twitter,
            'instagram' => $this->instagram,
            'shipping_price'=>(double)$this->shipping_price,
            'tax'=>(double)$this->tax,
            'logo' => BaseController::getImageUrl('Settings',$this->logo),
        ];
    }
}
