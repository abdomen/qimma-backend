<?php

namespace App\Http\Resources\Shop;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;

class user_codeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        return [
            'id' => $this->id,
            'name_ar' =>  $this->name_ar ,
            'name_en' =>  $this->name_en ,
            'desc_ar' =>  $this->desc_ar ,
            'desc_en' =>  $this->desc_en ,
            'code'=>$this->code,
            'amount'=>$this->amount,
            'amount_type'=>$this->amount_type,
            'end_date'=>$this->end_date,
            'is_use'=>(int)$this->pivot->is_use,
        ];
    }
}
