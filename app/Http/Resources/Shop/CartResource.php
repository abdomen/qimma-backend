<?php

namespace App\Http\Resources\Shop;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use DB,Auth;
use App\Models\color;
use App\Models\Sizes;
use App\Http\Resources\ColorResource;
use App\Http\Resources\ImagesResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        $color_code=null;
        $color=color::find($this->pivot->color_id);
        $size_name=null;
        if(!is_null($color)){
            $color_code=new ColorResource($color);
        }
        $size=Sizes::find($this->pivot->size_id);
        if(!is_null($size)){
            $size_name=new SizeResource($size);
        }
        return [
            'id' => $this->id,
            'id' => $this->id,
            'name_ar' =>  $this->name_ar ,
            'name_en' =>  $this->name_en ,
            'desc_ar' =>  $this->desc_ar ,
            'desc_en' =>  $this->desc_en ,
            'quantity'=>(int)$this->pivot->quantity,
            'image' => BaseController::getImageUrl('Products',$this->image),
            'rate' => $this->rate,
            'price' => (double)$this->price,
            'is_offer'=>$this->is_offer ? $this->is_offer : 0,
            'offer_amount'=>$this->offer_amount,
            'lat'=>(double)$this->lat,
            'lng'=>(double)$this->lng,
            'color' => $color_code,
            'size'=>$size_name,
        ];
    }
}
