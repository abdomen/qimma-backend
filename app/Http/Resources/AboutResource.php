<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use DB,Auth;
use App\Models\color;
use App\Models\Sizes;
class AboutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        return [
            'id' => $this->id,
            'title' => $lang == 'ar' ? $this->title_ar : $this->title_en,
            'desc' => $lang == 'ar' ? $this->desc_ar : $this->desc_en,
        ];
    }
}
