<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use DB,Auth;
class locationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        $user=Auth::user();
        return [
            'id' => $this->id,
            'lat' => (double)$this->lat,
            'lng' => (double)$this->lng,
            'address' =>$this->address,
            'name'=>$this->user->first_name . ' ' .$this->user->last_name,
        ];
    }
}
