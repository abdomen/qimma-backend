<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;


class ContactUsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        $address=$lang=='ar' ? $this->address_ar : $this->address_en;
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $address,
            'facebook' => $this->facebook,
            'snap' => $this->snap,
            'twitter' => $this->twitter,
            'instagram' => $this->instagram,
            'logo' => BaseController::getImageUrl('Settings',$this->logo),
        ];
    }
}
