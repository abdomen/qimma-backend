<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Shop;
use App\Models\Safe;

class Safe_transactionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        // return $this->safe_from_data->name_ar;

        if(Safe::find($this->safe_from) != null){
            $safe_from = Safe::find($this->safe_from);
        }else{
            $safe_from = new \stdClass();
            $safe_from->id = null;
            $safe_from->name_ar = null;
            $safe_from->name_en = null;
            $safe_from->status = null;
            $safe_from->total_money = null;
        }

        if(Safe::find($this->safe_to) != null){
            $safe_to = Safe::find($this->safe_to);
        }else{
            $safe_to = new \stdClass();
            $safe_to->id = null;
            $safe_to->name_ar = null;
            $safe_to->name_en = null;
            $safe_to->status = null;
            $safe_to->total_money = null;
        }

        if(Shop::find($this->admin_id) != null){
            $admin = Shop::find($this->admin_id);
        }else{
            $admin = new \stdClass();
            $admin->name = null;
        }

        $time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y').' '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');

        return [
            'id' => $this->id,
            'admin_name' => $admin->name, 
            'safe_from' => new SafeResource($safe_from),
            'safe_to' => new SafeResource($safe_to),
            'money' => $this->value,
            'date' => $time,
            'notes' => $this->notes,
        ];
    }
}
