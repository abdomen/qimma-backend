<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;

class Sliders_MResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        return [
            'id' => $this->id,
            'image' => BaseController::getImageUrl('Slider',$this->image),
            'title_ar' => $lang == 'ar' ? $this->title_ar : $this->title_en,
            'title_en' => $lang == 'ar' ? $this->title_ar : $this->title_en,
            'sub_title_ar' => $lang == 'ar' ? $this->sub_title_ar : $this->sub_title_en,
            'sub_title_en' => $lang == 'ar' ? $this->sub_title_ar : $this->sub_title_en,
        ];
    }
}
