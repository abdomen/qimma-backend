<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Product_details;
use Illuminate\Support\Collection;
use App\Http\Controllers\Manage\BaseController;
use DB,Auth;
use App\Models\Price;
use App\Models\Inventory;
use App\Models\Product_detail_inventory;

class Inventory_product_details_resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return [
        //     'product' => new product_detailsResource($this->Product_detail->Product_detail),
        //     'quantity' => $this->Product_detail->quantity,
        // ];
        $lang = $request->header('lang');
        $distance=0;
        if ($request->lat && $request->lng) {
            $distance=$this->Product_detail->select(DB::raw('*, ( 6367 * acos( cos( radians('.$request->lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$request->lng.') ) + sin( radians('.$request->lat.') )* sin( radians( lat ) ) ) ) AS distance'))->where('id', $this->Product_detail->id)->value('distance');
        }
        $is_fav=false;
        $user=Auth::user();
        if($user) {
            $is_fav = $this->Product_detail->user_favorite->contains($user->id);
        }
        if(isset($this->Product_detail->priceeee[0])) {
            $pro_price = Price::find($this->Product_detail->priceeee[0]->price_id);
        }else{
            $pro_price = new \stdClass();
            $pro_price->purchasing_price = false;
            $pro_price->wholesale_wholesale_price = false;
            $pro_price->wholesale_price = false;
            $pro_price->selling_price = false;
        }
        if(isset($this->Product_detail->Inventory[0])) {
            $pro_inv = Inventory::find($this->Product_detail->Inventory[0]->inventory_id);
        }else{
            $pro_inv = new \stdClass();
            $pro_inv->id = null;
            $pro_inv->name_ar = null;
            $pro_inv->name_en = null;
        }
        
        return [
            'id' => $this->Product_detail->id,
            'main_product_id' => $this->Product_detail->product_id,
            'main_Product_name' => $lang == 'ar' ? $this->Product_detail->Main_Product['name_ar'] : $this->Product_detail->Main_Product['name_en'],
            'desc' => $lang == 'ar' ?(string) $this->Product_detail->desc_ar : (string)$this->Product_detail->desc_en,
            'Difference' => $lang == 'ar' ?(string) $this->Product_detail->difference_ar : (string)$this->Product_detail->difference_en,
            'image' => BaseController::getImageUrl('Product_image',$this->Product_detail->image),
            'rate' => (int)$this->Product_detail->rate,
            'is_offer'=>$this->Product_detail->is_offer ?(int) $this->Product_detail->is_offer : 0,
            'offer_amount'=>$this->Product_detail->offer_amount .'%',
            'lat'=>(double)$this->Product_detail->lat,
            'lng'=>(double)$this->Product_detail->lng,
            'distance'=>(int)$distance . ' km',
            'is_favorite'=>$is_fav,
            'Inventory_id' => $pro_inv->id,
            'Inventory_name' => $lang == 'ar' ? $pro_inv->name_ar : $pro_inv->name_en,
            'Quantity' => $this->quantity,
            'colors'=>ColorResource::collection($this->Product_detail->colors),
            'sizes'=>SizeResource::collection($this->Product_detail->sizes),
            'images'=>ImagesResource::collection($this->Product_detail->images),
            'Purchasing_price' => $pro_price->purchasing_price,
            'Wholesale_wholesale_price' => $pro_price->wholesale_wholesale_price,
            'Wholesale_price' => $pro_price->wholesale_price,
            'Selling_price' => $pro_price->selling_price,
            'barcode'=>$this->Product_detail->barcode,
        ];
    }
}
