<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Product_details;
use Illuminate\Support\Collection;
use App\Http\Controllers\Manage\BaseController;
use DB,Auth;
use App\Models\Price;
use App\Models\Inventory;
use App\Models\Product_detail_inventory;

class ProInOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        $lang = $request->header('lang');

        if(isset($this->Product_detail)) {

            $product = Product_details::find($this->Product_detail->id);
        }else{
            $product = new \stdClass();
            $product->id = null;
            $product->product_id = null;
            $product->Main_Product['name_ar'] = null;
            $product->Main_Product['name_en'] = null;
            $product->desc_ar = null;
            $product->desc_en = null;
            $product->difference_ar = null;
            $product->difference_en = null;
            $product->image = null;
        }

        return [
            'id' => $product->id,
            'main_product_id' => $product->product_id,
            'main_Product_name' => $lang == 'ar' ? $product->Main_Product['name_ar'] : $product->Main_Product['name_en'],
            'desc' => $lang == 'ar' ?(string) $product->desc_ar : (string)$product->desc_en,
            'Difference' => $lang == 'ar' ?(string) $product->difference_ar : (string)$product->difference_en,
            'image' => BaseController::getImageUrl('Product_image',$product->image),
            'quantity'=>$this->quantity,
            'price'=>(double)$this->price,
            'color'=>new ColorResource($this->Color),
            'size'=>new SizeResource($this->Size), 
        ];
    }
}
