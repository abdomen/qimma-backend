<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;


class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        return [
            'id' => $this->id,
            'username' => $this->user_comment->frist_name,
            'userId' => $this->user_comment->id,
            'userImage'=>BaseController::getImageUrl('users',$this->user_comment->image),
            'comment'=>$this->comment,
            'time'=>$this->created_at
        ];
    }
}
