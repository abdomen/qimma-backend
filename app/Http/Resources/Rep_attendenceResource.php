<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\User;

class Rep_attendenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        $lang = $request->header('lang');
        
        if($lang == 'ar'){
            $status = $this->status == 1 ? 'حضور' : 'انصراف' ;
        }else{
            $status = $this->status == 1 ? 'attend' : 'leave' ;
        }

        if(User::find($this->representative_id) != null){
            $representative = User::find($this->representative_id);
        }else{
            $representative = new \stdClass();
            $representative->first_name = null;
            $representative->last_name = null;
        }

        if(User::find($this->user_id) != null){
            $user = User::find($this->user_id);
        }else{
            $user = new \stdClass();
            $user->first_name = null;
            $user->last_name = null;
        }

        return [
            'id' => $this->id,
            'status' => $status,
            'notes' => $this->note,
            'created_at' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y').' '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s'),
            'representative_id' => $this->representative_id,
            'rep_name' => $representative->first_name.' '.$representative->last_name,
            'user_id' => $this->user_id,
            'user_name' => $user->first_name.' '.$user->last_name,
            'lng' => $this->lng,
            'lat' => $this->lat,
        ];
    }
}
