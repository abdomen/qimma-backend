<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;


class BillResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        $lang = $request->header('lang');
        
        $adress=$this->address;

        if($this->representative_id == 0) {
            $rep_first_name = ' ';
            $rep_last_name = ' ';
        }else{
            $rep_first_name = $this->representative->first_name;
            $rep_last_name = $this->representative->last_name;
        }

        if(isset($this->user)) {
            $this->user->email = $this->user->email;
            $this->user->phone = $this->user->phone;
            $this->user->first_name = $this->user->first_name;
            $this->user->last_name = $this->user->last_name;            
        }else{
            $this->user = new \stdClass();
            $this->user->email = null;
            $this->user->phone = null;
            $this->user->first_name = null;
            $this->user->last_name = null;
        }

        if($this->price_type == 'wholesale_wholesale_price'){
            $price_type = $lang == 'ar' ? 'جملة الجملة' : 'Wholesale wholesale';
        }elseif($this->price_type == 'wholesale_price'){
            $price_type = $lang == 'ar' ? 'جملة' : 'Wholesale';
        }elseif($this->price_type == 'selling_price'){
            $price_type = $lang == 'ar' ? 'قطاعي' : 'Sectoral';
        }else{
            $price_type = null;
        }

        return [
            'id' => $this->id,
            'client_id'=>$this->user_id,
            'client_name'=>$this->user->first_name . ' '. $this->user->last_name,
            'client_phone'=>$this->user->phone,
            'address'=>$adress ? $adress->address: '',
            'representative_id'=>$this->representative_id,
            'representative_name'=>$rep_first_name . ' '. $rep_last_name,
            'payment_method'=>$this->payment_method == 1 ? 'Cash' : 'Online',
            'price_type' => $price_type,
            'status'=>$this->status == 1 ? 'New' : 'Finished',
            'date'=>\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y').' '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s'),
            'Products'=>Order_product_contResource::collection($this->Products),
            'discount' => $this->dis_type == 1 ? (double)$this->discount : (double)$this->discount.' %',
            'tax1' => $this->tax1_type == 1 ? (double)$this->tax1 : (double)$this->tax1.' %',
            'tax2' => $this->tax2_type == 1 ? (double)$this->tax2 : (double)$this->tax2.' %',
            'shipping_price'=>(double)$this->shipping_price,
            'pre_price'=>(double)$this->pre_price,
            'total_price' => (double)$this->total_price,
            'paid' => (double)$this->paid,
            'rest' => (double)$this->rest,
            'bank_transmission_receipt' => $this->bank_trans_receipt == null ? '' : BaseController::getImageUrl('Order',$this->bank_trans_receipt),
            'bank_name' => $this->bank_name,
            'notes'=>$this->notes,
        ];
    }
}
