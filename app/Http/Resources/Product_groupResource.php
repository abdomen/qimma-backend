<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product_groupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'group_id' => $this->group_id,
            'product_id' => $this->product_id,
            // 'discount_per_type' => $this->discount_per_type,
            // 'discount_per_value' => $this->discount_per_value,
            'product' => $this->product,
            'group' => $this->group,
        ];
    }
}
