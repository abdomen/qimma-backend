<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Product_details;
use Illuminate\Support\Collection;
use App\Http\Controllers\Manage\BaseController;
use DB,Auth;
use App\Models\Price;
use App\Models\Inventory;
use App\Models\Product_detail_inventory;


class Product_detailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        $lang = $request->header('lang');
        $distance=0;
        if ($request->lat && $request->lng) {
            $distance=$this->select(DB::raw('*, ( 6367 * acos( cos( radians('.$request->lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$request->lng.') ) + sin( radians('.$request->lat.') )* sin( radians( lat ) ) ) ) AS distance'))->where('id', $this->id)->value('distance');
        }
        $is_fav=false;
        $user=Auth::user();
        if($user) {
            $is_fav = $this->user_favorite->contains($user->id);
        }
        if(isset($this->priceeee[0])) {
            $pro_price = Price::find($this->priceeee[0]->price_id);
        }else{
            $pro_price = new \stdClass();
            $pro_price->purchasing_price = false;
            $pro_price->wholesale_wholesale_price = false;
            $pro_price->wholesale_price = false;
            $pro_price->selling_price = false;
        }
        if(isset($this->Inventory[0])) {
            $pro_inv = Inventory::find($this->Inventory[0]->inventory_id);
        }else{
            $pro_inv = new \stdClass();
            $pro_inv->id = null;
            $pro_inv->name_ar = null;
            $pro_inv->name_en = null;
        }
        if(isset($this->id)) {
            $pro_qty = Product_detail_inventory::where('product_detail_id',$this->id)->get();
            $qty = $pro_qty->sum('quantity');
        }else{
            $qty = 0;
        }

        return [
            'id' => $this->id,
            'main_product_id' => $this->product_id,
            'main_Product_name' => $lang == 'ar' ? $this->Main_Product['name_ar'] : $this->Main_Product['name_en'],
            'desc' => $lang == 'ar' ?(string) $this->desc_ar : (string)$this->desc_en,
            'Difference' => $lang == 'ar' ?(string) $this->difference_ar : (string)$this->difference_en,
            'image' => BaseController::getImageUrl('Product_image',$this->image),
            'rate' => (int)$this->rate,
            'is_offer'=>$this->is_offer ?(int) $this->is_offer : 0,
            'offer_amount'=>$this->offer_amount .'%',
            'lat'=>(double)$this->lat,
            'lng'=>(double)$this->lng,
            'distance'=>(int)$distance . ' km',
            'is_favorite'=>$is_fav,
            'Inventory_id' => $pro_inv->id,
            'Inventory_name' => $lang == 'ar' ? $pro_inv->name_ar : $pro_inv->name_en,
            'Quantity' => $qty,
            'colors'=>ColorResource::collection($this->colors),
            'sizes'=>SizeResource::collection($this->sizes),
            'images'=>ImagesResource::collection($this->images),
            'Purchasing_price' => $pro_price->purchasing_price,
            'Wholesale_wholesale_price' => $pro_price->wholesale_wholesale_price,
            'Wholesale_price' => $pro_price->wholesale_price,
            'Selling_price' => $pro_price->selling_price,
            'barcode'=>$this->barcode,
        ];
    }
}
