<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;


class UserAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        return [
            'id' => $this->id,
            'name' => $this->first_name.' '.$this->last_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'image' => BaseController::getImageUrl('users',$this->image),
            'fire_base_token'=>$this->fire_base_token,
            'phone' => $this->phone,
            'email' => $this->email,
            'status' => (int)$this->status,
            'social'=>(int)$this->social,
            'notification'=>$this->notification ? 1 : 0,
            'message'=>$this->message ? 1 : 0,
            'lang'=>$lang,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'debt' => (double)$this->debt,
            'currency_id' => (int)$this->currency_id,
            'addresses'=>$this->user_address,
        ];
    }
}
