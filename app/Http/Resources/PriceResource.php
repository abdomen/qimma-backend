<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;


class PriceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        if ($lang == 'ar'){
            return [
                'id' => $this->id,
                'Product_detail_price' => Product_details_priceResource::collection($this->Product_detail_price),
                'سعر الشراء' => $this->purchasing_price,
                'سعر جملة الجملة' => $this->wholesale_wholesale_price,
                'سعر الجملة' => $this->wholesale_price,
                'سعر البيع' => $this->selling_price
            ];
        }else{
            return [
                'id' => $this->id,
                'purchasing_price' => $this->purchasing_price,
                'wholesale_wholesale_price' => $this->wholesale_wholesale_price,
                'wholesale_price' => $this->wholesale_price,
                'selling_price' => $this->selling_price
            ];
            }
    }
}
