<?php

namespace App\Http\Resources;

use App\Http\Resources\locationResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\UserResource;
use App\Models\Order_status;

class P_d_ordersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        $lang = $request->header('lang');
        
        $adress=$this->address;

        $status_name = null;
        $status = Order_status::find($this->status);
        if(!is_null($status)){
            $status_name = $lang == 'ar' ? $status->status_ar : $status->status_en;
        }

        if(isset($this->user)) {
            $this->user->email = $this->user->email;
            $this->user->phone = $this->user->phone;
            $this->user->first_name = $this->user->first_name;
            $this->user->last_name = $this->user->last_name;            
        }else{
            $this->user = new \stdClass();
            $this->user->email = null;
            $this->user->phone = null;
            $this->user->first_name = null;
            $this->user->last_name = null;
        }

        if(isset($this->representative)) {
            $this->representative->first_name = $this->representative->first_name;
            $this->representative->last_name = $this->representative->last_name;            
        }else{
            $this->representative = new \stdClass();
            $this->representative->first_name = null;
            $this->representative->last_name = null;
        }

        if($this->price_type == 'wholesale_wholesale_price'){
            $price_type = $lang == 'ar' ? 'جملة الجملة' : 'Wholesale wholesale';
        }elseif($this->price_type == 'wholesale_price'){
            $price_type = $lang == 'ar' ? 'جملة' : 'Wholesale';
        }elseif($this->price_type == 'selling_price'){
            $price_type = $lang == 'ar' ? 'قطاعي' : 'Sectoral';
        }else{
            $price_type = null;
        }
        
        return [
            'id' => $this->id,
            'price_type' => $price_type,
            'pre_price' => $this->pre_price,
            'discount' => $this->dis_type == 1 ? (double)$this->discount : (double)$this->discount.' %',
            'tax1' => $this->tax1_type == 1 ? (double)$this->tax1 : (double)$this->tax1.' %',
            'tax2' => $this->tax2_type == 1 ? (double)$this->tax2 : (double)$this->tax2.' %',
            'total_price' => (double)$this->total_price,
            'paid' => (double)$this->paid,
            'rest' => (double)$this->rest,
            'shipping_price'=>(double)$this->shipping_price,
            'status'=> $status_name,
            'email'=>$this->user->email,
            'phone'=>$this->user->phone,
            'payment_method'=>$this->payment_method,
            'rate'=>$this->rate,
            'report'=>$this->report,
            'date'=>$this->created_at,
            'name'=>$this->user->first_name . ' '. $this->user->last_name,
            'user_id'=>$this->user_id,
            'representative_id'=>$this->representative_id,
            'representative_name'=>$this->representative->first_name .' '. $this->representative->last_name,
            'lat'=>!is_null($adress) ? $adress->lat : '',
            'lng'=>$adress ? $adress->lng : '',
            'address'=>$adress ? $adress->address: '',
            'products' => Order_product_detailsResource::collection($this->Products),
            'bank_transmission_receipt' => $this->bank_trans_receipt == null ? '' : BaseController::getImageUrl('Order',$this->bank_trans_receipt),
            'bank_name' => $this->bank_name,
            'notes'=>$this->notes,
            'safe' => new SafeResource($this->safe),
            'client_order' => (boolean)$this->client_order,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
            // 'location'=>$adress ? $adress: '',
            // 'user'=>$this->user,
            // 'representative'=>$this->representative,
        ];
    }
}
