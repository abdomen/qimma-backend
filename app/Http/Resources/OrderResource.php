<?php



namespace App\Http\Resources;



use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Controllers\Manage\BaseController;



class OrderResource extends JsonResource

{

    /**

     * Transform the resource into an array.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return array

     */

    public function toArray($request)

    {

        $adress=$this->address;



        return [

            'id' => $this->id,

            'total_price'=>(double)$this->total_price,

            'shipping_price'=>(double)$this->shipping_price,

            'status'=>(int)$this->status,

            'payment_method'=>(int)$this->payment_method,

            'rate'=> $this->rate ?   (int)$this->rate : 0,

            'report'=>$this->report,

            'address'=>new locationResource($adress),

            'products' => CartResource::collection($this->products),

            'date'=>\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y'),

        ];

    }

}

