<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Product_details;

class Order_product_contResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        
        if(Product_details::find($this->product_detail_id) != null){
            $pro = Product_details::find($this->product_detail_id);
        }else{
            $pro = new \stdClass();
            $pro->Main_Product['name_ar'] = null;
            $pro->Main_Product['name_en'] = null;
            $pro->difference_ar = null;
            $pro->difference_en = null;
        }
        
        return [
            'product_id' => $this->product_detail_id,
            'product_name' => $lang == 'ar' ? (string)$pro->Main_Product['name_ar'] : (string)$pro->Main_Product['name_en'],
            'difference' => $lang == 'ar' ? (string)$pro->difference_ar : (string)$pro->difference_en,
            'color'=>new ColorResource($this->color),
            'size'=>new SizeResource($this->Size),
            'quantity'=>(int)$this->quantity,
            'price'=>(int)$this->price,
            'sum'=>(int)$this->quantity*(int)$this->price,
        ];
    }
}
