<?php

namespace App\Http\Resources;

use App\Http\Resources\locationResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Controllers\Manage\BaseController;
use App\Http\Resources\UserResource;
use App\Models\Order_status;

class Orders_MResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        
        $adress=$this->address;

        $status_name = null;
        $status = Order_status::find($this->status);
        if(!is_null($status)){
            $status_name = $lang == 'ar' ? $status->status_ar : $status->status_en;
        }

        if(isset($this->user)) {
            $this->user->email = $this->user->email;
            $this->user->phone = $this->user->phone;
            $this->user->first_name = $this->user->first_name;
            $this->user->last_name = $this->user->last_name;            
        }else{
            $this->user = new \stdClass();
            $this->user->email = null;
            $this->user->phone = null;
            $this->user->first_name = null;
            $this->user->last_name = null;
        }

        if(isset($this->representative)) {
            $this->representative->first_name = $this->representative->first_name;
            $this->representative->last_name = $this->representative->last_name;            
        }else{
            $this->representative = new \stdClass();
            $this->representative->first_name = null;
            $this->representative->last_name = null;
        }

        if($this->price_type == 'wholesale_wholesale_price'){
            $price_type = $lang == 'ar' ? 'جملة الجملة' : 'Wholesale wholesale';
        }elseif($this->price_type == 'wholesale_price'){
            $price_type = $lang == 'ar' ? 'جملة' : 'Wholesale';
        }elseif($this->price_type == 'selling_price'){
            $price_type = $lang == 'ar' ? 'قطاعي' : 'Sectoral';
        }else{
            $price_type = null;
        }
        
        return [
            'id' => $this->id,
            'price_type' => $price_type,
            'pre_price' => (double)$this->pre_price,
            'discount_type' => $this->dis_type,
            'discount' => (double)$this->discount,
            'tax1_type' => $this->tax1_type,
            'tax1' => (double)$this->tax1,
            'tax2_type' => $this->tax2_type,
            'tax2' => (double)$this->tax2,
            'total_price' => (double)$this->total_price,
            'paid' => (double)$this->paid,
            'rest' => (double)$this->rest,
            'shipping_price'=>(double)$this->shipping_price,
            'status'=> $status_name,
            'email'=>$this->user->email,
            'phone'=>$this->user->phone,
            'payment_method'=>$this->payment_method,
            'rate'=>$this->rate,
            'report'=>$this->report,
            'date'=>$this->created_at,
            'name'=>$this->user->first_name . ' '. $this->user->last_name,
            'user_id'=>$this->user_id,
            'representative_id'=>$this->representative_id,
            'representative_name'=>$this->representative->first_name .' '. $this->representative->last_name,
            'lat'=>!is_null($adress) ? $adress->lat : '',
            'lng'=>$adress ? $adress->lng : '',
            'address_id'=> $adress ? $this->address->id: '',
            'address'=> $adress ? $adress->address: '',
            'products' => ProInOrderResource::collection($this->Products),
            'bank_transmission_receipt' => $this->bank_trans_receipt == null ? '' : BaseController::getImageUrl('Order',$this->bank_trans_receipt),
            'bank_name' => $this->bank_name,
            'safe' => new SafeResource($this->safe),
            'notes'=>$this->notes,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
        ];
    }
}
