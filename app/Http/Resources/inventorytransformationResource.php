<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Shop;
use App\Models\Inventory;
use App\Models\Product_details;

class inventorytransformationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');

        if(Inventory::find($this->from_inv_id) != null){
            $from_inv = Inventory::find($this->from_inv_id);
            $from_inv_nameEN = $from_inv->name_en;
            $from_inv_nameAR = $from_inv->name_ar;
        }else{
            $from_inv = new \stdClass;
            $from_inv_nameEN = null;
            $from_inv_nameAR = null;
        }

        if(Inventory::find($this->to_inv_id) != null){
            $to_inv = Inventory::find($this->to_inv_id);
            $to_inv_nameEN = $to_inv->name_en;
            $to_inv_nameAR = $to_inv->name_ar;
        }else{
            $to_inv = new \stdClass;
            $to_inv_nameEN = null;
            $to_inv_nameAR = null;
        }

        if(Product_details::find($this->product_detail_id) != null){
            $pro = Product_details::find($this->product_detail_id);
        }else{
            $pro = new \stdClass();
            $pro->Main_Product['name_ar'] = null;
            $pro->Main_Product['name_en'] = null;
            $pro->difference_ar = null;
            $pro->difference_en = null;
        }

        if(Shop::find($this->user_id) != null){
            $admin = Shop::find($this->user_id);
        }else{
            $admin = new \stdClass();
            $admin->name = null;
        }

        return [
            'id' => $this->id,
            'admin_id'=>$this->user_id ,
            'from_inv_id'=>$this->from_inv_id,
            'to_inv_id'=>$this->to_inv_id,
            'product_detail_id'=>$this->product_detail_id,
            'admin_name'=>$admin->name,
            'product_name' => $lang == 'ar' ? (string)$pro->Main_Product['name_ar'] : (string)$pro->Main_Product['name_en'],
            'difference' => $lang == 'ar' ? (string)$pro->difference_ar : (string)$pro->difference_en,
            'from_inv_nameEN' => $from_inv_nameEN,
            'from_inv_nameAR' => $from_inv_nameAR,
            'to_inv_nameEN' => $to_inv_nameEN,
            'to_inv_nameAR' => $to_inv_nameAR,
            'quantity'=>$this->quantity,
            'created_at' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y').' '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s'),
        ];
    }
}
