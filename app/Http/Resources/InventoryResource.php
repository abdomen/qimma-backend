<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = $request->header('lang');
        
        return [
            'id' => $this->id,
            'name' => $lang == 'ar' ? $this->name_ar : $this->name_en,
            'address' => $lang == 'ar' ? $this->address_ar : $this->address_en,
            'status' => $this->status,
            'phone' => $this->phone,
            'lng' => $this->lng,
            'lat' => $this->lat,
            'products' => Product_detail_inventoryResource::collection($this->product_detail),
        ];    
    }
}
